
class Outer:

    x = 10 

    def __init__(self):
        print("Outer Constructor ")
        self.y = 20

    @classmethod
    def fun(self):
        print("In Fun - 1")
        self.z = 30

        @classmethod
        class Inner:
            a = 70
            def __init___(self):
                print("Inner Constructor ")
                self.b = 80

            def gun(self):
                print("In Inner Function")

        print("In Fun - 2")
        i = Inner()
        i.gun()
        print("In Fun - 3")
        return i

print("Start ")
o = Outer.fun().Inner()
o.gun()
i = Outer.fun()

i.gun()
print("End")
