
class Outer:  
    x = 10 
    def __init__(self):
        print("Outer Constructor")
        y = 20 

    #@classmethod
    class Inner:
        a = 30
        def __init__(self):
            print("Inner Constructor",self)#," : ",out)
            self.b = 40
        
        @classmethod
        def fun(self):
            print("Instance of Inner ",self)
            print(self.a)
            #print(self.a,self.b)
            #print(self.x,self.y)    #   Error


print("start")
#i = Outer().Inner()
Outer().Inner()
Outer().Inner.fun()
#i.fun()
print("End")
