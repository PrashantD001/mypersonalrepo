

# Integer example
x = 5
y = -10
z = 0

print(x," : ",type(x))
print(y," : ",type(y))
print(z," : ",type(z))

# Arithmetic operations
sum_result = x + y
difference_result = x - y
product_result = x * y
quotient_result = x / y
remainder_result = x % 3

print("Sum:", sum_result)
print("Difference:", difference_result)
print("Product:", product_result)
print("Quotient:", quotient_result)
print("Remainder:", remainder_result)

