


# Floating-point example
a = 3.14
b = -0.5
c = 2.0


print(a," : ",type(a))
print(b," : ",type(b))
print(c," : ",type(c))

# Arithmetic operations
sum_result = a + b
product_result = a * c
division_result = b / c

print("Sum:", sum_result)
print("Product:", product_result)
print("Division:", division_result)

