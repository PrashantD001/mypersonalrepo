

# Complex number example
complex_num1 = 2 + 3j
complex_num2 = -1.5 + 0.5j
complex_num3 = complex(15,16)

a = 50
b = 15.15
complex_num4 = complex(a,b)

print(complex_num1," : ",type(complex_num1))
print(complex_num2," : ",type(complex_num2))
print(complex_num3," : ",type(complex_num3))
print(complex_num4," : ",type(complex_num4))

# Arithmetic operations
sum_result = complex_num1 + complex_num2
product_result = complex_num1 * complex_num2
conjugate_result = complex_num1.conjugate()

print("Sum:", sum_result)
print("Product:", product_result)
print("Conjugate:", conjugate_result)

