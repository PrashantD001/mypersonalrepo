# Frozenset example
mutable_set = {1, 2, 3}
frozen_set = frozenset(mutable_set)

print("Mutable set:", mutable_set)
print("Frozen set:", frozen_set)

# Attempting to modify a frozenset (will result in an error)
try:
    frozen_set.add(4)
except AttributeError as e:
    print("Error:", e)

