# Set example
fruits = {"apple", "banana", "cherry"}
print(fruits)

# Adding elements to a set
fruits.add("orange")
print("Updated set:", fruits)

# Removing elements from a set
fruits.remove("banana")
print("Set after removal:", fruits)

# Checking membership in a set
is_apple_in_set = "apple" in fruits
print("Is 'apple' in the set?", is_apple_in_set)

# Set operations
vegetables = {"carrot", "broccoli", "spinach"}

union_result = fruits | vegetables  # Union of sets
intersection_result = fruits & vegetables  # Intersection of sets
difference_result = fruits - vegetables  # Difference of sets

print("Union:", union_result)
print("Intersection:", intersection_result)
print("Difference:", difference_result)

