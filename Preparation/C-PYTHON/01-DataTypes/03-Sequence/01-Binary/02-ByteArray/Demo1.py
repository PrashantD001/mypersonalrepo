

# Bytearray example
mutable_byte_data = bytearray(b'world')
print(mutable_byte_data)

# Modifying bytes in a bytearray
mutable_byte_data[0] = ord('W')
mutable_byte_data.append(ord('!'))
print(mutable_byte_data)

# Converting bytearray back to bytes
immutable_byte_data = bytes(mutable_byte_data)
print(immutable_byte_data)

