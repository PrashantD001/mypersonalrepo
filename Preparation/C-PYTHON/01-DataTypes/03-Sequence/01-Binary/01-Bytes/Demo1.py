


# Bytes example
byte_data = b'hello'
print(byte_data)

# Accessing individual bytes
first_byte = byte_data[0]
print("First byte:", first_byte)
print("First byte:", type(first_byte))
print("byte_data :", type(byte_data))


# Iterating through bytes
for byte in byte_data:
    print(byte)

