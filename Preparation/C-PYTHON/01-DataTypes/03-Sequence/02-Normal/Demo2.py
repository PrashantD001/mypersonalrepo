# List example
numbers = [1, 2, 3, 4, 5]
print(numbers)

# Accessing elements in a list
first_number = numbers[0]
print("First number:", first_number)

# Modifying elements in a list
numbers[2] = 10
print("Modified list:", numbers)

# Adding elements to a list
numbers.append(6)
numbers.insert(0, 0)
print("Updated list:", numbers)

