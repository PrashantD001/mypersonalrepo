# Tuple example
coordinates = (3, 5)
print(coordinates)

# Accessing elements in a tuple
x_coord = coordinates[0]
y_coord = coordinates[1]
print("X-coordinate:", x_coord)
print("Y-coordinate:", y_coord)

