# String example
message = "Hello, Python!"
print(message)

# Accessing characters in a string
first_char = message[0]
print("First character:", first_char)

# Slicing a string
substring = message[7:13]
print("Substring:", substring)

# String concatenation
greeting = "Hello"
subject = "Python"
full_message = greeting + ", " + subject + "!"
print(full_message)

