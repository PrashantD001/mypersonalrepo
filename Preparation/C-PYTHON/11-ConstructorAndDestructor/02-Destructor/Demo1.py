


class Demo:

    def __init__(self):
        print("In Constructor ")
        

    def info(self):
        print("In Info")

    def __del__(self):
        print("Deleting Object ")


print("Start")
obj1 = Demo()
obj3 = obj1
print(obj1)
#del obj1
#del obj3
#obj1.info()
#del obj1
obj2 = Demo()
print("End")
