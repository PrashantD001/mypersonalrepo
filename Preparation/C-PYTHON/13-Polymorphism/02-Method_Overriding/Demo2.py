
class Parent:
    x = 10

    @classmethod
    def show(cls):
        print("Parent Show : ",cls.x)

class Child(Parent):
    x = 20
    def show(self):
        print("Child Show : ",self.x)
        super().show()

obj = Child()
obj.show()
