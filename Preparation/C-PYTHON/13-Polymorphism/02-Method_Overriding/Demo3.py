
class Parent:
    x = 10

    @classmethod
    def show(cls):
        print("Parent Show : ",cls.x)

class Child(Parent):
    def __init__(self):
        self.x = 20
    def show(self):
        print("Child Show : ",self.x)
        super().show()

obj = Child()
obj.show()
