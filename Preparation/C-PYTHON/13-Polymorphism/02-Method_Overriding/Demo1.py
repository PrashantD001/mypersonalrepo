
class Parent:
    x = 10
    def show(self):
        print("Parent Show : ",self.x)

class Child(Parent):
    x = 20
    def show(self):
        print("Child Show : ",self.x)
        super().show()

obj = Child()
obj.show()
