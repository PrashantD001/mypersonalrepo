
class Parent:
    x = 10

    @classmethod
    def show(cls):
        print("Parent Show : ",cls.x)

class Child(Parent):
    x = 20

    @classmethod
    def show(self,x,y):
        print("Child Show : ",self.x)
        super().show()

obj = Child()
obj.show(10,20)
