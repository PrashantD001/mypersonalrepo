

class Demo:
    x =10
    
    def __init__(self,x):
        self.x = x

    def __add__(self,obj):
        return self.x + obj.x

    def __sub__(self,obj):
        return self.x - obj.x

    def __mul__(self,obj):
        return self.x * obj.x

    def __truediv__(self,obj):
        return self.x / obj.x

    def __mod__(self,obj):
        return self.x / obj.x

obj1 = Demo(20)
obj2 = Demo(10)

print(obj1+obj2)
print(obj1-obj2)
print(obj1*obj2)
print(obj1/obj2)
print(obj1%obj2)
