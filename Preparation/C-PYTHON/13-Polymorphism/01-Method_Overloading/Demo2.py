

#           2nd Way

from multipledispatch import dispatch

class Demo:

    @dispatch(int,int)
    def add(self,x,y):
        return x+y

    @dispatch(int,int,int)
    def add(self,x,y,z):
        return x+y+z

    @dispatch(float,float)
    def add(self,x,y):
        return x+y
    
    @dispatch(float,float,float)
    def add(self,x,y,z):
        return x+y+z


obj = Demo()

print(obj.add(10,20))
print(obj.add(10,20,30))
print(obj.add(10.10,20.20))
print(obj.add(10.10,20.20,30.30))
