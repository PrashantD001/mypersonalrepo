

def Palin(fun):
    print("Palin : 1")
    #fun(10000)
    def Inner(x):
        print("Inner in Palin : 1 ")
        temp1=x
        print("Temp 1 : ",temp1)
        temp2 = fun(x)
        print("Temp 2 : ",temp2)
        if(temp1==temp2):
            return "Given Number is Palindrome "
        else:    
            return "Given Number is Not Palindrome "
        
    return Inner

def Rev(fun):
    print("Rev : 1")
    #fun(20000)
    def Inner(x):
        print("Inner in Rev : 1 ")
        temp1=0
        num=0
        print(x)
        fun(x)
        while(x!=0):
            rem=x%10
            x=x//10
            num=num*10+rem
            print("Num : ",num)
            #print(num)
        return num
    return Inner

@Palin
@Rev
def PrintNum(x):
    print("In Print Num : 1 ")
    return x


a=int(input("Enter the Number : "))

print(PrintNum(a))



