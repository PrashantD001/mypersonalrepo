

#           228



def rev(fun):
    print("In Rev")
    def Inner(*args):
        print("In Inner Rev : ",args)
        str2 = fun(*args)
        return str2[::-1]
    return Inner

def Concat(fun):
    print("IN Concat")
    def Inner(*args):
        print("In Inner concat",args)
        retTup=fun(*args)
        str2 = ""
        for x in retTup:
            str2+=x
        return str2
    return Inner

@rev
@Concat
def PrintStr(str1,str2):
    print("IN PrintStr ")
    return str1,str2

print(PrintStr("Shashi","Bagal"))



