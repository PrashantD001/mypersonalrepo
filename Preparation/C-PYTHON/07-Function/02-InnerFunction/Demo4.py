

def outer(num):
    print("In Outer ",num)
    def inner(num):
        print("In Inner ",num)
    return inner


ans = outer(10)

ans(50)
ans()
