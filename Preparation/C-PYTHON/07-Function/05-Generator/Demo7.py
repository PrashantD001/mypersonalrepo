

def fun():
    print("Start - Fun-1")
    yield 10
    print("Start - Fun-2")
    yield 10
    print("Start - Fun-3")
    yield 10
    print("Start - Fun-4")
    yield 10
    print("End - Fun-1")
    yield 10
    print("End - Fun-2")
    print("End - Fun-3")
    yield 10
    print("End - Fun-4")

x= fun()
print(x)

y= fun()
print(y)


for i in fun():
    print(i)
    print("---*----")
