

def fun():
    print("In Fun - 1 ")
    yield 10
    #return 10
    print("In Fun - 2 ")
    yield 20
    print("In Fun - 3 ")
    yield 30
    print("In Fun - 4 ")
    yield 40
    print("In Fun - 5 ")
    yield 50
    print("In Fun - 6 ")

print(fun())
for i in fun():
    print(i)
