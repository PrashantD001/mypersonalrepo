def countdown(n):
    while n > 0:
        yield n
        n -= 1

# Create a generator object
counter = countdown(5)

print(counter)

# Iterating over the generator using a for loop
for num in counter:
    print(type(num))
    print(num)

