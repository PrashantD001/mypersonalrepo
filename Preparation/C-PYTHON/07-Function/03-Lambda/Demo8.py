

x,y,z = [10,20,30]
print(x)
print(y)
print(z)

tup = (10,20,30)
print(type(tup))
a,b,c=tup
print(a)
print(b)
print(c)

lis = [10,20,30]
print(type(lis))
a,b,c=lis
print(a)
print(b)
print(c)

se = {10,20,30}
print(type(se))
a,b,c=se
print(a)
print(b)
print(c)
