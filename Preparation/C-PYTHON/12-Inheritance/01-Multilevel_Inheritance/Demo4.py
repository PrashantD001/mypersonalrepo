

class Parent:
    cp = 10 
    def __init__(self):
        self.ip = 20
        print("Parent Constructor : ",self)

    def fun(self):
        print("In Fun : ",self)

    @classmethod
    def cfun(cls):
        print("In Classmethod cFun : ",cls)

class Child(Parent):
    cc = 30 
    def __init__(self):
        #super().__init__()
        self.ic = 40
        print("Child Constructor : ",self)

    def gun(self):
        print("In Gun : ",self)
        #print("ip : ",self.ip)
        print("ic : ",self.ic)
    
    @classmethod
    def cgun(cls):
        print("In Classmethod cGun : ",cls)


print("Start")
c = Child()
c.gun()
c.cgun()
c.fun()
c.cfun()
Child.cgun();
Child.cfun();
print(c.ic)
print(c.cc)
print(c.ip)
print(c.cp)
#p = Parent()
#p.fun()
print("End ")
