
class Demo :

    def __init__(self):
        self.x =10
        self._y =20
        self.__z =30

print("Start")
obj = Demo()
print(obj.x)
print(obj._y)
#print(obj.__z)
print(obj._Demo__z)
print(dir(Demo))
print(dir(obj))
print("End")
