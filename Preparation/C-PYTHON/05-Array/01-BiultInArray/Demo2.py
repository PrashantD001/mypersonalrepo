
import array

arr1=array.array('i',[10,20,30])
arr2=array.array('i',[10,20,30])

for ele in arr1:
    print(ele)

for ele in arr1:
    print(ele)

print(arr1[0])
print(arr1[1])
print(arr1[2])

print(arr2[0])
print(arr2[1])
print(arr2[2])

print(id(arr1))
print(id(arr2))

print(id(arr1[0]))
print(id(arr2[0]))
