

import array

arr1 = array.array('i',[10,20,30])

print(arr1)
print(id(arr1))
print(id(arr1[0]))
print(type(arr1))
print(type(arr1[0]))


l = [1,2,3,4,5,6,7,8]

arr = array.array('i',l)
print(arr)
