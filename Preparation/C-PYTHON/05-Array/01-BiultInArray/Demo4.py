
#                   Deep Copy

import array

arr1=array.array('i',[10,20,30])

print(arr1)

arr2=array.array(arr1.typecode,[ele for ele in arr1])
print(arr2)

print(id(arr1))
print(id(arr2))

print(id(arr1[2]))
print(id(arr2[2]))

arr2[2]=50

print(arr1)
print(arr2)

print(id(arr1[2]))
print(id(arr2[2]))
