
#               Slicing

import array

iarr=array.array('i',[10,20,30,40,50,60,70,80,90,100])

ans = iarr[0::]
print(ans)

ans = iarr[0:7:2]
print(ans)

ans = iarr[-8:-4:]
print(ans)

ans = iarr[-8:-4:2]
print(ans)

ans = iarr[0:9:-2]
print(ans)

ans = iarr[-5:-8:]
print(ans)

