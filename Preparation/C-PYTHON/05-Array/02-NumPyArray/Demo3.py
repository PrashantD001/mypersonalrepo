
import numpy

# 1.array()
print("--1--")
arr = numpy.array([10,'Rahul',30,40.5,50])
print(arr)
print(type(arr))

# 2.linspace()
print("--2--")
arr1=numpy.linspace(1,21,10)
print(arr1)
arr2=numpy.linspace(1,20,)
print(arr2)
print(type(arr2))

# 3.arrange()
print("--3--")
arr3=numpy.arange(10,20)
print(arr3)
arr4=numpy.arange(10,20,2)
print(arr4)

arr5=numpy.arange(10,20,-2)
print(arr5)
print(arr5.ndim)
print(type(arr5))

# 4.zeros()
print("--4--")
arr6=numpy.zeros(5)
print(arr6)
arr7=numpy.zeros(5,int)
print(arr7)
print(type(arr7))

# 5.once()
print("--5--")
arr8=numpy.ones(5)
print(arr8)
arr9=numpy.ones(5,int)
print(arr9)
print(type(arr9))
