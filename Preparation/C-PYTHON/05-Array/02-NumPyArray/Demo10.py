

import numpy

arr=numpy.matrix([[1,2,3],[4,5,6],[7,8,9]])
print(arr)

print(arr.diagonal())
print(arr.sort())

arr1=arr.transpose()
print(arr1)

# String Splitter
arr3=numpy.matrix('1,2,3;4,5,6;7,8,9')
print(arr3)


arr4=numpy.matrix([[21,3,15],[9,5,13],[7,4,14]])
print(numpy.sort(arr4))


print(arr4.sum())
print(arr4.max())
print(arr4.min())
print(arr4.mean())
