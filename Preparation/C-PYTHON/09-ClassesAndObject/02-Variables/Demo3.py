
# Making Variable Private which cannot be access in Ouside of the Class

class Demo:
 
    __x = 100

    def __init__(self,var):
        self.__var = var

    #@staticmethod
    #def fun():
    def fun(self):
        #print(self.var)             # Atribute Error
       # print(__x)
        #print(__var)
        print(__var)
        print(__x)
        print(self.__var)
        print(self.__x)

x=int(input("Enter the Value : "))
obj=Demo(x)
obj.fun()
#print(obj.__var)                   # Atribute Error
