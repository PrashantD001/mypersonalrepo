

#               INSTANCE METHOD

class Demo:

    x = 100 
    xy = 500 
    def __init__(self):
        print("In Constructor ")
        self.x = 10
        self.xyz = 10

    def fun(self):
        print("start Fun ")
        x = 20 
        print("self.x : ",self.x)
        print("x : ",x)
        print("Demo.x : ",Demo.x)
        print("xy : ",Demo.xy)
        #gun()
        self.gun()
        #self.gun()
        #Demo.gun()
        print("end Fun ")

    @classmethod
    def gun(cls):
        print("In Gun ",x)
        print("In Gun ",cls.x)
        #print("In Gun ",cls.xyz)


print("Start ")

Demo().fun();
Demo().gun()
Demo.gun()
print("End ")
