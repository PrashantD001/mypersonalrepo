

class myclassmethod():
    def __init__(self,method):
        print("In Constructor of MyclassDecorator : self : ",self," method : ",method)
        self.method=method
    def __get__(self,instance,cls):
        x = lambda *args, **kw: self.method(cls,*args,**kw)
        print("In __get__ of MyclassDecorator : self : ",self,"instance : ",instance," cls : ",cls)
        print("X : ",x)
        return x


class MyClass():
    @myclassmethod
    def method(cls):
        print(cls)


print("------------------------------------1---------------------------------")
m=MyClass()
print("------------------------------------2---------------------------------")
m.method()
print(m.method())

print("------------------------------------3---------------------------------")
MyClass.method()

print("------------------------------------4---------------------------------")
