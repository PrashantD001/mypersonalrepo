

class myclassmethod():
    def __init__(self,method):
        self.method=method
    def __get__(self,instance,cls):
        return lambda *args, **kw: self.method(cls,*args,**kw)


class MyClass():
    @myclassmethod
    def method(cls):
        print(cls)


m=MyClass()
m.method()

MyClass.method()

