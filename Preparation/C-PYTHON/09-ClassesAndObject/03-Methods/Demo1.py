
#               CLASS METHODS


class Cafe:
    menucard = 1
    def __init__(self,dish):
        self.dish = dish
        self.PrintData()

    @classmethod
    def PrintData(cls):
        print(cls)
        cls.menucard = 0;
        #print(cls.dish)
    
obj1 = Cafe("Burger")
obj2 = Cafe("Large Fries")

print(obj1.menucard)
print(obj2.menucard)

obj1.PrintData()
Cafe.PrintData()
print(obj1.menucard)
print(obj2.menucard)

