
# to make method class method there will be always decorator 

class Cric:
    format = "T-20"

    def __init__(self,name,JerNo):
        self.name = name
        self.JerNo = JerNo

    #@classmethod           # AttributeError
    def Display(cls):                       # cls --> self      # we can change name of self
        print(cls)
        print(cls.name)
        print(cls.JerNo)

player1 = Cric("Dhoni",7)
player2 = Cric("Virat",18)

player1.Display()
