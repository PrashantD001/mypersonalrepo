

class Demo:

    def myClassMethod(fun):
        def inner(*args):
            if(len(args) !=0):
                fun(args[0].__class)
            else:
                fun(fun.__class)
        return inner

    @myClassMethod
    def fun(cls):
        print(cls)
        #print(cls.matchformat)


Demo.fun()
Demo().fun()
