
#           Class Method 

class Demo:
    x = 100
    def __init__(self):
        y = 200 

    @classmethod
    def fun(cls):
        print("Start Fun")
        print(cls)
        print("Class Variable : ",cls.x)
        #print("Instance Variable :",cls.y)     # Error for trying to access Instance method 
        cls.sun()
        #cls.Gun()      # call to instaNCE METHOD fROM cLASS mETHOD gIVE ERROR MISSING oNE pARAMETER 
        print("End fun")
    
    @classmethod
    def sun(cls):
        print("In Class Method Sun " )


    def Gun(self):
        print(self)
        print("In Instance Gun")

print("Start ")
obj = Demo()
obj.Gun()
Demo.fun()
print("End ")
