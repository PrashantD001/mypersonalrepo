

#           STATIC METHOD



class Temp:
    a = 40
    def __init__(self):
        self.b = 50

class Demo:
    x = 10  
    def __init__(self):
        print("In Costructor")
        self.y = 20

    @staticmethod
    def fun(obj):
        print("In Static ")
        print(Demo.x)
        #print(Demo.y)

        print(obj.a)
        print(obj.b)

print("Start")
t = Temp()
Demo.fun(t);
print("Medium")
Demo().fun(t)
print("End")

#fun()
