
# there are 6 way to creat class


# Method 1
class A(object):
    def __init__(self):
        pass

a=A()
print(a)


# Method 2
class B(object):
    def __call__(cls, *args, **kwards):
        return super(B,cls). __call__(cls, *args, **kwards)

b=B()
print(b)

# Method 3
class C(object):
    def __new__(cls, *args, **kwards):
        return super(C,cls). __new__(cls, *args, **kwards)
''' 
    def __init__(self, *args, **kwards):
        super(C,self). __init__(*args, **kwards)
'''
c=C()
print(c)


#    Three ways using Metaclass

# Method 4
class D(type):
    def __call__(cls, *args, **kwards):
        instance = super(D, cls).__call__(*args, **kwards)
        return instance

    def __init__(cls, name, base, attr):
        super(D, cls).__init__(name, base, attr)

class E(metaclass = D):
    pass

e=E()
print(e)


# Method 5
class F(type):
    def __call__(cls, *args, **kwards):
        instance = super(F, cls).__call__(*args, **kwards)
        return instance

    def __init__(cls, name, base, attr):
        super(F, cls).__init__(name, base, attr)

class G(metaclass = F):
    def __new__(cls, *args, **kwards):
        return super(G, cls).__new__(cls, *args, **kwards)

    def __init__(self, *args, **kwards):
        super(G, self).__init__(*args, **kwards)

g=G()
print(g)

    

























    
