
import java.util.*;

class Demo{

    	public static void main(String[] args) {
	        int[] numbers = {1, 2, 3, 4, 5};

        	// Iterating over an array
	        System.out.println("Iterating over an array:");
        	for (int number : numbers) {
	            System.out.println(number);
        	}

	        System.out.println();

        	// Iterating over a collection (ArrayList)
	        List<String> fruits = new ArrayList<>();
        	fruits.add("Apple");
	        fruits.add("Banana");
        	fruits.add("Orange");

        	System.out.println("Iterating over a collection:");
	        for (String fruit : fruits) {
        	    System.out.println(fruit);
        	}
    	}
}
