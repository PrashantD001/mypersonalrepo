

class Temp{

	Temp(){
	
		System.out.println("Temp Constructor ");
	}
}


class Parent{

	Parent(){
	
		System.out.println("In Parent Constrctor");
	}
}













class Demo{

	int x = 30;

	Demo() {
	
		this(x);	// Error : error: cannot reference x before supertype constructor has been called
		
		System.out.println("In Demo Constructor - No Para");
	}

	Demo(int x ) {
	
		System.out.println("In Demo Constructor -  In Para");
	}


	public static void main(String[] pn){
	

		Demo obj = new Demo();
	}
}







