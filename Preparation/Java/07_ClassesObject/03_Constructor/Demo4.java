

class Temp{
	Temp(){	
		System.out.println("Temp Constructor ");
	}
}
class Parent{
	Parent(){
		System.out.println("In Parent Constrctor");
	}
	{
		System.out.println("Parent Instance Block");
	}
}
class Demo extends Parent{
	int x = 30;

	Temp t = new Temp();
	Demo() {
		//this(x);	// Error : error: cannot reference x before supertype constructor has been called
		System.out.println("In Demo Constructor - No Para");
	}
	{
		System.out.println("In Demo Instance Block ");
	}

	Demo(int x ) {
	
		System.out.println("In Demo Constructor -  In Para");
	}


	public static void main(String[] pn){
	

		Demo obj = new Demo();
	}
}







