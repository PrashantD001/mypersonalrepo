

class Demo{

	Demo(){
	
		this(10);
		System.out.println("In Normal Constructor");
	}
	
	Demo(int x ){
	
		System.out.println("In Para Constructor ");
	}

	public static void main(String[] pn){
	
		Demo obj = new Demo();

		System.out.println("Main obj : "+obj);
	}
}

