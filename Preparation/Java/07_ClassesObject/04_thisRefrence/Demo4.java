

class Demo{

	Demo(){
	
		System.out.println("Constructor This : "+this);
	}

	void fun(){
	
		System.out.println("Fun This : "+this);
	}

	public static void main(String[] pn){
	
		Demo obj = new Demo();
		obj.fun();

		System.out.println("Main obj : "+obj);
	}
}

