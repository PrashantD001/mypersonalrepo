

class Demo{

	Demo(){


		System.out.println("In Normal Constructor");
		super();
		this(10);
	}
	
	Demo(int x ){
	
		System.out.println("In Para Constructor ");
	}

	public static void main(String[] pn){
	
		Demo obj = new Demo();

		System.out.println("Main obj : "+obj);
	}
}

