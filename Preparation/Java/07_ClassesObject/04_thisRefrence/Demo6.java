
class Temp{

	Temp(){
	
		System.out.println("Temp - Constructor");
	}

	
	static{
		
		System.out.println("Temp - Static");
	}
}

class Parent {

	static{
	
		System.out.println("Parent - Static");
	}
}

class Demo extends Parent{

	static Temp t = new Temp();

	static{
		System.out.println("Demo - Static");
	}

	public static void main(String[] pn){
	

	}
}
