

class Demo{

	{
	
		System.out.println("In Instance Block ");
	}

	Demo(){
	
		System.out.println("In Constructor ");
	}

	public static void main(String[] pn){
	
		System.out.println("In Main - 1 ");

		Demo obj = new Demo();

		System.out.println("In Main - 2 ");
	}
}
