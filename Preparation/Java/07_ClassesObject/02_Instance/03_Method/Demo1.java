

class Demo{

	void fun(){
	
		System.out.println("In Fun");
	}
	public static void main(String[] pn){
	
		System.out.println("In Main - 1");

		Demo obj = new Demo();

		obj.fun();

		//fun();			// Error
		//Demo.fun();			// Error

		System.out.println("In Main - 2");
	}
}
