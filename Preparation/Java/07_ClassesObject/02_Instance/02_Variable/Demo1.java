

class Temp{

	Temp(){
	
		System.out.println("In Temp Constructor ");
	}
}


class Demo{

	Temp obj = new Temp();

	{
	
		System.out.println("In Instance Block ");
	}

	Demo(){
	
		System.out.println("In Constructor ");
	}

	public static void main(String[] pn){
	
		System.out.println("In Main - 1 ");

		Demo obj = new Demo();

		System.out.println("In Main - 2 ");
	}
}


