


class Demo{

	static int x;

	static{
	
		System.out.println("IN Static Block - 1 ");
		x = 20;						// Assignment Not Declaration 
		int y =30 ;
		System.out.println("IN Static Block - 2 ");
	}

	public static void main(String[] pn){
	
		System.out.println("In Main x : "+x);
		//System.out.println("In Main x : "+y);	// Cannot find Symbol
	}
}
