


class Demo{

	static int x = 10;

	static{
	
		System.out.println("IN Static Block - 1 ");
		x = 20;
		System.out.println("IN Static Block - 2 ");
	}

	public static void main(String[] pn){
	
		System.out.println("In Main x : "+x);
	}
}
