

class Demo{

	static int x = 10 ;
	int y = 20 ;

	static void fun(){
	
		System.out.println("In Fun - 1 ");
		System.out.println("x : "+x);
		System.out.println("y : "+y);		// error : non static variable cannot be referenced from static context
		System.out.println("In Fun - 2 ");
		
	}

	public static void main(String[] pn){
	
		System.out.println("In Main - 1 ");
		fun();
		System.out.println("In Main - 2 ");
	}
}
