

class Demo{

	public static void main(String[] pn){
	
        	// Create a StringBuilder with initial capacity of 16
        	StringBuilder stringBuilder = new StringBuilder("Hello");

        	// Append characters to the StringBuilder
        	stringBuilder.append(", World!");
        	System.out.println("After append: " + stringBuilder);

        	// Insert characters at a specific index
        	stringBuilder.insert(5, " Java");
        	System.out.println("After insert: " + stringBuilder);

        	// Replace characters in the StringBuilder
        	stringBuilder.replace(0, 5, "Hi");
        	System.out.println("After replace: " + stringBuilder);

        	// Delete characters from the StringBuilder
        	stringBuilder.delete(0, 3);
        	System.out.println("After delete: " + stringBuilder);

        	// Reverse the StringBuilder
        	stringBuilder.reverse();
        	System.out.println("After reverse: " + stringBuilder);

        	// Get the character at a specific index
        	char charAtIndex = stringBuilder.charAt(5);
        	System.out.println("Character at index 5: " + charAtIndex);

        	// Get the length of the StringBuilder
        	int length = stringBuilder.length();
        	System.out.println("Length of the StringBuilder: " + length);

        	// Get the capacity of the StringBuilder
        	int capacity = stringBuilder.capacity();
        	System.out.println("Capacity of the StringBuilder: " + capacity);

        	// Set the length of the StringBuilder
        	stringBuilder.setLength(5);
        	System.out.println("After setLength: " + stringBuilder);

        	// Get the substring from the StringBuilder
        	String substring = stringBuilder.substring(2);
        	System.out.println("Substring from index 2: " + substring);

        	// Clear the StringBuilder
        	stringBuilder.setLength(0);
        	System.out.println("After clearing the StringBuilder: " + stringBuilder);

	}
}
