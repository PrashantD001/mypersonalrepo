

class Demo{

	public static void main(String[] pn){
	
        	String str = "Hello, World!";

        	// Length of the string
        	int length = str.length();
        	System.out.println("Length of the string: " + length);

        	// Concatenation
        	String anotherString = " Welcome";
        	String concatenatedStr = str.concat(anotherString);
        	System.out.println("Concatenated string: " + concatenatedStr);

        	// Character extraction
        	char charAtIndex = str.charAt(0);
        	System.out.println("Character at index 0: " + charAtIndex);

        	// Substring
        	String substring = str.substring(0, 5);
        	System.out.println("Substring from index 0 to 4: " + substring);

        	// Index of a character or substring
        	int indexOfO = str.indexOf('o');
        	int indexOfWorld = str.indexOf("World");
        	System.out.println("Index of 'o': " + indexOfO);
        	System.out.println("Index of 'World': " + indexOfWorld);

        	// Last index of a character or substring
        	int lastIndexOfO = str.lastIndexOf('o');
        	System.out.println("Last index of 'o': " + lastIndexOfO);

        	// Replace characters or substrings
        	String replacedStr = str.replace('o', 'a');
        	System.out.println("Replaced string: " + replacedStr);

        	// Convert to uppercase and lowercase
        	String uppercaseStr = str.toUpperCase();
        	String lowercaseStr = str.toLowerCase();
        	System.out.println("Uppercase string: " + uppercaseStr);
        	System.out.println("Lowercase string: " + lowercaseStr);

        	// Trimming whitespace
        	String stringWithSpaces = "    Spaces     ";
        	String trimmedStr = stringWithSpaces.trim();
        	System.out.println("Trimmed string: " + trimmedStr);

        	// Checking if the string starts/ends with a given prefix/suffix
        	boolean startsWithHello = str.startsWith("Hello");
        	boolean endsWithExclamation = str.endsWith("!");
        	System.out.println("Starts with 'Hello': " + startsWithHello);
        	System.out.println("Ends with '!': " + endsWithExclamation);

        	// Splitting a string into an array based on a delimiter
        	String sentence = "This is a sentence.";
        	String[] words = sentence.split(" ");
        	System.out.print("Split sentence: ");
        	for (String word : words) {
        	    System.out.print(word + " ");
        	}
        	System.out.println();

        	// Checking if a string is empty or null
        	String emptyString = "";
        	boolean isEmpty = emptyString.isEmpty();
        	System.out.println("Is empty string empty?: " + isEmpty);

        	// Checking if two strings are equal (case-sensitive)
        	String str1 = "Hello";
        	String str2 = "hello";
        	boolean isEqualCaseSensitive = str1.equals(str2);
        	System.out.println("Are str1 and str2 equal (case-sensitive)?: " + isEqualCaseSensitive);

        	// Checking if two strings are equal (case-insensitive)
        	boolean isEqualCaseInsensitive = str1.equalsIgnoreCase(str2);
        	System.out.println("Are str1 and str2 equal (case-insensitive)?: " + isEqualCaseInsensitive);

	}
}
