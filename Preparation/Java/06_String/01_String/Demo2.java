


class Demo{

	public static void main(String[] pn){
	
        	String str1 = "Hello";
        	String str2 = new String("Hello");
        	String str3 = "Hello";

        	// Check if str1 and str2 point to the same object
        	System.out.println("str1 and str2 are the same object: " + (str1 == str2));

        	// Call intern() on str2 to add it to the string pool
        	str2 = str2.intern();

        	// Check if str1 and str2 point to the same object after interning str2
        	System.out.println("str1 and str2 are the same object after interning str2: " + (str1 == str2));

        	// Check if str1 and str3 point to the same object
        	System.out.println("str1 and str3 are the same object: " + (str1 == str3));

	}
}
