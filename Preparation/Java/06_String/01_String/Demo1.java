

class Demo{

	public static void main(String[] pn){
	
        	// Using string literals
        	String str1 = "Hello";
        	String str2 = "Hello";

        	// Using a string constant (new String())
        	String str3 = new String("Hello");
        	String str4 = new String("Hello");

        	// Comparing references to check if strings point to the same object
        	boolean areEqualLiteral = str1 == str2;
        	boolean areEqualConstant = str3 == str4;
        	System.out.println("Strings str1 and str2 are equal (using literals): " + areEqualLiteral);
        	System.out.println("Strings str3 and str4 are equal (using constants): " + areEqualConstant);

        	// Using the equals method to compare the contents of two strings
        	boolean areEqualContent1 = str1.equals(str2);
        	boolean areEqualContent2 = str3.equals(str4);
        	System.out.println("Strings str1 and str2 are equal (using equals): " + areEqualContent1);
        	System.out.println("Strings str3 and str4 are equal (using equals): " + areEqualContent2);

	}
}
