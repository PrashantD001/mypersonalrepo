

class Demo{

	public static void main(String[] pn){

        	// Create a StringBuffer with initial capacity of 16
        	StringBuffer stringBuffer = new StringBuffer("Hello");

        	// Append characters to the StringBuffer
        	stringBuffer.append(", World!");
        	System.out.println("After append: " + stringBuffer);

        	// Insert characters at a specific index
        	stringBuffer.insert(5, " Java");
        	System.out.println("After insert: " + stringBuffer);

        	// Replace characters in the StringBuffer
        	stringBuffer.replace(0, 5, "Hi");
        	System.out.println("After replace: " + stringBuffer);

        	// Delete characters from the StringBuffer
        	stringBuffer.delete(0, 3);
        	System.out.println("After delete: " + stringBuffer);

        	// Reverse the StringBuffer
        	stringBuffer.reverse();
        	System.out.println("After reverse: " + stringBuffer);

        	// Get the character at a specific index
        	char charAtIndex = stringBuffer.charAt(5);
        	System.out.println("Character at index 5: " + charAtIndex);
	
        	// Get the length of the StringBuffer
        	int length = stringBuffer.length();
        	System.out.println("Length of the StringBuffer: " + length);

        	// Get the capacity of the StringBuffer
        	int capacity = stringBuffer.capacity();
        	System.out.println("Capacity of the StringBuffer: " + capacity);

        	// Set the length of the StringBuffer
        	stringBuffer.setLength(5);
        	System.out.println("After setLength: " + stringBuffer);

        	// Get the substring from the StringBuffer
        	String substring = stringBuffer.substring(2);
        	System.out.println("Substring from index 2: " + substring);

        	// Clear the StringBuffer
        	stringBuffer.setLength(0);
        	System.out.println("After clearing the StringBuffer: " + stringBuffer);
    
	}
}
