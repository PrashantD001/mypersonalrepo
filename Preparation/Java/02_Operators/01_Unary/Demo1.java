

// Unary Operator

class Demo{

	public static void main(String[] pn) {
        	int x = 10;
	        int result;

        	// Unary plus operator (+)
	        result = +x;
        	System.out.println("Unary plus: " + result);

	        // Unary minus operator (-)
        	result = -x;
	        System.out.println("Unary minus: " + result);

        	// Increment operator (++)
	        int a = 5;
	        int b = ++a; // Pre-increment
	        System.out.println("Pre-increment: a = " + a + ", b = " + b);

        	int c = 5;
	        int d = c++; // Post-increment
        	System.out.println("Post-increment: c = " + c + ", d = " + d);

		// Decrement operator (--)
        	int e = 5;
	        int f = --e; // Pre-decrement
        	System.out.println("Pre-decrement: e = " + e + ", f = " + f);

	        int g = 5;
        	int h = g--; // Post-decrement
	        System.out.println("Post-decrement: g = " + g + ", h = " + h);

        	// Logical complement operator (!)
	        boolean flag = true;
        	boolean negatedFlag = !flag;
	        System.out.println("Logical complement: " + negatedFlag);
	
	}

}
