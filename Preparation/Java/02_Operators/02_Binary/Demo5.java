
// BitWise Operator


class Demo{

	public static void main(String[] pn){
	
		int a = 5;  // 0101 in binary
        	int b = 3;  // 0011 in binary

	        // Bitwise AND
        	int resultAnd = a & b;
	        System.out.println("Bitwise AND: " + resultAnd);

        	// Bitwise OR
	        int resultOr = a | b;
        	System.out.println("Bitwise OR: " + resultOr);
		// Bitwise XOR
        	int resultXor = a ^ b;
	        System.out.println("Bitwise XOR: " + resultXor);

	        // Bitwise NOT
        	int resultNotA = ~a;
	        System.out.println("Bitwise NOT for a: " + resultNotA);

        	// Left shift
        	int leftShift = a << 1;
        	System.out.println("Left Shift: " + leftShift);

        	// Right shift
        	int rightShift = a >> 1;
        	System.out.println("Right Shift: " + rightShift);
		// Unsigned right shift
        	int unsignedRightShift = a >>> 1;
        	System.out.println("Unsigned Right Shift: " + unsignedRightShift);

	}
}
