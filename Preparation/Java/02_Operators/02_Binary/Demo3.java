

// Logical Operator 


class Demo{

	public static void main(String[] pn) {
        	boolean a = true;
	        boolean b = false;

        	// Logical AND
	        boolean resultAnd = a && b;
        	System.out.println("Logical AND: " + resultAnd);

	        // Logical OR
        	boolean resultOr = a || b;
	        System.out.println("Logical OR: " + resultOr);


	}
}
