
// Relational Operator


class Demo{

    public static void main(String[] pn) {
        int a = 10;
        int b = 5;

        // Equality
        boolean isEqual = a == b;
        System.out.println("Equal: " + isEqual);

        // Inequality
        boolean isNotEqual = a != b;
        System.out.println("Not Equal: " + isNotEqual);

        // Greater than
        boolean isGreaterThan = a > b;
        System.out.println("Greater Than: " + isGreaterThan);

        // Less than
        boolean isLessThan = a < b;
        System.out.println("Less Than: " + isLessThan);

        // Greater than or equal to
        boolean isGreaterThanOrEqual = a >= b;
        System.out.println("Greater Than or Equal To: " + isGreaterThanOrEqual);

        // Less than or equal to
        boolean isLessThanOrEqual = a <= b;
        System.out.println("Less Than or Equal To: " + isLessThanOrEqual);
    }
}
