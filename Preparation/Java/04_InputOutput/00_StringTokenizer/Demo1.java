

import java.io.*;
import java.util.*;

class Demo{

	public static void main(String[] pn){
	
        	String input = "Hello,World,Java";

        	// Create a StringTokenizer with a comma (",") delimiter
        	StringTokenizer tokenizer = new StringTokenizer(input, ",");

        	// hasMoreTokens() and nextToken()
        	while (tokenizer.hasMoreTokens()) {
            		String token = tokenizer.nextToken();
            		System.out.println(token);
        	}

        	// Reset the StringTokenizer
        	tokenizer = new StringTokenizer(input, ",");

        	// hasMoreElements() and nextElement()
        	while (tokenizer.hasMoreElements()) {
            		String token = (String) tokenizer.nextElement();
            		System.out.println(token);
        	}

        	// Reset the StringTokenizer
        	tokenizer = new StringTokenizer(input, ",");

        	// nextToken(String delimiter)
        	while (tokenizer.hasMoreTokens()) {
            		String token = tokenizer.nextToken(",");
        	    	System.out.println(token);
        	}

        	// Reset the StringTokenizer
        	tokenizer = new StringTokenizer(input, ",");

        	// nextToken(String delimiter) with multiple delimiters
        	while (tokenizer.hasMoreTokens()) {
            		String token = tokenizer.nextToken(",J");
        		System.out.println(token);
        	}

        	// Reset the StringTokenizer
        	tokenizer = new StringTokenizer(input, ",");

        	// countTokens()
        	int tokenCount = tokenizer.countTokens();
	        System.out.println("Total tokens: " + tokenCount);

	}
}
