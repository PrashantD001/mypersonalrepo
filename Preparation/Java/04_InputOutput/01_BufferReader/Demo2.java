
import java.io.*;

class Demo{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String str = br.readLine();

		System.out.println("Input String : "+str);

		char ch = (char)br.read();
		System.out.println("Input ch1 : "+ch);

		br.skip(1);

		char ch2 = (char)br.read();
		System.out.println("Input ch2 : "+ch2);
	}
}
