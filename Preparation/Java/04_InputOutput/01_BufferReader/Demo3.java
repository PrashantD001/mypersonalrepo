import java.io.*;

class Demo{

	public static void main(String[] pn)throws IOException{
	
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        	// Reading a String
	        System.out.print("Enter a string: ");
        	String strInput = bufferedReader.readLine();
	        System.out.println("You entered: " + strInput);

        	// Reading an integer
	        System.out.print("Enter an integer: ");
        	int intInput = Integer.parseInt(bufferedReader.readLine());
	        System.out.println("You entered: " + intInput);

        	// Reading a floating-point number
	        System.out.print("Enter a floating-point number: ");
        	double doubleInput = Double.parseDouble(bufferedReader.readLine());
	        System.out.println("You entered: " + doubleInput);

        	// Reading a character
	        System.out.print("Enter a character: ");
        	char charInput = bufferedReader.readLine().charAt(0);
	        System.out.println("You entered: " + charInput);

        	// Reading a boolean
	        System.out.print("Enter a boolean (true or false): ");
        	boolean boolInput = Boolean.parseBoolean(bufferedReader.readLine());
	        System.out.println("You entered: " + boolInput);

        	// Close the BufferedReader
	        bufferedReader.close();

	}
}
