

import java.util.*;


class Demo{

	public static void main(String[] pn){

	        Scanner scanner = new Scanner(System.in);

        	// Reading a String
	        System.out.print("Enter a string: ");
        	String strInput = scanner.nextLine();
	        System.out.println("You entered: " + strInput);

        	// Reading an integer
	        System.out.print("Enter an integer: ");
        	int intInput = scanner.nextInt();
	        System.out.println("You entered: " + intInput);

        	// Reading a floating-point number
	        System.out.print("Enter a floating-point number: ");
        	double doubleInput = scanner.nextDouble();
	        System.out.println("You entered: " + doubleInput);

        	// Reading a character
	        System.out.print("Enter a character: ");
        	char charInput = scanner.next().charAt(0);
	        System.out.println("You entered: " + charInput);

        	// Reading a boolean
	        System.out.print("Enter a boolean (true or false): ");
        	boolean boolInput = scanner.nextBoolean();
	        System.out.println("You entered: " + boolInput);

        	scanner.close();

	}
}
