
class Demo {


    public static void main(String[] pn) {
        
	// boolean
        boolean flag = true;
        System.out.println("boolean: " + flag);

        // byte
        byte myByte = 127;
        System.out.println("byte: " + myByte);
        System.out.println("Minimum value of byte: " + Byte.MIN_VALUE);
        System.out.println("Maximum value of byte: " + Byte.MAX_VALUE);
        System.out.println("Size of byte: " + Byte.SIZE + " bits");

        // short
        short myShort = 32767;
        System.out.println("short: " + myShort);
        System.out.println("Minimum value of short: " + Short.MIN_VALUE);
        System.out.println("Maximum value of short: " + Short.MAX_VALUE);
        System.out.println("Size of short: " + Short.SIZE + " bits");

	 // int
        int myInt = 2147483647;
        System.out.println("int: " + myInt);
        System.out.println("Minimum value of int: " + Integer.MIN_VALUE);
        System.out.println("Maximum value of int: " + Integer.MAX_VALUE);
        System.out.println("Size of int: " + Integer.SIZE + " bits");

        // long
        long myLong = 9223372036854775807L;
        System.out.println("long: " + myLong);
        System.out.println("Minimum value of long: " + Long.MIN_VALUE);
        System.out.println("Maximum value of long: " + Long.MAX_VALUE);
        System.out.println("Size of long: " + Long.SIZE + " bits");

        // float
        float myFloat = 3.14f;
        System.out.println("float: " + myFloat);
        System.out.println("Minimum value of float: " + Float.MIN_VALUE);
        System.out.println("Maximum value of float: " + Float.MAX_VALUE);
        System.out.println("Size of float: " + Float.SIZE + " bits");

        // double
        double myDouble = 3.14159;
        System.out.println("double: " + myDouble);
        System.out.println("Minimum value of double: " + Double.MIN_VALUE);
        System.out.println("Maximum value of double: " + Double.MAX_VALUE);
        System.out.println("Size of double: " + Double.SIZE + " bits");

        // char
        char myChar = 'A';
        System.out.println("char: " + myChar);
        System.out.println("Minimum value of char: " + Character.MIN_VALUE);
        System.out.println("Maximum value of char: " + Character.MAX_VALUE);
       System.out.println("Size of char: " + Character.SIZE + " bits");
    }
}
