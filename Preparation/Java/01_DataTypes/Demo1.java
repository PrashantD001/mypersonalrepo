

class Demo{

	public static void main(String[] pn){
	
		// Primitive DataTypes

		byte a = 1;	//	1 byte  range -2^8 to 2^8 - 1
		short b = 2;	//	2 byte	range -2^16 to 2^16 - 1
		int c = 3;	//	4 byte	range -2^32 to -2^32 - 1  == -2147483648 to 2147483647
		float d =  4.05f;//	4 byte
		long e = 5;	//	8 byte
		double f = 6;	//	8 byte

		char ch = 'A';	//	2 byte
		boolean bl = true; //	1 bit
	}
}
