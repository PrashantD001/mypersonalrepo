
//	Integer Cache Example

class Demo {
    public static void main(String[] pn) {
        Integer a = 100; // Value within the cache range
        Integer b = 100; // Value within the cache range

        Integer c = 200; // Value outside the cache range
        Integer d = 200; // Value outside the cache range

        System.out.println(a == b); // true (Both refer to the same cached object)
        System.out.println(c == d); // false (Different objects, as outside the cache range)
	}
}
