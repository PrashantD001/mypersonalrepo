

class Demo{

	public static void main(String[] pn){
	
		// Method 1: Initializing using array literals
        	int[][] array1 = { {1, 2, 3}, {4, 5, 6}, {7, 8, 9} };

        	// Method 2: Initializing using nested loops
        	int[][] array2 = new int[3][3];
        	for (int i = 0; i < array2.length; i++) {
            		for (int j = 0; j < array2[i].length; j++) {
                		array2[i][j] = i + j;
            		}
        	}

        	// Method 3: Initializing with specific values
        	int[][] array3 = new int[2][3];
        	array3[0] = new int[]{1, 2, 3};
        	array3[1] = new int[]{4, 5, 6};

        	// Method 4: Initializing using anonymous array
        	int[][] array4 = new int[][]{
            					{1, 2},
            					{3, 4, 5},
            					{6, 7, 8, 9}
        				};

        	// Printing the arrays
        	System.out.println("Array 1:");
        	printArray(array1);

        	System.out.println("Array 2:");
        	printArray(array2);

        	System.out.println("Array 3:");
        	printArray(array3);

        	System.out.println("Array 4:");
        	printArray(array4);
    	}

    	// Helper method to print the contents of a 2D array
	public static void printArray(int[][] array) {
        	for (int[] row : array) {
	            	for (int element : row) {
                		System.out.print(element + " ");
            		}
            		System.out.println();
        	}
        	System.out.println();
   	}
}
