
import java.lang.reflect.Array;

class Demo{

	public static void main(String[] pn){
	
		//	Ways with Initializer List

		int arr1[] = {10,20,30};		// Way-1
		int arr2[] = new int[]{10,20,30};	// Way-2

		//	Way with AssignMent

		int arr3[] = new int[3];		// Way-3
		arr3[0] = 10;
		arr3[1] = 20;
		arr3[2] = 30;

	}
}
