


class Demo{

    	public static void main(String[] args) {
        	// Method 1: Initializing using array literals
        	int[][][] array1 = {
            				{ {1, 2}, {3, 4} },
            				{ {5, 6}, {7, 8} }
        			};

        	// Method 2: Initializing using nested loops
        	int[][][] array2 = new int[2][2][2];
        	for (int i = 0; i < array2.length; i++) {
            		for (int j = 0; j < array2[i].length; j++) {
                		for (int k = 0; k < array2[i][j].length; k++) {
                    			array2[i][j][k] = i + j + k;
                		}
            		}
        	}

        	// Method 3: Initializing with specific values
        	int[][][] array3 = new int[2][2][2];
        	array3[0][0] = new int[]{1, 2};
        	array3[0][1] = new int[]{3, 4};
        	array3[1][0] = new int[]{5, 6};
        	array3[1][1] = new int[]{7, 8};

        	// Method 4: Initializing using anonymous array
        	int[][][] array4 = new int[][][]{
            				{ {1, 2}, {3, 4} },
            				{ {5, 6}, {7, 8} }
        				};

        	// Printing the arrays
        	System.out.println("Array 1:");
        	printArray(array1);

        	System.out.println("Array 2:");
        	printArray(array2);

        	System.out.println("Array 3:");
        	printArray(array3);

        	System.out.println("Array 4:");
        	printArray(array4);
    	}

    	// Helper method to print the contents of a 3D array
    	public static void printArray(int[][][] array) {
        	for (int[][] plane : array) {
            		for (int[] row : plane) {
                		for (int element : row) {
                    			System.out.print(element + " ");
                		}
                		System.out.println();
            		}
            		System.out.println();
        	}
        	System.out.println();
    	}	
}

