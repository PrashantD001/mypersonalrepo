



class Demo{

	public static void main(String[] pn){
	
		int[][][] arr1  = {{{1,2},{3,4}},{{5,6},{7,8}}};
		int[][][] arr2  = new int[][][]{{{1,2},{3,4}},{{5,6},{7,8}}};
		

		int[][][] arr3 = new int[2][][];
		arr3[0]=new int[][]{{1,2},{3,4}};
		arr3[0]=new int[][]{{5,6},{7,8}};
		
		int[][][] arr4 = new int[2][2][];
		arr4[0][0] = new int[]{1,2};
		arr4[0][1] = new int[]{3,4};
		arr4[1][0] = new int[]{5,6};
		arr4[1][1] = new int[]{7,8};

		int[][][] arr5 = new int[2][2][2];
		arr5[0][0][0]=1;
		arr5[0][0][1]=2;
		arr5[0][1][0]=3;
		arr5[0][1][1]=4;
		arr5[1][0][0]=5;
		arr5[1][0][1]=6;
		arr5[1][1][0]=7;
		arr5[1][1][1]=8;
	}
}
