

class Outer{


	int x =100;

	class Inner1{
	
		int x =10;

		class Inner2{
		
			int x =1;

			void fun(){
			
				System.out.println(this.x);
				System.out.println(Inner1.this.x);
				System.out.println(Outer.this.x);
			}
		}
	}
}


class Demo{

	public static void main(String[] pn){
	
	
		Outer o = new Outer();

		Outer.Inner1 i1 = o.new Inner1();

		Outer.Inner1.Inner2 i2 = i1.new Inner2();

		i2.fun();
	}
}
