

class Parent{


	static void m1(){
	
		System.out.println("In Parent m1");
	}
}

class Child extends Parent{

	static void m1(){
	
		System.out.println("In Child - m1");
	}

	public static void main(String[] pn){
	
		Parent p = new Child ();

		p.m1();
	}
}
