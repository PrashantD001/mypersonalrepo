

class demo{

	demo(){
	
		System.out.println("In Constructor");
	}
	static int x;

	static{
	
		System.out.println("In Static block ");
		System.out.println(x);
	}
	
	{
	
		System.out.println("In Non-Static block ");
		
	}

	static void fun(){
	
		x = 20;
	}
	public static void main(String[] pn){

		System.out.println("In Main");
		demo o = new demo();
		//System.out.println("In Main");

		//System.out.println("In Main");
	}
}
