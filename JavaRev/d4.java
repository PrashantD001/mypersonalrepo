class Parent{

	Parent(){
	
		System.out.println("In Parent Constructor ");
	}

	static{
	
		System.out.println("In Parent Static ");
	}
}

class Child1 extends Parent{

	Child1(){
	
		System.out.println("In Child1 Constructor ");
	}

	static{
	
		System.out.println("In Child1 Static ");
	}
}

class Child2 extends Child1{

	Child2(){
	
		System.out.println("In Child2 Constructor ");
	}

	static{
	
		System.out.println("In Child2 Static ");
	}

	public static void main(String[] pn){
	
		System.out.println("In Child2 Main ");

		Child1 o1 = new Child1();
		Child2 o2 = new Child2();

	}
}

class Demo{


	public static void main(String[] pn){
	

		Child1 o1 = new Child1();
		Child1 o2 = new Child2();
	}
}

