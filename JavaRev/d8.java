class Outer{
	int x =100;
	static int y = 200;
	class Inner1{
		int x =10;
		int y =20;
		class Inner2{
			int x = 1 ;
			int y = 2 ;
			void fun(){
				System.out.println(this.x);
				System.out.println(Inner1.this.x);
				System.out.println(Outer.this.x);
				
				System.out.println(y);
				System.out.println(Inner1.this.y);
				System.out.println(Outer.y);
			}
		}
	}
}
class Demo{
	public static void main(String[] pn){
		Outer o = new Outer();
		Outer.Inner1 i1 = o.new Inner1();
		Outer.Inner1.Inner2 i2 = i1.new Inner2();
		//Inner2 i2 = i1.new Inner2();
		i2.fun();
	}
}
