class Outer{
	Outer(){
	
		System.out.println("In Outer Constructor ");
		System.out.println("This in Outer :  "+this);
	}
	class Inner1{
		Inner1(){

			System.out.println("In Inner Constructor ");
			System.out.println("This in Inner :  "+this);
			System.out.println("Outer.this in Inner :  "+Outer.this);
			//System.out.println("This$0 in Inner :  "+this$0);
		}
	}
}
class Demo{
	public static void main(String[] pn){
		Outer o = new Outer();
		Outer.Inner1 i = o.new Inner1();
	
		System.out.println("In main");
		System.out.println("Object of O :  "+o);
		System.out.println("Object of i  :  "+i);
	}
}
