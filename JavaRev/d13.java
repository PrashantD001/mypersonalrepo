class Parent{
	void m1(){		
		System.out.println("In  m1 of Parent");
	}
}
class Child extends Parent{
	void m1(){
		System.out.println("In  m1 of Child");
	}
}
class Demo{
	public static void main(String[] pn){
		Parent p = new Child(){
			void m1(){
			
				System.out.println("In  m1 of Anno");
			}
		};
		p.m1();
	}
}
