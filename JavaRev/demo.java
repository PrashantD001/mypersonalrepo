

import java.lang.*;
import java.io.*;


class Demo {

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String s1 = br.readLine();
		String s2 = br.readLine();
		System.out.println(s1==s2);
		System.out.println(s1.equals(s2));
		
		String s3 = "Prashant";
		String s4 = "Prashant";
		System.out.println(s3==s4);
		System.out.println(s3.equals(s4));

		String s5 =  new String(br.readLine());
		String s6 = new String(br.readLine());
		System.out.println(s5==s6);
		System.out.println(s5.equals(s6));
	}

}
