


interface A{

	class B{
	
		void m1(){
		
			System.out.println("In m1");
		}
	}

}


class Demo implements A{

	public static void main(String[] pn){
	
		A obj = new Demo();

		obj.B o = obj.new B();

		o.m1();
	}
}
