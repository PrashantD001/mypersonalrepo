import java .io.*;

class Demo{

	public static void main(String[] pn){
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		try{
		
			System.out.println("in try");
			br.readLine();
		}catch(Exception e){
		
			System.out.println("in catch");
		}

		System.out.println("After try-catch");
	}
}
