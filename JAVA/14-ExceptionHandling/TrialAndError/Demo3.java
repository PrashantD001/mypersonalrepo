import java.io.*;

class Demo {

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		try{
		
			br.close();
			br.readLine();

		} catch (IOException i){
		
			throw new IOException("MY NAME IS PN");

			/*	In Throwable 
			 *	public Throwable(String message){
			 *		fillInStackTrace();
			 *		detailMessage = message;
			 *	}
			 *
			 *	fillInStackTrace(){
			 *		FillInStackTrace(0);
			 *	}
			 *
			 *	native  fillInStackTrace(int n);					<------<
			 */
			

	//		i.printStackTrace();						// Error Unreacheble Statement
		}
	}
}
