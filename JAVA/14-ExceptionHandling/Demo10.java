import java.io.*;

class Demo{

	static void gun() throws IOException{
	
		System.out.println("In gun");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		br.close();
		br.readLine();
		System.out.println("in gun After BufferedReader");
	}

	static void fun() throws IOException{
	
		System.out.println("in fun");
		gun();
		System.out.println("in fun after gun");
	}

	public static void main(String[] pn)throws IOException{
	
	
		System.out.println("in main");
		fun();
		System.out.println("in main after fun");
	}
}
