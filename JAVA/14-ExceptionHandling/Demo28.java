import java.io.*;
import java.lang.*;

class NoNegativeAgeException extends Exception{

	NoNegativeAgeException(String str){
	
		super(str);

	}

}

class Demo{

	public static void main(String[] pn)throws NoNegativeAgeException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter your Age : ");
		int age = 0;

		try{
		
			age = Integer.parseInt(br.readLine());
		}catch(IOException io){
		
			System.out.println("In Catch ");
		}
		if(age<=0){
		
			throw new NoNegativeAgeException("Age Cannot be Zero or Negative");
		}
	
	}

}
