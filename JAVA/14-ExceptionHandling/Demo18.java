import java .io.*;
import java.util.*;

class Demo{

	public static void main(String[] pn){
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String str =  null;
		try{
		
			System.out.println("in try");
			String str = Integer.parseInt(br.readLine());
		}catch(Exception e){
		
			System.out.println("in catch");
			Scanner sc = new Scanner(System.in);
			str = sc.nextLine();
		}

		System.out.println(str);
		System.out.println("After try-catch");
	}
}
