
// NullPointerException

class Demo{

	void m1(){
	
		System.out.println("In m1 Demo");
	}

	public static void main(String[] pn){
	
		Demo d= new Demo();
		d.m1();

		d = null;

		d.m1();

		System.out.println("The End ");


		// NullPointerException
	}
}
