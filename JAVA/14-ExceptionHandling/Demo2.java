
//     		IOException

import java.io.*;

class ExceptionDemo{

	public static void main(String[] pn){
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String str = br.readLine();
		
		// Unreported exception IOException : must be caught or declared to be thrown
	}
}
