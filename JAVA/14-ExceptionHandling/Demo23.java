import java .io.*;
import java.util.*;

class Demo{

	public static void main(String[] pn){
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String str =  null;
		try{
		
			System.out.println("in try");
			
			br.close();				// Comment this line for new OutPut
			str = br.readLine();

		}catch(IOException e){
		
			System.out.println("Handling code ");

		}finally {
		
			System.out.println("In finally");
		}
	}
}
