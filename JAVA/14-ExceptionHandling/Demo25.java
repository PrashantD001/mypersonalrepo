
import java.io.*;

class Demo{

	public static void main(String[] pn){
	
		BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
		BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));

		try{
		
			br1.close();
			br1.readLine();

		}catch(IOException io){
		
			System.out.println("Handling Code");

		} finally {
	
	//		br2.close();

			try{
			
				br2.close();
				br2.readLine();

			} catch (IOException e) {
			
				System.out.println("catch - finally");

			}

			System.out.println("In Finally");
		
		}

	}

}
