import java.lang.*;
import java.io.*;

class MyException extends Exception{

	MyException(String str){
	
		super(str);

	}

}

class Demo{

	void m1() throws MyException{
	
		for(int i=10; i>=0; i--){
		
			System.out.println(i);
			if(i==2){
			
				throw  new MyException("divide by 2");
			}
		}
	}

	public static void main(String[] pn) throws MyException{
	
		Demo obj = new Demo();

		obj.m1();

	}	
	
}
