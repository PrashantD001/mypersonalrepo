import java .io.*;
import java.util.*;

class Demo{

	public static void main(String[] pn){
	
		BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
		BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));

		String str =  null;
		try{
		
			System.out.println("in try");
			
			br1.close();				
			br1.readLine();

		}catch(IOException e){
		
			System.out.println("Handling code ");

		}finally {
			
			try{
				br2.close();
				br2.readLine();
			}catch (Exception  ex){
				System.out.println(" Catch - Finally");
			}
			System.out.println("In finally");
		}
	}
}
