			// NORMAL TERMINATION \\

import java.io.*;

class Demo{

	static void m1(){
	
		System.out.println("In m1 ");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		try{
			br.close();
			String str = br.readLine();
		}catch (IOException io){
		
			System.out.println("Exception in m1");
		}
	}

	static void m2(){
	
		System.out.println("In m2 ");
	}

	public static void main(String[] pn){
	
		m1();
		m2();

	}
}
