import java .io.*;
import java.util.*;

class Demo{

	public static void main(String[] pn){
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String str =  null;
		try{

			// Exception code		
			System.out.println("in try");
			
		//	br.close();
			str = br.readLine();

		}catch(IOException e){
		
			// Handling  code
			System.out.println("in catch-1");
			System.out.println(e);

		}catch(NumberFormatException nf){
		
			// Handling code 
			System.out.println("in catch-2");
			System.out.println(nf);
		}catch(InterruptedException ie){
		
			// Handling code
			System.out.println("In catch-3");
			System.out.println(ie);
		}

		//System.out.println(str);
		System.out.println("After try-catch");
	}
}
