import java .io.*;
import java.util.*;

class Demo{

	public static void main(String[] pn){
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String str =  null;
		try{
		
			System.out.println("in try");
			int x = Integer.parseInt(br.readLine());

			br.close();
			str = br.readLine();

		}catch(IOException e){
		
			System.out.println("in catch-1");
			System.out.println(e);

		}catch(NumberFormatException nf){
		
			System.out.println("in catch-2");
			System.out.println(nf);
		}

		System.out.println(str);
		System.out.println("After try-catch");
	}
}
