			// ABNORMAL TERMINATION \\

import java.io.*;

class Demo{

	static void m1()throws IOException{
	
		System.out.println("In m1 ");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		br.close();
		String str = br.readLine();
		System.out.println(str);
	}

	static void m2(){
	
		System.out.println("In m2 ");
	}

	public static void main(String[] pn)throws IOException{
	
		m1();
		m2();

	}
}
