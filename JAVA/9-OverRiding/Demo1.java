class Parent{

	Parent(){
		System.out.println("In Constructer - P ");
	}
	void m1(){
		System.out.println("In method m1 ");
	}
}

class Child extends Parent{

	Child(){
		System.out.println("In Constructer - C ");
	}
	void m2(){
		System.out.println("In method m2 ");
	}
}

class Demo{

	public static void main(String[] pn){
	
		Parent p = new Parent();
		p.m1();
	//	p.m2();		//error
		
		Child c = new Child();
		c.m1();
		c.m2();

		Parent p1 = new Child();
		p1.m1();
	//	p1.m2();	//error
	}
}
