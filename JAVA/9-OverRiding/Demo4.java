class Demo{

	Demo(String s){
		System.out.println("String");
	}
	Demo(StringBuffer sb){
		System.out.println("StringBuffer");
	}

	public static void main(String[] pn){
	
		Demo d = new Demo(null);		// Error = Both Constructor matches

		String str = null;
		StringBuffer sb = null;

		String str2 = "PN";
	//	StringBuffer sb2 = "PN";		//Error = incompatible types : String cannot be converted to StringBuffer
	}
}
