
import java.util.*;

class Node{

	Node prev = null;
	int data ; 
	Node next = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	//1 AddFirst()	
	void addFirst(int data){
	
		Node newNode = new Node(data);

		if(head == null){
			head = newNode;
		}else{
		
			newNode.next = head;
			head.prev = newNode;
			head = newNode;
		}
	}

	//2 AddLast()
	void addLast(int data){
	
		Node newNode = new Node(data);

		if(head == null){
		
			head = newNode;
		}else{
		
			Node temp = head.prev;

			temp.next = newNode;
			newNode.prev = temp;

		}
	}

	//3 AddAtPos()
	void addAtPos(int data , int pos){
	
		int count = countLinkedList();

		if(pos <= 0 || pos >= count+2){
		
			System.out.println("Invalid Position ");
			return ;
		}
		if(pos == 1){
		
			addFirst(data);
		}else if (pos == count+1){
		
			addLast(data);
		}else{
		
			Node newNode = new Node(data);

			Node temp = head;
			
			while(pos-2 != 0 ){
			
				temp = temp.next;
				pos--;
			}
			newNode.next = temp.next;
			temp.next.prev = newNode;

			temp.next = newNode;
			newNode.prev = temp;
		}
	}

	//4 delFirst()
	void delFirst(){
	
		if(head == null){
		
			System.out.println("LinkedList is Empty ");
		}else if (head.next == null){
			head = null;
		}else {
		
			head = head.next;

		}
	}

	//5 delLast()
	void delLast(){
	
		if(head == null){
		
			System.out.println("LinkedList is Empty ");
		}else if (head.next == null){
			head= null;
		}else{
		
			Node temp = head.prev.prev;
			temp.next = null;

		}
	}

	//6 delAtPos()
	void delAtPos(int pos){
	
		int count = countLinkedList();

		if(pos <= 0 || pos >= count+1){
		
			System.out.println("Invalid Position ");
			return ;
		}
		if(pos == 1){
		
			delFirst();
		}else if (pos == count){
		
			delLast();
		}else{
			Node temp = head;
			
			while(pos-2 != 0 ){
			
				temp = temp.next;
				pos--;
			}

			temp.next = temp.next.next;
			temp.next.prev = temp;
		}
	}
	
	//7 countLinkedList()
	int countLinkedList(){
	
		int count = 0;
		if(head == null){
			System.out.println("LinkedList is Empty ");
		}else{
		
			Node temp = head;

			while(temp!=null){
			
				temp = temp.next;
				count++;
			}
		}
		return count ;
	}
	
	//8 printLinkedList()
	void printLinkedList(){

		if(head == null){
			System.out.println("LinkedList is Empty ");
		}else{
		
			Node temp = head;

			while(temp.next !=null){
			
				System.out.print(temp.data+" -> ");
				temp = temp.next;
			}
			System.out.println(temp.data);
		}
	}

}
class Client{

	public static void main(String[] pn){
	
		LinkedList ll = new LinkedList();


		Scanner sc = new Scanner(System.in);

		char ch = 'Y';
		do{
		
			System.out.println("1.addFirst");
			System.out.println("2.addLast");
			System.out.println("3.addAtPos");
			System.out.println("4.delFirst");
			System.out.println("5.delLast");
			System.out.println("6.delAtPos");
			System.out.println("7.countLinkedList");
			System.out.println("8.printLinkedList");

			System.out.println("Enter the Choice : ");
			int choice = sc.nextInt();

			switch(choice){
			
				case 1: {
					System.out.println("Enter the Data : ");
					int data = sc.nextInt();
					ll.addFirst(data);
				}
				break;
				case 2: {
					System.out.println("Enter the Data : ");
					int data = sc.nextInt();
					ll.addLast(data);
				}
				break;
				case 3: {
					System.out.println("Enter the Data : ");
					int data = sc.nextInt();
					System.out.println("Enter the Position : ");
					int pos = sc.nextInt();
					ll.addAtPos(data,pos);
				}
				break;
				case 4: {
					ll.delFirst();
				}
				break;
				case 5: {
					ll.delLast();
				}
				break;
				case 6: {
					System.out.println("Enter the Position : ");
					int pos = sc.nextInt();
					ll.delAtPos(pos);
				}
				case 7: {
					int count = ll.countLinkedList();
					System.out.println("Count : "+count);
				}
				break;
				case 8: {
					ll.printLinkedList();
				}
				break;
				default:{
				
					System.out.println("Wrong Choice ..............!");
				}
				break;
			}

			System.out.println("do you want to Continue ? ");
			ch = sc.next().charAt(0);

		}while(ch=='Y' || ch=='y');
	}
}
