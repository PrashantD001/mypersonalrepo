
/*
 *14] Maximum repeating number

Given an array Arr of size N, the array contains numbers in range from 0 to K-1
where K is a positive integer and K <= N. Find the maximum repeating number in
this array. If there are two or more maximum repeating numbers return the element
having least value.
 * */

import java.io.*;

class Demo{

	static int maxRepeatingNum(int[] arr , int size){
	
		int num = -1 ;
		int maxCount = 0;
		
		for(int i=0 ; i<size ; i++){
			int count = 1;
			for(int j = i+1 ; j<size ; j++){
			
				if(arr[i] == arr[j]){
					count++;
				}
			}
			if(count> maxCount){
				num = arr[i];
				maxCount = count;
			}
		}
		return num;
	}

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Size Of Array : ");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];

		System.out.println("Enter the Elements in Array : ");
		for(int i = 0 ; i<size ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		int ans = maxRepeatingNum(arr,size);

		System.out.println("The maximum Repeating Number is : "+ans);
	}
}
