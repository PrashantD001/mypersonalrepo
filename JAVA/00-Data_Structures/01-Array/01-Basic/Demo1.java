
/*
 * 1] Search an Element in an array

Given an integer array and another integer element. The task is to find if the given
element is present in the array or not.
*/

import java.io.*;

class Demo{

	static int isPresent(int[] arr , int size , int num){
	
		for(int i =0 ; i<size ; i++){
		
			if(arr[i]==num)
				return 1;
		}
		return 0;
	}

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Size Of Array : ");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];

		System.out.println("Enter the Elements in Array : ");
		for(int i = 0 ; i<size ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter the Element : ");
		int num = Integer.parseInt(br.readLine());

		int ans = Demo.isPresent(arr,size,num);

		if(ans == 1){
			System.out.println("Element is Present");
		}else{
			System.out.println("Element is Not Present");
		}
	}
}
