
/*
 * 10] Max Odd Sum

Given an array of integers, check whether there is a subsequence with odd sum and
if yes, then find the maximum odd sum. If no subsequence contains an odd sum,
print -1.
*/

import java.io.*;

class Demo{

	static int maxOddSum(int[] arr, int size){
	
		int max = -1;

		for(int i=0 ; i<size ; i++){
			int sum = 0;
			for(int j = i ; j<size ; j++){
				sum+=arr[j];
				if((sum%2==1) && sum>max){
					max = sum;
				}
			}
		}
		return max;
	}

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Size Of Array : ");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];

		System.out.println("Enter the Elements in Array : ");
		for(int i = 0 ; i<size ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		int ans = Demo.maxOddSum(arr,size);

		System.out.println("The Maximun odd sum  of Array element subsequence is :  "+ans);
	}
}
