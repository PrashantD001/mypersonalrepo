
/*
 * 13] Find unique element

Given an array of size n which contains all elements occurring in multiples of K,
except one element which doesn't occur in multiple of K. Find that unique element.
*/

import java.io.*;

class Demo{

	static int findUnique(int[] arr, int size){
	
		int temp = 0,size2=0;
		for(int i= 0 ; i<size ; i++){
			int flag1=1 ,flag2=1 ;

			for(int j= i+1 ; j<size ; j++){
				if(arr[i] ==arr[j]){
					flag1++;
				}
			}
			for(int k =i-1 ; k>=0 ; k--){
				if(arr[i] == arr[k]){
					flag2 = 0;
					break;
				}
			}
			if(flag1== 1 && flag2==1){
				return arr[i];
			}
		}
		return -1;
	}

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Size Of Array : ");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];

		System.out.println("Enter the Elements in Array : ");
		for(int i = 0 ; i<size ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		int ans = Demo.findUnique(arr,size);

		System.out.println("Answer : "+ans);
	}
}
