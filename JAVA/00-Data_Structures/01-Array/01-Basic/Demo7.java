
/*
 * 7] Form largest number from digits

Given an array of numbers from 0 to 9 of size N. Your task is to rearrange elements
of the array such that after combining all the elements of the array, the number
formed is maximum.
*/

import java.io.*;

class Demo{

	static int largestNumber(int[] arr, int size){

		for(int i=0 ; i<size; i++){
			for(int j=i ; j<size; j++){
				if(arr[i] < arr[j]){
					int temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}	
		}	
		int ans = 0;

		for(int i=0 ; i<size ; i++){
		
				ans = (ans*10)+arr[i];
		}
		return ans;
	}

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Size Of Array : ");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];

		System.out.println("Enter the Elements in Array : ");
		for(int i = 0 ; i<size ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		int ans = Demo.largestNumber(arr,size);

		System.out.println("The Largest Number formed by Array element is :  "+ans);
	}
}
