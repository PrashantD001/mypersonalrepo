
/*
 * 2] Find minimum and maximum element in an array

Given an array A of size N of integers. Your task is to find the minimum and
maximum elements in the array.
*/

import java.io.*;

class Demo{

	static int[] findMinMax(int[] arr, int size){
	
		int[] ans = new int[]{arr[0],arr[0]};

		for(int i=0 ; i<size ; i++){
		
			if(arr[i] < ans[0])
				ans[0] = arr[i];
			if(arr[i] > ans[1])
				ans[1] = arr[i];
		}
		return ans;
	}

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Size Of Array : ");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];

		System.out.println("Enter the Elements in Array : ");
		for(int i = 0 ; i<size ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		int ans[] = Demo.findMinMax(arr,size);

		System.out.println("The minimun element is :  "+ans[0]);
		System.out.println("The maximun element is :  "+ans[1]);
		
	}
}
