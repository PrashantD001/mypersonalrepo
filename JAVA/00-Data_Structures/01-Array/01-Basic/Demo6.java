
/*
 * 6] Elements in the Range

Given an array arr[] containing positive elements. A and B are two numbers
defining a range. The task is to check if the array contains all elements in the given
range.
*/

import java.io.*;

class Demo{

	static int inRange(int[] arr , int size , int A ,int B){
	
		for(int j = A ; j<= B ; j++){
			int flag = 0;
			for(int i =0 ; i<size ; i++){
		
				if(arr[i]== j ){
					flag = 1;
					break;
				}
			}
			if(flag == 0){
				return 0;
			}
		}
		return 1;
	}

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Size Of Array : ");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];

		System.out.println("Enter the Elements in Array : ");
		for(int i = 0 ; i<size ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter the Start of Range : ");
		int A = Integer.parseInt(br.readLine());
		
		System.out.println("Enter the End of Range : ");
		int B = Integer.parseInt(br.readLine());

		int ans = Demo.inRange(arr,size,A,B);

		if(ans == 1){
			System.out.println("Yes");
		}else{
			System.out.println("No");
		}
	}
}
