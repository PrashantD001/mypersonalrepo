
/*
 * 5] Replace all 0's with 5
 *
 *  You given an integer N, you need to convert all zeros of N to 5.
*/

import java.io.*;

class Demo{

	static void replaceZeros(int[] arr, int size){
	
		for(int i=0 ; i<size ; i++){
		
			if(arr[i]  == 0)
				arr[i] = 5 ;
		}
	}

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Size Of Array : ");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];

		System.out.println("Enter the Elements in Array : ");
		for(int i = 0 ; i<size ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println("Array Before Replacing all 0's with 5 : ");
		for(int i = 0 ; i<size ; i++){
			System.out.print(arr[i]+ " - ");;	
		}
		System.out.println();

		Demo.replaceZeros(arr,size);
		
		System.out.println("Array After Replacing all 0's with 5 : ");
		for(int i = 0 ; i<size ; i++){
			System.out.print(arr[i]+" - ");
		}
		System.out.println();
	}
}
