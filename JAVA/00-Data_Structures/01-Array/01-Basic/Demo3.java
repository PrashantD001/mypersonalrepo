
/*
 * 3] Largest Element in Array

Given an array A[] of size n. The task is to find the largest element in it.
*/

import java.io.*;

class Demo{

	static int findLargest(int[] arr, int size){
	
		int ans = arr[0];

		for(int i=0 ; i<size ; i++){
		
			if(arr[i] > ans)
				ans = arr[i];
		}
		return ans;
	}

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Size Of Array : ");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];

		System.out.println("Enter the Elements in Array : ");
		for(int i = 0 ; i<size ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		int ans = Demo.findLargest(arr,size);

		System.out.println("The Largest element is :  "+ans);
	}
}
