
/*
 * 15] Sum of distinct elements

You are given an array Arr of size N. Find the sum of distinct elements in an array.

*/

import java.io.*;

class Demo{

	static int sumOfDistinct(int[] arr, int size){
	
		int temp = 0,size2=0;
		int sum = 0 ;
		for(int i= 0 ; i<size ; i++){
			int flag1=1 ,flag2=1 ;

			for(int j= i+1 ; j<size ; j++){
				if(arr[i] ==arr[j]){
					flag1++;
				}
			}
			for(int k =i-1 ; k>=0 ; k--){
				if(arr[i] == arr[k]){
					flag2 = 0;
					break;
				}
			}
			if(flag1== 1 && flag2==1){
				sum+=arr[i];
			}
		}
		return sum;
	}

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Size Of Array : ");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];

		System.out.println("Enter the Elements in Array : ");
		for(int i = 0 ; i<size ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		int ans = Demo.sumOfDistinct(arr,size);

		System.out.println("Answer : "+ans);
	}
}
