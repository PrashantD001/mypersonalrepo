
/*
 * 16] Last index of One
Given a string S consisting only '0's and '1's, find the last index of the '1' present in
it.
 * */

import java.io.*;

class Demo{

	static int lastIndexOfOne(char[] arr , int size){
	
		int index = -1 ;
		for(int i=0 ; i<size ; i++){
			if(arr[i] == '1'){
				index = i;
			}
		}
		return index;
	}

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Sring of 0's as 1's only : ");
		String str = br.readLine();

		char[] arr = str.toCharArray();

		int ans = lastIndexOfOne(arr,arr.length);

		System.out.println("Answer : "+ans);
	}
}
