
/*
 * 12] First and last occurrences of X
Given a sorted array having N elements, find the indices of the first and last
occurrences of an element X in the given array.
Note: If the number X is not found in the array, return '-1' as an array.

*/

import java.io.*;

class Demo{

	static int[] firstAndLastOccurance(int[] arr, int size,int num){
	
		int[] ans = new int[2];

		int flag = 0;
		for(int i=0 ; i<size ; i++){
		
			if(arr[i] == num){
				if(flag==0){
					ans[0] = i;
					ans[1] = i;
				}
				flag = 1 ;
				ans[1] = i;
				
			}
		}
		if(flag ==0 ){
			return null;
		}
		return ans;
	}

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Size Of Array : ");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];

		System.out.println("Enter the Elements in Array : ");
		for(int i = 0 ; i<size ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter the Number : ");
		int num = Integer.parseInt(br.readLine());
		
		int ans[] = Demo.firstAndLastOccurance(arr,size,num);

		if(ans ==null){
		
			System.out.println("-1");
		}else{
			System.out.println("The First Occurance is :  "+ans[0]);
			System.out.println("The Last Occurance is :  "+ans[1]);
		}		
	}
}
