
/*
 * 4] Product of array elements

This is a functional problem. Your task is to return the product of array elements
under a given modulo.
The modulo operation finds the remainder after the division of one number by
another. For example, K(mod(m))=K%m= remainder obtained when K is divided
by m
*/

import java.io.*;

class Demo{

	static int arrayProduct(int[] arr, int size){
	
		int ans = 1;

		for(int i=0 ; i<size ; i++){
		
				ans *= arr[i];
		}
		return ans;
	}

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Size Of Array : ");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];

		System.out.println("Enter the Elements in Array : ");
		for(int i = 0 ; i<size ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		int ans = Demo.arrayProduct(arr,size);

		System.out.println("The Product of Array element is :  "+ans);
	}
}
