
/*
 * 8] Even occurring elements
Given an array Arr of N integers that contains an odd number of occurrences for all
numbers except for a few elements which are present even number of times. Find
the elements which have even occurrences in the array.
*/

import java.io.*;

class Demo{

	static int[] evenOccuring(int[] arr, int size){
	
		int temp = 0,size2=0;
		for(int i= 0 ; i<size ; i++){
			int flag1=1 ,flag2=1 ;

			for(int j= i+1 ; j<size ; j++){
				if(arr[i] ==arr[j]){
					flag1++;
				}
			}
			for(int k =i-1 ; k>=0 ; k--){
				if(arr[i] == arr[k]){
					flag2 = 0;
					break;
				}
			}
			if((flag1%2)==0 && flag2==1){
				size2++;
			}
		}


		if(size2 == 0 )
			return null;

		int[] ans = new int[size2];
		for(int i= 0 ; i<size ; i++){
			int flag1=1 ,flag2=1 ;

			for(int j= i+1 ; j<size ; j++){
				if(arr[i] ==arr[j]){
					flag1++;
				}
			}
			for(int k =i-1 ; k>=0 ; k--){
				if(arr[i] == arr[k]){
					flag2 = 0;
					break;
				}
			}
			if((flag1%2)==0 && flag2==1){
				ans[temp++] = arr[i];
			}
		}

		return ans;
	}

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Size Of Array : ");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];

		System.out.println("Enter the Elements in Array : ");
		for(int i = 0 ; i<size ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		int ans[] = Demo.evenOccuring(arr,size);

		System.out.println("Answer : ");
		if(ans == null){
			System.out.println("-1");
		}else{
			for(int i = 0; i<ans.length; i++){
			
				System.out.print(ans[i]+" : ");
			}
			System.out.println();
		}
	}
}
