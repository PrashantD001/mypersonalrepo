
/*
 * 11] Product of maximum in first array and minimum in second

Given two arrays of A and B respectively of sizes N1 and N2, the task is to
calculate the product of the maximum element of the first array and minimum
element of the second array.
*/

import java.io.*;

class Demo{

	static int productOfMinMax(int[] arr1, int size1, int[] arr2, int size2){
	
		int max = arr1[0];

		for(int i=0 ; i<size1 ; i++){
			if(arr1[i] >max)
				max = arr1[i];
		}

		int min = arr2[0];
		for(int i=0 ; i<size2 ; i++){

			if(arr2[i] <min)
				min = arr2[i];
		}
		return max*min;
	}

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter the Size Of first Array : ");
		int size1 = Integer.parseInt(br.readLine());

		int[] arr1 = new int[size1];

		System.out.println("Enter the Elements in first Array : ");
		for(int i = 0 ; i<size1 ; i++){
			arr1[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter the Size Of Second Array : ");
		int size2 = Integer.parseInt(br.readLine());

		int[] arr2 = new int[size2];

		System.out.println("Enter the Elements in Second Array : ");
		for(int i = 0 ; i<size2 ; i++){
			arr2[i] = Integer.parseInt(br.readLine());
		}

		int ans = Demo.productOfMinMax(arr1,size1,arr2,size2);

		System.out.println("The Product of maximun of first array and minimum of second array is  :  "+ans);
	}
}
