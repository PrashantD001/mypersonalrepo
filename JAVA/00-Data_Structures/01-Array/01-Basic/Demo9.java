
/*
 * 9] Remove an Element at Specific Index from an Array
Given an array of a fixed length. The task is to remove an element at a specific
index from the array.
*/

import java.io.*;

class Demo{

	static int[] removeElement(int[] arr, int size , int index){
	
		int temp = 0;
		int ans[] = new int[size-1];
		for(int i= 0 ; i<size ; i++){
	
			if(i!=index){
				ans[temp++] = arr[i];
			}
		}

		return ans;
	}

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Size Of Array : ");
		int size = Integer.parseInt(br.readLine());

		int[] arr = new int[size];

		System.out.println("Enter the Elements in Array : ");
		for(int i = 0 ; i<size ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter the Index Of Array : ");
		int index = Integer.parseInt(br.readLine());
		
		int ans[] = Demo.removeElement(arr,size,index);

		System.out.println("Answer : ");
	
		for(int i = 0; i<ans.length; i++){
		
			System.out.print(ans[i]+" : ");
		}
		System.out.println();
	}
}
