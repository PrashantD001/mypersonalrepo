class Parent{

	Parent(){
		System.out.println("In Parent Constructor");
	}
	
	abstract void m1();
}

class Child extends Parent{

	Child(){
		System.out.println("In Child Constructor");
	}

	public static void main(String[] pn){
	
		Child c = new Child();
	}
}
