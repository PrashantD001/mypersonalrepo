
abstract class India{

	abstract void Communication();
}

class Maharastra extends India{

	void Communication(){
	
		System.out.println("Communication in Marathi");
	}
}

class Panjab extends India{

	void Communication(){
	
		System.out.println("Communication in Panjabi");
	}
}

class Person{

	public static void main(String[] pn){
	
		Maharastra m = new Maharastra();

		m.Communication();

		Panjab p = new Panjab();

		p.Communication();
	}
}
