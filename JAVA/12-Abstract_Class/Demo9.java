abstract class HindustaniPencils{

	abstract void Price();

}

class NatRaj extends HindustaniPencils{

	void Price(){
		System.out.println("Apsra peksha Mahag");
	}
}

class Apsra extends HindustaniPencils{

	void Price(){
		System.out.println("NatRaj peksha Swast");
	}
}

class Pencils{

	public static void main(String[] pn){
	
	//	HindustaniPencils h = new HindustaniPencils();		// Error

		HindustaniPencils hp1 = new NatRaj();
		hp1.Price();

		HindustaniPencils hp2 = new Apsra();
		hp2.Price();
	}
}
