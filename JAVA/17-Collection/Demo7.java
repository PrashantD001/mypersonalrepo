import java.util.*;

class IterationDemo{

	public static void main(String[] pn){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a Numbers : ");
		int x = sc.nextInt();
		int y = sc.nextInt();
	
		LinkedList v = new LinkedList();

		v.add(10);
		v.add(20);
		v.add(30);
		v.add(40);
		v.add(50);
		v.add(60);

		System.out.println(v);

		Iterator obj = v.iterator();

		int count=0;

		while(obj.hasNext()){
			Object temp  = obj.next();	
			if((temp%x)==0){
			      	if ((temp%y)==0){
					obj.remove();
				}
			}
		
		}
		System.out.println(v);
	}
}
