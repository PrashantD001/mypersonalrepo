
import java.util.*;

class ArrayListDemo{

	public static void main(String[] pn){
	
		ArrayList al = new ArrayList();

		// 1.add(object)

		al.add("Apple");
		al.add("MicroSoft");
		al.add("Amazon");
		al.add("Google");
		al.add("Amazon");
		System.out.println(al);		// [Apple, MicroSoft, Amazon, Google, Amazon]


		// 2.contains(object)

		System.out.println(al.contains("FB"));		// False
		System.out.println(al.contains("Google"));	// True


		// 3.indexOf(object)
		
		System.out.println(al.indexOf("Amazon"));		// First Attempt Value -->  index = 2


		// 4.lastIndexOf(object)
		
		System.out.println(al.indexOf("Amazon"));		// Last Attempt Value -->  index = 4


		// 5.OObject[] toArray(0

		Object[] arr = al.toArray();

		for(Object val : arr)
			System.out.println(val);


		// 6.size()
		
		System.out.println(al.size());		// 5 : No. of Element  Present In ArrayList


		// 7.get(int index)
		
		System.out.println(al.get(4));		// Amazon
		//System.out.println(al.get());		//  Error : IndexOutOfBoundsException


		// 8.set(int,object)

		al.set(3,"FaceBook");			// Update/Replace
		System.out.println(al);			// [Apple, MicroSoft, Amazon, FaceBook, Amazon]


		// 9.remove(int)			// Element Remove at That Index

		System.out.println(al.remove(2));
		System.out.println(al);


		// 10.remove(object)

		System.out.println(al.remove("Amazon"));	// True
		System.out.println(al);				// [Apple, MicroSoft, FaceBook]


		// 11.addAlll(collection)

		ArrayList al1 = new ArrayList();
		al1.addAll(al);
		System.out.println(al1);			// al1 --> [Apple, MicroSoft, FaceBook]

		
		// 12.clear()

		al1.clear();
		System.out.println(al1);			// []

		
		// 13.addAll(int,collection)
		
		ArrayList al2 = new ArrayList();
		al2.add("TCS");
		al2.add("Congi");
		al2.add("Wipro");
		al2.addAll(2,al);
		System.out.println(al2);			// [TCS, Congi, Apple,  MicroSoft, FaceBook, Wipro]
		
		
		// 14.removeAll(collection)

		al2.removeAll(al);
		System.out.println(al2);			// [TCS, Congi, Wipro]

		
		// 15.removeRang(int,int) 			// int,range-1
		
//		al2.removeRange(2,5);
//		System.out.println(al2);			// Error : removeRange(int,int) has Protected access in ArrayList

		
		// 16.clone()

		Object al3 = al2.clone();
		System.out.println(al3);			// al3 = [TCS, Congi, Wipro]

	}
}

