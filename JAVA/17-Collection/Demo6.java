import java.util.*;

class EnumCursor{

	public static void main(String[] pn){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a String : ");
		String temp = sc.nextLine();
	
		Vector v = new Vector();

		v.add("Shashi");
		v.add("Ashish");
		v.add("Kanha");
		v.add("Badhe");
		v.add("Rahul");
		v.add("PN");

		System.out.println(v);

		Enumeration obj = v.elements();

		int count=0;

		while(obj.hasMoreElements()){
				
			if(temp.equals(obj.nextElement())){
				
				count++;
			}
		
		}

		if(count>0){
			System.out.println("Found");
		}else{
			System.out.println("Not Found");
		}
	}
}
