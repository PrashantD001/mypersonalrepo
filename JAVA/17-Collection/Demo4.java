import java.util.*;

class EnumCursor{

	public static void main(String[] pn){
	
		Vector v = new Vector();

		v.add("Shashi");
		v.add("Ashish");
		v.add("Kanha");
		v.add("Badhe");
		v.add(25.45);
		v.add(45);

		System.out.println(v);

		Enumeration obj = v.elements();

		while(obj.hasMoreElements()){
				
			System.out.println(obj.nextElement());
		
		}
	}
}
