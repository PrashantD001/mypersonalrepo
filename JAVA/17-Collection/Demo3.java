import java.util.*;

class ForEachDemo{

	public static void main(String[] pn){
	
		ArrayList al = new ArrayList();

		al.add(1);
		al.add("Rahul");
		al.add(25.30);

		//for(data:al):
		//	print(data);
		
		for(Object data : al){
			System.out.println(data);
		}

		System.out.println(al.indexOf(1));
		System.out.println(al.getClass());


	}
}
