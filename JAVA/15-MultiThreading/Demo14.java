import java.io.*;

class Demo{

	void fun() throws IOException{
		throw new IOException("IO : EXCEPTION");
		System.out.println("IN FUN");			// Error = UnReacheable Statement
	}

	public static void main(String[] pn)throws IOException{
	
		Demo d = new Demo();
		d.fun();
	}
}
