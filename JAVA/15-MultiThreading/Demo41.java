class ThreadDemo extends Thread implements Runnable{

	public void run(){
	
		System.out.println("In Run : In ThreadDemo");
		System.out.println("In Run : " +  Thread.currentThread().getName());
	}

	public static void main(String[] pn){

		ThreadDemo obj = new ThreadDemo();
		Thread t3 = new Thread(obj);
		System.out.println(t3.getName());	// Thread-1
		System.out.println(t3.getThreadGroup());
		t3.start();				// no output calls Parent run which is Thread class run 
							// Due to Onject is of parent and refrence is aslo of Parent 
		obj.start();
	}
}
