class ThreadDemo extends Thread{

	ThreadDemo(){
	
		super();
	}
	ThreadDemo(ThreadGroup tg , String name){
	
		super(tg , name);
	}

	public void run(){
	
		System.out.println(Thread.currentThread().getName());

		for(int i = 0 ; i<10 ; i++){
			System.out.println(" : In Run :");
			
			try{
				Thread.sleep(1000);
			} catch (InterruptedException ie){
				System.out.println(Thread.currentThread().getName()+"Destroyed  ......");
			}
		}
	}
}

class ThreadGroupDemo{

	public static void main(String[] pn){
	
		ThreadDemo t1 = new ThreadDemo();
		t1.start();

		ThreadGroup tg1 = new ThreadGroup("C2W Group");
		ThreadDemo t2 = new ThreadDemo(tg1 , "tFirst");
		ThreadDemo t3 = new ThreadDemo(tg1 , "tSecond");
		ThreadDemo t4 = new ThreadDemo(tg1 , "tThird");
		
	
		ThreadGroup tg2 = new ThreadGroup(tg1  , "BienCaps Group");
		Thread t5 = new Thread(tg2 , "tFour");
		Thread t6 = new Thread(tg2 , "tFive");
		Thread t7 = new Thread(tg2 , "tSix");

		t5.start();
		t6.start();

		tg1.interrupt();
	
	}

}
