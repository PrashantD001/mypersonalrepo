
class MyThread extends Thread{

	public void run(){
	
		System.out.println("In run ");
		System.out.println(Thread.currentThread().getName());

		fun();
	}

	void fun(){
	
		System.out.println("In Fun");
	}

	public static void main(String[] pn){
	
		System.out.println(Thread.currentThread().getName());

		MyThread t1 = new MyThread();
		t1.start();

		System.out.println(Thread.currentThread().getName());
		
		MyThread t2 = new MyThread();
		t2.start();

		System.out.println(Thread.currentThread().getName());

	}
}
