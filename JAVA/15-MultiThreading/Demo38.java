class ThreadDemo implements Runnable{

	public void run(){
	
		System.out.println("In Run : In ThreadDemo");
	}

	public static void main(String[] pn){
	
		Thread t1 = new Thread();
		System.out.println(t1.getName());	// Thread-0
		System.out.println(t1.getPriority());	// 5
		System.out.println(t1.getThreadGroup());	// Main     maxPriority = 10
				
	}
}
