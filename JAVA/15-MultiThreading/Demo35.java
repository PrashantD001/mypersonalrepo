import java.io.*;

class OutOfCash extends Exception{

	OutOfCash(String str){
	
		super(str);
	}
}

class LimitExceeded extends Exception{

	LimitExceeded(String  str){
	
		super(str);
	}
}

class InsufficientFunds extends Exception{

	InsufficientsFunds(String str){
	
		super(str);
	}
}

class Bank{

	public static void main(String[] pn){
	
		User obj1 = new User();
		obj1.setName("User1");
		obj1.start();


		Cashier obj2 = new Cashier();
		obj2.setName("Cashier1");
		obj2.start();

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		try{
			String str = br.readLine();
			br.close();
			br.readLine();
		} catch (IOException io){
			io.printStackTrace();
		}
	}
}
