class Demo{

	public static void main(String[] pn){
	
		System.out.println("In Demo Class Thread is : " + Thread.currentThread().getName());

		ThreadDemo obj = new ThreadDemo();
		obj.setName("C2W");
		obj.setPriority(8);
		obj.start();
	}

}

class ThreadDemo extends Thread{

	void fun(){
	
		System.out.println("In ThreadDemo - m1() ");
	}

	public void run(){
	
		System.out.println("In ThreadDemo Class Thread is : " + Thread.currentThread().getName());

		ThreadDemo obj1 = new ThreadDemo();
		obj1 = null;
		try{
		
			obj1.fun();
		} catch (NullPointerException ne){
		
			System.out.println("Exception Occured In ThreadDemo ");
		}

		RunDemo obj2 = new RunDemo();
		Thread obj3 = new Thread(obj2);
		obj3.setName("BienCaps");
		obj3.start();

	}
}

class RunDemo implements Runnable{

	void gun(){
		System.out.println("In RunDemo : "+(10/0));
	}
	public void run(){
		
		System.out.println("In RunDemo Class Thread is : " + Thread.currentThread().getName());

		gun();
	
	}
}
