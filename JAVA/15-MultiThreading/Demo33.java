
class MyException1 extends Exception{

	MyException1(String str){
	
		super(str);	
	}
}

class MyException2 extends Exception{

	MyException2(String str){
	
		super(str);	
	}
}

class Demo{

	public static void main(String[] pn){
	
		ThreadClass1 obj = new ThreadClass1();
		obj.setName("C2W");
		obj.setPriority(8);
		obj.start();
	}
}

class ThreadClass1 extends Thread{

	static int temp = 0; 
	void fun() throws MyException1{
	
		for(int i=1 ; i<=50 ; i++){
		
			if(i%4==0 && i%5==0){
				temp++;
				if(temp>=2){
				
					throw new MyException1("Exception due to  divisible  by 4 & 5");
				}
			}
		}
	}

	public void run(){
	
		System.out.println("In Run of ThreadClass1 : "+Thread.currentThread().getName());

		ThreadClass2 obj1 = new ThreadClass2();
		Thread obj2 = new Thread(obj1);
		obj2.setName("BienCaps");
		obj2.start();

		try{
			fun();
		}catch(MyException1 me){
			me.printStackTrace();
		
		}
	}
}

class ThreadClass2  implements  Runnable{

	static int temp =0 ;	
	void gun() throws MyException2{
	
		for(int i=1 ; i<=50 ; i++){
		
			if(i%3==0 ){
				temp++;
				if(temp>=8){
				
					throw new MyException2("Exception due to  divisible  by 3");
				}
			}
		}
	}

	public void run(){
		
		System.out.println("In Run of ThreadClass2 : "+Thread.currentThread().getName());
		
		try{
			gun();
		}catch(MyException2 me){
			me.printStackTrace();	
		}
	
	}
}
