class ThreadGroupDemo{

	public static void main(String[] pn){
	
		Thread t1 = new Thread();

		ThreadGroup tg1 = new ThreadGroup("C2W Group");
		Thread t2 = new Thread(tg1 , "tFirst");
		Thread t3 = new Thread(tg1 , "tSecond");
		Thread t4 = new Thread(tg1 , "tThird");

		ThreadGroup tg2 = new ThreadGroup("BienCaps Group");
		Thread t5 = new Thread(tg2 , "tFour");
		Thread t6 = new Thread(tg2 , "tFive");
		Thread t7 = new Thread(tg2 , "tSix");


		System.out.println("First Way : "+tg1.getParent().activeGroupCount());
		
		System.out.println("Second Way : "+Thread.currentThread().getThreadGroup().activeGroupCount());

	}

}
