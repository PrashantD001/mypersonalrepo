
class YieldDemo extends Thread{

	public void run(){
	
		for(int i=0 ; i<=10 ; i++){
		
			System.out.println("In Run ");
		//	yield();
			try{
			
				Thread.sleep(1000);
			} catch (InterruptedException ie){
			
			}
		}
	}
}

class Demo{

	public static void main(String[] pn){

		YieldDemo obj = new YieldDemo();
		obj.start();
		obj.yield();
		
		for(int i=0 ; i<=10 ; i++){
		
			System.out.println("In Main ");
			try{
			
				Thread.sleep(1000);
			} catch (InterruptedException ie){
				
			}
		}
	}
}
