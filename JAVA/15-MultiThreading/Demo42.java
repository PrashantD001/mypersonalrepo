class ThreadDemo extends Thread{

	ThreadDemo(){
	
		super();
	}

	ThreadDemo(String name){
	
		super(name);
	}

	ThreadDemo(ThreadGroup tg , String name){
	
		super(tg , name);
	}

	public void run(){
	
		System.out.println("In Run : In ThreadDemo");
	}




}
class ThreadGroupDemo{

	public static void main(String[] pn){

		System.out.println(Thread.activeCount());		// 1
			
		ThreadDemo t1 = new ThreadDemo();
		t1.start();
		System.out.println(t1.getThreadGroup());

		//    Thread Group = 1

		ThreadGroup tg1 = new ThreadGroup("C2W Group");
		ThreadDemo t2 = new ThreadDemo(tg1, "Pashant");
		System.out.println(t2.getThreadGroup());
		
		//    Thread Group = 2
		ThreadGroup tg2 = new ThreadGroup("BienCaps Group");
		ThreadDemo t3 = new ThreadDemo(tg1, "Pashant");
		System.out.println(t3.getThreadGroup());
	

		System.out.println(Thread.activeCount());		// 1
	}
}
