
class JoinDemo extends Thread {

	public void run(){
	
		for(int i=0 ; i<=10 ; i++){
		
			System.out.println("Child Thread ");
			try{
			
				Thread.sleep(1000);
			} catch (InterruptedException ie){
			
			}
		}
	}
}

class Demo{

	public static void main(String[] pn)  throws InterruptedException{
	
		JoinDemo d = new JoinDemo();

		d.start();
		d.join();			// Main Thread Stops  until Thread0 completes its execution

		for(int i=0 ; i<=10 ; i++){

			System.out.println("Main Thread ");			

			try{

				Thread.sleep(1000);

			} catch (InterruptedException ie){
			
			}
		}
	}
}
