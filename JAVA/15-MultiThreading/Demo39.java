class ThreadDemo extends Thread implements Runnable{

	public void run(){
	
		System.out.println("In Run : In ThreadDemo");
	}

	public static void main(String[] pn){
	
		Thread t1 = new Thread();
		System.out.println(t1.getName());	// Thread-0
		System.out.println(t1.getThreadGroup());
		t1.start();				// no output calls Parent run which is Thread class run 
							// Due to Onject is of parent and refrence is aslo of Parent 

		Thread t2 = new Thread("C2W");
		System.out.println(t2.getName());	// C2W
		System.out.println(t2.getThreadGroup());
		t2.start();				// no output calls Parent run which is Thread class run 
							// Due to Onject is of parent and refrence is aslo of Parent 


		ThreadDemo obj = new ThreadDemo();
		Thread t3 = new Thread(obj);
		System.out.println(t3.getName());	// Thread-1
		System.out.println(t3.getThreadGroup());
		t3.start();				// no output calls Parent run which is Thread class run 
							// Due to Onject is of parent and refrence is aslo of Parent 

		
		ThreadDemo obj2 = new ThreadDemo();
		Thread t4 = new Thread(obj2, "PN");
		System.out.println(t4.getName());	// PN
		System.out.println(t4.getThreadGroup());
		t4.start();				// no output calls Parent run which is Thread class run 
							// Due to Onject is of parent and refrence is aslo of Parent 

		
	}
}
