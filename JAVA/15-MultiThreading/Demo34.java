
class ExceptionDiv45 extends Exception{

	ExceptionDiv45(String str){
	
		super(str);	
	}
}

class ExceptionDiv3 extends Exception{

	ExceptionDiv3(String str){
	
		super(str);	
	}
}

class ThreadDemo{

	public static void main(String[] pn){
	
		ThreadClass1 obj = new ThreadClass1();
		obj.setName("C2W");
		obj.setPriority(8);
		obj.start();
	}
}

class ThreadClass1 extends Thread{

	static int temp = 0; 
	void fun() throws ExceptionDiv45{
	
		for(int i=1 ; i<=50 ; i++){
		
			if(i%4==0 && i%5==0){
				temp++;
				if(temp>=2){
				
					throw new ExceptionDiv45("Exception Causee -->  divisible  by 4 & 5");
				}
			}
		}
	}

	public void run(){
	
		System.out.println("In Run of ThreadClass1 : "+Thread.currentThread().getName());

		ThreadClass2 obj1 = new ThreadClass2();
		Thread obj2 = new Thread(obj1);
		obj2.setName("BienCaps");
		obj2.start();

		try{
			fun();
		}catch(ExceptionDiv45 me){
			System.out.println("Exception in Thread : "+Thread.currentThread().getName());
			me.printStackTrace();
		
		}
	}
}

class ThreadClass2  implements  Runnable{

	static int temp =0 ;	
	void gun() throws ExceptionDiv3{
	
		for(int i=1 ; i<=50 ; i++){
		
			if(i%3==0 ){
				temp++;
				if(temp>=8){
				
					throw new ExceptionDiv3("Exception Cause  --> Divisible by 3");
				}
			}
		}
	}

	public void run(){
		
		System.out.println("In Run of ThreadClass2 : "+Thread.currentThread().getName());
		
		try{
			gun();
		}catch(ExceptionDiv3 me){
			System.out.println("Exception in Thread : "+Thread.currentThread().getName());
			me.printStackTrace();	
		}
	
	}
}
