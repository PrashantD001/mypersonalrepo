class Demo extends Thread{

	public void run(){
	
		System.out.println("In Run");
	}

	public static void main(String[] pn){
	
		Demo d = new Demo();
		d.start();
		System.out.println(d.getName());		//  The Thread which is Created using this d object
								// i.e. --> O/P = Thread0

		Thread t1 = new Thread();
		t1.start();
		System.out.println(t1.getName());		// Thread1

		Thread t2 = new Thread();
		t2.start();
		System.out.println(t2.getName());		// Thread2

	}
}
