class Demo extends Thread{

	public void run(){
	
		System.out.println("In run");

	}

	public static void main(String[] pn){
	
		Demo d = new Demo();
		d.start();
		System.out.println(d.getName());	// Thread0

		d.setName("Shashi");
		System.out.println(d.getName()); 	// Shashi


		Thread t1 = new Thread();
		t1.start();
		System.out.println(t1.getName());	// Thread1

		t1.setName("Core2Web");
		System.out.println(t1.getName()); 	// Core2Web


		Thread t2 = new Thread();
		t2.start();
		System.out.println(t2.getName());	// Thread2

		t2.setName("BineCaps");
		System.out.println(t2.getName()); 	// BineCaps


	}
}
