class ThreadDemo implements Runnable{

	public void run(){
	
		System.out.println("In Run : In ThreadDemo");
	}

	public static void main(String[] pn){
	
		Thread t1 = new Thread();
		System.out.println(t1.getName());	// Thread-0

		Thread t2 = new Thread("C2W");
		System.out.println(t2.getName());	// C2W

		ThreadDemo obj = new ThreadDemo();
		Thread t3 = new Thread(obj);
		System.out.println(t3.getName());	// Thread-1
		
		ThreadDemo obj2 = new ThreadDemo();
		Thread t4 = new Thread(obj2, "PN");
		System.out.println(t4.getName());	// PN
		
	}
}
