// 303 
class ThreadSync extends Thread{

	synchronized void work(){
	
		for(int i=0; i<5 ; i++){
		
			System.out.println(Thread.currentThread().getName()+" Work");
			try{
			
				Thread.sleep(2000);
			}catch(Exception e){
			
			}
		}
	}
	public void run(){
		
		System.out.println(Thread.currentThread().getName()+" Start");
		work();
		System.out.println(Thread.currentThread().getName()+" End");
		
	}
}

class ThreadDemo{

	public static void main(String[] pn){
		ThreadSync t1 = new ThreadSync();
		t1.start();
		ThreadSync t2 = new ThreadSync();
		t2.start();
	}
}
