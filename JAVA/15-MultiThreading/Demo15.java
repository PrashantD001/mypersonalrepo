import java.io.*;

class MyException extends Exception{

	MyException(String str){
	
		super(str);
	}
}

class Demo extends Thread{

	void Fun() throws IOException{
	
		System.out.println("In Fun By : ");
		System.out.println(Thread.currentThread().getName());
		throw new IOException("IOEXceotion ");
	}
	void Gun() throws MyException{
	
		System.out.println("In Gun By : ");
		System.out.println(Thread.currentThread().getName());
		throw new MyException("MyEXceotion  --> PN");
	}
	public void run(){
	
		if(Thread.currentThread().getName().equals("C2W")){
			try{
				Fun();
			}catch  (IOException io ){
			
				io.toString();
				io.getMessage();
				io.printStackTrace();
			}
		}
		if(Thread.currentThread().getName().equals("PN")){
			try{	
				Gun();
			}catch(MyException me){
			
				me.printStackTrace();
			}
		}
	}

	public static void main(String[] pn){
	
		Demo d1 = new Demo();
		d1.setName("C2W");
		d1.start();

		Demo d2 = new Demo();
		d2.setName("PN");
		d2.start();

	}
}
