class JoinDemo extends Thread{

	static {
	
		System.out.println("In Static Run  : "+Thread.currentThread().getName());
	}

	static Thread x = null;

	public void run(){
	
		try{
			System.out.println("In Child  : ");	
			System.out.println(x.getName());		// Main
		
			x.join();		// Thread0 stops untill Main Thread is fully executed
		
		} catch (InterruptedException ie){
			
		}

		for(int i=0; i<=10; i++){
			System.out.println("Child Thread");
			try{
				Thread.sleep(1000);
			} catch(InterruptedException ei){
			
			}
		}
	}
}

class Demo{

	public static void main(String[] opn) throws InterruptedException{
	
		JoinDemo.x = Thread.currentThread();

		JoinDemo d = new JoinDemo();
		d.start();

		for(int i=0; i<=10; i++){
			
			Thread.sleep(1000);
			System.out.println("Main Thread");
		}

	}
}
