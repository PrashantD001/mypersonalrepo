
class Demo implements Runnable{

	public void run(){
	
		System.out.println(Thread.currentThread().getName());
		
		for(int i=0; i<5; i++){

			System.out.println("In runnable ");
		}

	}

	public static void main(String[] pn){
	
		System.out.println(Thread.currentThread().getName());
		
		Demo d = new Demo();
		Thread t = new Thread(d);

		t.start();


		for(int i=0; i<20; i++){
			
			System.out.println("In Main ");
		}

		System.out.println(Thread.currentThread().getName());

	}
}
