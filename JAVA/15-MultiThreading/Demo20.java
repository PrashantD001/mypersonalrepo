
class JoinDemo extends Thread{

	public void run(){
	
	/*	try{					// dosent Effect the OutPut
		
			Thread.sleep(20);
		} catch (InterruptedException ie){
		
		}*/

		for(int i=0; i<=20; i++){
			System.out.println("Child Thread");
		}
	}
}

class Demo{

	public static void main(String[] opn) throws InterruptedException{
	
		JoinDemo d = new JoinDemo();

		Thread.currentThread().join();			// joins own it's Thread(Main)    --> Never do that
								// Dangoruos
		d.start();

		for(int i=0; i<=20; i++){
		
			System.out.println("Main Thread");
		}

	}
}
