
class DaemonDemo extends Thread{

	public void run(){

		String str = Thread.currentThread().getName();
	
		for(int i=0 ; i<=2 ; i++){
		
			System.out.println("In Run : "+str);
	/*		try{
			
				Thread.sleep(1000);
			} catch (InterruptedException ie){
			
			}*/
		}
	}
}

class Demo{

	public static void main(String[] pn){

		String str = Thread.currentThread().getName();

		DaemonDemo obj1 = new DaemonDemo();
		DaemonDemo obj2 = new DaemonDemo();
		DaemonDemo obj3 = new DaemonDemo();
		DaemonDemo obj4 = new DaemonDemo();
		
		obj1.setDaemon(true);

		obj1.start();
		obj2.start();
		obj3.start();
		obj4.start();
		
		for(int i=0 ; i<=2 ; i++){
		
			System.out.println("In Main : "+str);

		/*	try{
			
				Thread.sleep(1000);
			} catch (InterruptedException ie){
				
			}*/
		}
	}
}
