class JoinDemo extends Thread{

	public void run(){
	
		for(int i=0; i<=10; i++){
		
			System.out.println("Child Thread");
		}
	}
}

class Demo{

	public static void main(String[] pn) throws InterruptedException{
	
		JoinDemo d1 = new JoinDemo();
		d1.join();				// InterChange the two Lines it gives New OutPut
		d1.start();

		for(int i=0; i<=10; i++){
		
			System.out.println("Main Thread");
		}
	}
}
