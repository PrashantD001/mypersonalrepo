

class ThreadClass2 implements Runnable{

	void gun(){
		System.out.println("In RunDemo  --> gun() : "+(10/0));
	}
	public void run(){
		
		System.out.println("In ThreadClass2 Class Thread is : " + Thread.currentThread().getName());

		gun();
	
	}
}
class ThreadClass1 extends Thread{

	void fun(){
	
		System.out.println("In ThreadDemo --> fun() ");
	}

	public void run(){
	
		System.out.println("In ThreadClass1 Class Thread is : " + Thread.currentThread().getName());

		ThreadDemo obj1 = new ThreadDemo();
		obj1 = null;
		try{
		
			obj1.fun();
		} catch (NullPointerException ne){
					System.out.println("Exception Occured In ThreadClass1 ");
		}

		ThreadClass2 obj2 = new ThreadClass2();
		Thread obj3 = new Thread(obj2);
		obj3.setName("BienCaps");
		obj3.start();

	}
}

class Demo{
	public static void main(String[] pn){
	
		System.out.println("In Demo Class Thread is : " + Thread.currentThread().getName());

		ThreadClass1 obj = new ThreadClass1();
		obj.setName("C2W");
		obj.setPriority(8);
		obj.start();
	}

}


