// 302
//
import java.util.concurrent.*;
//import java.util.concurrent.ExecuterService;

class PoolThread implements Runnable{

	void work(){
	
		System.out.println("In Work");
		try{
			Thread.sleep(2000);
		}catch(Exception e){
		
			e.printStackTrace();
		}

	}
	public void run(){
	
		System.out.println(Thread.currentThread().getName()+" - Start");
		work();
		System.out.println(Thread.currentThread().getName()+" - End");
	}
}

class ThreadPoolDemo{

	public static void main(String[] pn){
	
		ExecutorService obj = Executors.newFixedThreadPool(5);
		
		//ExecutorService obj = Executors.newFixedThreadPool(10);

		for(int i=0; i<=10 ; i++){
		
			PoolThread task = new PoolThread();
			obj.execute(task);
		}
//`		obj.shutDown();
	}
}
