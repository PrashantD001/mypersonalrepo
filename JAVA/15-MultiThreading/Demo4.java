class Parent{

	Parent(){
	
		System.out.println("In parent "+this);
		this.m1();
	}
	void m1(){
	
	}
}

class Child extends Parent{

	Child(){
	
		System.out.println("In Child  "+this);
	}

	void m1(){
	
		System.out.println("In m1");
	}

	public static void main(String[] pn){
	
		Child c = new Child();

	}

}
