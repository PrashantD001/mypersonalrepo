class ThreadDemo extends Thread{

	ThreadDemo(){
	
		super();

	}

	ThreadDemo(String name){
	
		super(name);
		
	}

	public void run(){
	
		System.out.println("In Run : In ThreadDemo");
	}

	public static void main(String[] pn){
	
		ThreadDemo t1 = new ThreadDemo();
		System.out.println(t1.getName());	// Thread-0

		ThreadDemo t2 = new ThreadDemo("C2W");
		System.out.println(t2.getName());	// C2W

		RunDemo obj = new RunDemo();
		ThreadDemo t3 = new ThreadDemo(obj);
		System.out.println(t3.getName());
		
	}
}

class RunDemo implements Runnable{


	public void run(){
	
		System.out.println("In Run : In RunDemo");
	}
}
