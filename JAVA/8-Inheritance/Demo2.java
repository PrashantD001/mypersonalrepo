class Parent{
	int x = 10;
	static int y = 20;
}

class Child extends Parent{
	
	int z = 39;
	static int p =40;

	void m1(){
		System.out.println("In m1");
		System.out.println(x);
		System.out.println(y);
		System.out.println(z);
		System.out.println(p);
	}
}

class Main{
	public static void main(String[] pn){
	
		Child obj = new Child();
		obj.m1();
	}
}
