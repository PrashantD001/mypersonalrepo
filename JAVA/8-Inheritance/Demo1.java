class Parent{
	void m1(){
		System.out.println("In m1");
	}
	static void m2(){
		System.out.println("In m2");
	}
}
class Child extends Parent{
	void m3(){
		System.out.println("In m3");
	}
}

class Main{
	public static void main(String[] pn){
		Child obj = new Child();

		obj.m1();
		obj.m2();
		obj.m3();
	}
}
