class Parent{

	int x =10;
	static int y=20;
	void m1(){
		System.out.println("In m1");
	}
	static void m2(){
		System.out.println("In m2");
	}
}


class Child{
	Parent p = new Parent();
}

class Demo{

	public static void main(String[] pn){
	
		Child c = new Child();

		c.p.m1();
		c.p.m2();
	}
}
