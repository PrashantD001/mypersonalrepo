//  Que 1) How many obects are created in inheritance ?

class Parent{
	int x=10;
	static int  y=20;

	Parent(){
		System.out.println("In Parent  Constructor");
	}
	static{
		System.out.println("In static block of Parent");
	}
}

class Child extends Parent{
	int a=30;
	static int  b=40;

	Child(){
		System.out.println("In Child  Constructor");
	}
	static{
		System.out.println("In static block of Child");
	}
}

class Demo{

	public static void main(String[] pn){
		
		Parent p =  new Parent();		// Comment his line it gives new output.

		Child c = new Child();
	}
}
