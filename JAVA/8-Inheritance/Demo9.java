// # COMPOSITION = 
// 		  Creating object of other class to access variables and methods of that class .

class Parent{

	int x = 10;
	void m1(){
	
	}
}

class Child{
	public static void main(String[] pn){

		Parent p = new Parent();

		System.out.println(p.x);
	
		p.m1();

	}
}
