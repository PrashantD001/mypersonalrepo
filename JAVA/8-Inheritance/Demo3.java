class Parent{

	Parent(){
		System.out.println(this);
	}
}

class Child extends Parent{

	Child(){
	
		System.out.println(this);
	}
}

class Demo{

	public static void main(String[] pn){
	
		Parent obj1 = new Parent();		// It makes new object on heap with new Address

		Child obj = new Child();
	}
}


/* # When a subclass(Child) object is created a saperate object of a super class(Parent) object will not be created.
 *
 *
 * # It is mendatory that when n object is created , the constructur is for sure called 
 * but it is not mandtory whwn a constructor is called object creation is mandatory.	*/
