class Parent{
	
	int x = 10;
	static int y = 20;
	Parent(){
			System.out.println("In Parent Constructor");
	}
	static{
		System.out.println("In Static block of Parent");
	}
}
class Child1 extends Parent{

	int p =50;
	static int q=60;
	Child1(){
		System.out.println("In Child1 Constructor");
	}
	static{
		System.out.println("In static block of Child1");
	}
}

class Child2 extends Parent{

	int a =80;
	static int b =90;
	Child2(){
		System.out.println("In Child2 Constructor");
	}
	static{
		System.out.println("In static block of Child2");
	}
}

class Demo{
	
	public static void main(String[] pn){
		
		Child1 obj1 = new Child1();

		Child2 obj2 = new Child2();
	}
}
