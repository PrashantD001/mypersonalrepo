// # compile time bindig

class Parent{
	static int a = 10;
	int b = 20;
	static{
		System.out.println("In Parent S-Block");
	}
	Parent(){
		System.out.println("In Parent Constructor");
	}
	void m1(){
		System.out.println(a);
		System.out.println(b);
	}
}

class Child extends Parent{
	static int a = 30;
	int b = 40;
	static{
		System.out.println("In Child S-Block");
	}
	Child(){
		System.out.println("In Child Constructor");
	}
}

class Demo{

	public static void main(String[] pn){
	
		Parent p = new Parent();

		Child c = new Child();

		c.m1();
	}
}
