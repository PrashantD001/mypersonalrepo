class Parent{

	int x =10;
	static int y=20;
	
	static{
		System.out.println("In Static of Parent");
	}
	Parent(){
		System.out.println("In Constructor of Parent");
	}
}


class Child{
	Parent p = new Parent();
	
	static {
		System.out.println("In static of Child");
	}
	Child(){
		System.out.println("In Constructor of Child");
	}
}

class Demo{

	public static void main(String[] pn){

//		System.out.println("In main Before");

		Child c = new Child();

//		System.out.println("In main After");

	}
}
