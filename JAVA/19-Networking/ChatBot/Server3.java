

import java.net.*;
import java.io.*;


class Client implements Runnable{

	static BufferedReader br = null;
	static Socket s = null;
	static PrintStream ps = null;

	public void run(){
	
		String str ;
		try{
			while(!(str = br.readLine()).equals("Bye")){

                        	System.out.println("server : "+str);
			}
			System.out.println("Chat Ended");

			ps.close();
			s.close();
			br.close();
                }catch(Exception e){
		}
	}

	public static void main(String[] pn)throws IOException{
	
		s = new Socket("localhost",1718);
		InputStream is = s.getInputStream();

		br = new BufferedReader(new InputStreamReader(is));

		OutputStream os = s.getOutputStream();
		ps = new PrintStream(os);

		Thread t1 = new Thread(new Client());
		t1.start();

		String str ;
		BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));
		while(((str =br2.readLine()).length()) != 0){
			ps.println(str);	
		}
	}
}
