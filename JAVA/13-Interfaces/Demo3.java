interface IndianCulture{

	void LifeStyle();

	void Festival();

}

class Maharastra implements IndianCulture{

	public void LifeStyle(){
		System.out.println("Feta, kurta , saree ");
	}

	public void Festival(){
		System.out.println("Ganesh festival");
	}
}

class Kerla implements IndianCulture{

	public void LifeStyle(){
		System.out.println("Lungi");
	}

	public void Festival(){
		System.out.println("Konan");
	}
}

class Person{

	public static void main(String[] pn){
	
		Maharastra m = new Maharastra();
		m.LifeStyle();
		m.Festival();

		Kerla k = new Kerla();
		k.LifeStyle();
		k.Festival();
	}
}
