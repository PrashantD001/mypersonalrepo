
interface A{

	void m1();
}

interface B extends A{

	void m2();
}

class Demo implements B{

	public void m1(){
	
		System.out.println("In m1");
	}
	public void m2(){
	
		System.out.println("In m2");
	}

	public static void main(String[] pn){
	
		A obj = new Demo();

		obj.m1();
		obj.m2();			// error --> cannot find symbol
	}
}
