interface A{

	default void m1(){
	
		System.out.println("In m1 - A");
	}
}

interface B extends A{

	default void m1(){
	
		System.out.println("In m1 - B");
	}
}

//class Demo implements B,A{				// Same result
class Demo implements A,B{

	public static void main(String[] pn){
	
		A obj = new Demo();
		obj.m1();
	}
}
