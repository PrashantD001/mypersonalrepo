class A{

	interface B{
	
		default void m1(){
		
			System.out.println("In m1-B");
		}
	}
}

//class Demo implements  A,B{			// Error --> Interface  expected here
class Demo implements  A.B{

	public static void main(String[] pn){
	
		Demo d = new Demo();
		d.m1();
	}
}
