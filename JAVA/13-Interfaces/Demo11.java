interface A{

	default  void m1(){
		System.out.println("In default method");
	}

	static void m2(){
		System.out.println("In static method");
	}

/*	void m3(){			// Error = interface abstract method cannot have body 
	
	}*/
}
