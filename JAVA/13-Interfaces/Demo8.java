interface A{

	int x = 10;		// --> public static final int x

	void m1();		// --> public abstract void m1();
}

class B implements A{

	public void m1(){
	
		System.out.println("In m1()");

		System.out.println(x);		// after commenting this line there is bo bypush in bytecode
	}
}

class Demo{

	public static void main(String[] pn){
	
		A obj = new B();

		obj.m1();
	}
}
