class C2W{

	void prog(){
		System.out.println("Program");
	}
}

class MD{

	void math(){
		System.out.println("Maths");
	}
}
		// * Composition * \\:
class Student extends C2W{

	MD obj = new MD();

	void edu(){
	
		obj.math();
		prog();
	}

	public static void main(String[] pn){
	
		Student s = new Student();

		s.edu();
	}
}

/*
class Student2 extends MD{

	void edu(C2W c){

		c.prog();
		math();
	}

	public static void main(String[] pn){

		Student2 s = new Student2();
		s.edu(new C2W());

	}
} */
