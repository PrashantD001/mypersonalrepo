
interface India{

	void Communication();

	void CM();
}

class Maharastra implements India{

	public void Communication(){
		System.out.println("Communication In Marathi");
	}

	public void CM(){
		System.out.println("Udhdhav Thakare");
	}
}

class UP implements India{

	public void Communication(){
		System.out.println("Communication In Hindi");
	}

	public void CM(){
		System.out.println("Yogi Adityanath");
	}
}

class Person{

	public static void main(String[] pn){
	
		India i1 = new Maharastra();
		i1.Communication();
		i1.CM();

		India i2 = new UP();
		i2.Communication();
		i2.CM();
	}
}
