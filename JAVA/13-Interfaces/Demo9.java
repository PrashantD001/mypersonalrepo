
//    Check the ByteCode of both Class 

class Demo{					// There is no static block 

	final static int x = 20;
}

interface A{

	final static int x = 30;

	int y = 40;
}
