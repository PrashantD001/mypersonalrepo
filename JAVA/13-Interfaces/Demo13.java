interface India{

	void Communication();             

	default void Anthom(){
		System.out.println(" Jan - Gn - Mn");
	}
}

class Maharashtra implements India{

	public void Communication(){
		System.out.println("Marathi");
		Anthom();
	}
}

class Panjab implements India{

	public void Communication(){
		System.out.println("Panjabi");
		Anthom();
	}
	public void Anthom(){
		System.out.println(" In Anthom - Panajab");
	}
}

class Demo{

	public static void main(String[] pn){
	
		Maharashtra m = new Maharashtra();
		m.Communication();
		m.Anthom();
	//	India.Anthom();				// Error = Nonstatic method anthom() cannot be reffered from static context

		Panjab p = new Panjab();
		p.Communication();
		p.Anthom();

	}
}
