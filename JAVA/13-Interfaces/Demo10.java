class Demo{

	final static int x = 10;
}

interface A{

	final static int x = 20;
	int y = 30;
}

//class B implements A extends Demo{				// Error

class B extends Demo implements A{

	void M1(){
		System.out.println(x);				// Error = reference to x is ambigious
		System.out.println(Demo.x);			// 10
		System.out.println(A.x);			// 20
		System.out.println(y);				// 30

	}
}

interface D{

	final static int x = 50;
}

class C extends Demo implements A,D{

	int x = 60;

	void M2(){
		System.out.println(x);				// 60
		System.out.println(Demo.x);			// 10
		System.out.println(A.x);			// 20
		System.out.println(y);				// 30

	}
}
