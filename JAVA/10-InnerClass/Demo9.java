class Outer{

	int x = 10;

	class Inner1{
	
		int x = 100;

		class Inner2{
		
			int x = 1000;

			void m1(){
			
				System.out.println(this.x);
				System.out.println(x);					// Runs

				System.out.println(Inner1.this.x);
			//	System.out.println(Outer.Inner1.x);			// Error

				System.out.println(Outer.this.x);
			//	System.out.println(Outer.x);				// Error
				
			}
		}
	}

	public static void main(String[] pn){
	
		Outer o = new Outer();

		Outer.Inner1 i1 = o.new Inner1();

		Outer.Inner1.Inner2 i2 = i1.new Inner2();

		i2.m1();
	}
}
