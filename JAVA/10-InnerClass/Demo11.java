class Outer{

	class Inner{
	
		int x = 10;

		void m1(){
		
			System.out.println(x);
		}
	
	}

	static void m2(){
		
		Outer o = new Outer();

		Outer.Inner i1 = o.new Inner();

		Inner i2 =o.new Inner();

		i1.m1();

		i2.m1();
	}

	public static void main(String[] pn){
	
		m2();

	//	Outer.m2();

	}
}
