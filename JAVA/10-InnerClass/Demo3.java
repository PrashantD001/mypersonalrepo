class Outer{
	int x = 10;
	static int y = 20;

	void m1(){
		System.out.println("In M1");
	}

	static void m2(){
		System.out.println("In M2");
	}

	class Inner{
		int x = 120;
	
		void m1(){

			System.out.println(x);
			System.out.println(y);
			System.out.println(Outer.y);
			
			Outer.m1();
			Outer.m2();
		}
	}
}

class Demo{

	public static void main(String[] pn){
	
		Outer o = new Outer();

		Outer.Inner i = o.new Inner();

		i.m1();
	}
}
