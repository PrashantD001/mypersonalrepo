class Outer{

	class Inner{
	
		int x = 10;

		void m1(){
			
			System.out.println(x);
		}
	}

	void m2(){							// void m2(this){     --> this == Outer Object
	
	/*:	Outer o = new Outer();
		Outer.Inner i = o.new Inner();

		i.m1();				*/

		Inner i2 = new Inner();

		i2.m1();

	}

	public static void main(String[] pn){
	
		Outer o = new Outer();

		o.m2();
	}
}
