class Parent{

	void m1(){
	
		System.out.println("In m1");
	}
}

class Child extends Parent{

	void m2(){
	
		System.out.println("In m2");
	}
}

class Demo{

	public static void main(String [] pn){
	
		Parent p = new Parent(){				// Referance = Parent      Object =  Parent
									// Parent p = new Demo$1();
		
			void m3(){
			
				System.out.println("In m3");
			}
		};

		Child c = new Child(){					// Referance = Child      Object = Child
									// Child c = new Demo$2();
		
			void m4(){
			
				System.out.println("In m4");
			}
		};

		Parent p1 = new Child(){				// Referance = Parent    Object = Child
									// Parent p1 = new Demo$3();
		
			void m5(){
			
				System.out.println("In m5");
			}
		};
	}
}
