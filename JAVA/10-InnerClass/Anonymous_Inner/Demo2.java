class Parent{

	void m1(){
	
		System.out.println("In m1");
	}
}

class Child extends Parent{

	void m2(){
	
		System.out.println("In m2");
	}
}

class Demo{

	public static void main(String [] pn){
	
		Parent p = new Parent(){
		
			void m3(){
			
				System.out.println("In m3");
			}
		}.m3();							// incompatible type --> Error = void cannot be converted into Parent

		Child c = new Child(){
		
			void m4(){
			
				System.out.println("In m4");
			}
		}.m4();							// incompatible type --> Error = void cannot be converted into Child

		Parent p1 = new Child(){
		
			void m5(){
			
				System.out.println("In m5");
			}
		}.m5();							// incompatible type --> Error = void cannot be coverted into Parent
	}
}
