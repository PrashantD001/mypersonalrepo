class Parent{

	void m1(){
	
		System.out.println("In m1 - Parent");
	}
}

class Child extends Parent{

	void m1(){
	
		System.out.println("In m1 - Child");
	}

	void m2(){
	
		System.out.println("In m2 - Child");
	}
}

class Demo{

	public static void main(String[] pn){
	
		Parent p = new Parent(){
			void m3(){
			
				System.out.println("In m3 - Demo$1");
			}
		};
		p.m1();								// runs due to parent child relation between Parent class and Demo$1 class
	//	p.m3();								// Error = Cannot find symbol 

	/*	Child c = new Child(){

			void m4(){
			
				System.out.println("In m4 - Demo$2");
			}		
		}.m4();								// Error = Incompatible Types : void cannot be converted into Child  

		*/

		new Parent(){
		
			void m5(){
			
				System.out.println("In m5 - Demo$3");
			}
		}.m5();

	}
}
