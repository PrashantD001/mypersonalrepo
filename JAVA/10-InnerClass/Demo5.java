class Outer{
	Outer(){
		System.out.println("In Outer constructor ");
		System.out.println(this);
//		System.out.println(this$0);			//Error
	}

	class Inner{
	
		Inner(){

			System.out.println("In Inner constructor ");
			System.out.println(this);
//			System.out.println(this$0);		//Error
		}
	}
}

class Demo{

	public static void main(String[] pn){
	
		Outer o = new Outer();

		Outer.Inner i = o.new Inner();


	}
}
