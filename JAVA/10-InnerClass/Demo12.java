class Outer{

	class Inner1{								// this$0 --> Outer this
	
		class Inner2{							// this$1 --> Inner this
		
			void m3(){						//m3(this)      this --> Inner2 class object or Inner2 this
			
				System.out.println("In m3");
			}
		}

		void m2(){							// m2(this)     this --> Inner1 class object or Inner1 this

			Inner2 i2 = new Inner2();				// Inner2 i2 = this.new Inner2();
										// Inner2 i2 = new Inner2( i2 , Inner1 this );

			i2.m3();						// i2.m3(i2);
		
			System.out.println("In m2");
		}
	}

	void m1(){								// m1(this)     this --> Outer class object or Outer this
	
		Inner1 i1 = new Inner1();					// Inner1 i1 = this.new Inner1();
										// Inner1 i1 = new Inner1( i1 , Outer this );

		i1.m2();							// i1.m2(i1);

		System.out.println("In m1");
	}

	public static void main(String[] pn){
	
		Outer o = new Outer();

		o.m1();								// o.m1(o);
	}
}
