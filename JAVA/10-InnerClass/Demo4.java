class Mall{
	int x =  10;
	static int y =  20;
	void m1(){
		System.out.println("In M1-mall");
	}

	class Shop{
		int x = 30;
		void m1(){
			System.out.println("In M1-shop");
		}
	}

	static void m2(){
		System.out.println("In M2-mall");
	}
}

class Demo{

	public static void main(String[] pn){
	
		Mall m = new Mall();
		//Shop s = new Shop();			//error
		//shop s = m.new Shop();		//error

		Mall.Shop s = m.new Shop();
	}
}
