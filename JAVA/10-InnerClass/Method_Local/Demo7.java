class Outer{

	int x = 10;
	static int y = 20;
	
//	void m1(){							// -->gives Different Output
	static void m1(){
		
		int z = 40;
		class  Inner{
			static int k = 50;
			void m2(){
			
	//			System.out.println(x);			// Error = Non static x 
				System.out.println(y);
				System.out.println(z);
				System.out.println(k);
			}
		}

		Inner i = new Inner();
		i.m2();
		System.out.println(i.k);
	}

	public static void main(String[] pn){
	
	//	m1();
		Outer.m1();

	}
}
