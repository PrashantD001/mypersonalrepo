class Outer{

	int x = 10;
	static int y = 20;

	static void m1(){
	
		class  Inner{
		
			void m2(){
			
				System.out.println("In m2");
			}
		}

		Inner i = new Inner();
		i.m2();
		System.out.println("In m1");
	}

	public static void main(String[] pn){
	
		m1();
		Outer.m1();

	}
}
