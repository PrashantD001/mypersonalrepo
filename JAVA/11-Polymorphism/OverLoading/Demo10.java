
class  Main{

	void m1(String str){
	
		System.out.println(" String ");
	}

	void m1(StringBuffer sb){
	
		System.out.println(" StringBuffer ");
	}

	public static void main(String[] pn){
	
		Main m = new Main();

		m.m1("PN");			// String str = "PN" --> matches
						// StringBuffer sb = "PN" --> no match 

		m.m1(null);			// String str = null --> matches
						// StringBuffer sb = null -->matches
						// both matches but there is no inheritance so it gives error
						// Error = referance to m1 is ambiguous

	}
}
