class Parent{
	Parent(){
		System.out.println(this + " in parent constructor");
	}
	void p1(){
		System.out.println(" in p1 --> " + this);
	}
}

class Child extends Parent{
	Child(){
		System.out.println(this + "  in Child constructor");
	}
	void p2(){
		System.out.println(" in p2 --> " + this);
	}
}

class  Main{
	Main(){
		System.out.println(this + " in Main Constructor");
	}
	void m1(Parent p){
	
		System.out.println(" m1 -  Parent --> "+ p );
	}

	void m1(Child c){
	
		System.out.println(" m1 - Child --> "+ c );
		c.p1();
		c.p2();
	}

	public static void main(String[] pn){
	
		Main m = new Main();

		m.m1(new Child());

	}
}
