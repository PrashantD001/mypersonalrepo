class Parent{
	void p1(){
		System.out.println(" in p1");
	}
}

class Child extends Parent{
	void p2(){
		System.out.println(" in p2");
	}
}

class  Main{

	void m1(Parent p){
	
		System.out.println(" m1 -  Parent");
	}

	void m1(Child c){
	
		System.out.println(" m1 - Child");
		c.p1();
		c.p2();
	}

	public static void main(String[] pn){
	
		Main m = new Main();

		m.m1(new Child());

	}
}
