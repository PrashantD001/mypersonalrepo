class Demo{

	void m1(int x , int y){
	
		System.out.println(" int - int ");
	}

	void m1(char  x , char y){
	
		System.out.println(" char - char ");
	}
}

class Main{

	public static void main(String[] pn){
	
		Demo d = new Demo();

		d.m1('A' , 'B');					// Error = reference to m1 is ambiguous
									// both method m1(int,char) in Demo and method m1(char,int) in Demo match

	}	
}
