class Parent{

}

class Child extends Parent{

}

class  Main{

	void m1(Parent p){
	
		System.out.println(" m1 -  Parent");
	}

	void m1(Child c){
	
		System.out.println(" m1 - Child");
	}

	public static void main(String[] pn){
	
		Main m = new Main();

		m.m1(new Parent());		// Parent p = new Parent() -->matches

		m.m1(new Child());		// parent p = new Child() --> matches
						// Child c = new Child() -->matches
						// both matches but there is parent child relation that's why priority goes to  Child

		m.m1(null);			// parent p = null --> matches
						// Child c = null -->matches
						// both matches but there is inheritance so priority goes to  Child

	}
}
