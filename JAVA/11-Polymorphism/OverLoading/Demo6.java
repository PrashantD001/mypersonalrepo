class Demo{

	void m1(double x , float y){
	
		System.out.println(" double - float ");
	}

	void m1(float  x , double y){
	
		System.out.println(" float - double ");
	}
}

class Main{

	public static void main(String[] pn){
	
		Demo d = new Demo();

		d.m1(10.5f,10.5f);					// Error = reference to m1 is ambiguous
									// both method m1(double,float) in Demo and method m1(float,double) in Demo match

	}	
}
