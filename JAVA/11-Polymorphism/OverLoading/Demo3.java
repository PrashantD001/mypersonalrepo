class Demo{

	void m1(double x){
	
		System.out.println(" m1 - double");
	}

	void m1(float x){
	
		System.out.println(" m1 - float");
	}

	void m1(long x){
	
		System.out.println(" m1 - long");
	}
}

class Main{

	public static void main(String[] pn){
	
		Demo d = new Demo();

		d.m1('A');
	}
}
