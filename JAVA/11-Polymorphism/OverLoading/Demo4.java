class Demo{

	void m1(int x , int y){
	
		System.out.println(" int - int ");
	}

	void m1(float  x , float y){
	
		System.out.println(" float - float ");
	}
}

class Main{

	public static void main(String[] pn){
	
		Demo d = new Demo();

		d.m1(10,10);

		d.m1(10.5f,10.5f);

		d.m1(10.5f,10);						// int can converted into float but float can't be converted into int 
									// if if dosen't get matching parameter it try to match existing parameter but it should be greater as
									// char --> int -->. long --> float --> double
	}	
}
