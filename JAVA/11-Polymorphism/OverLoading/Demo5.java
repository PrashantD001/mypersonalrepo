class Demo{

	void m1(int x , float y){
	
		System.out.println(" int - float ");
	}

	void m1(float  x , int y){
	
		System.out.println(" float - int ");
	}
}

class Main{

	public static void main(String[] pn){
	
		Demo d = new Demo();

		d.m1(10.5f,10.5f);					// Error = No suitable Method found m1(float , float)

	}	
}
