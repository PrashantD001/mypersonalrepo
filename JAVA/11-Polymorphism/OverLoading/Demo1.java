class Demo{

	void m1(){
		System.out.println(" void m1() - No para - 1");
	}

	int m1(){							// Error - m1() is already Defined
		System.out.println(" int m1() - No para ");
	}

	void m1(){							// Error  - m1() is already defined
		System.out.println(" void m1() - No para -2");
	}

}

class Main{

	public static void main(String[] pn){
	
		Demo d = new Demo();
	}
}
