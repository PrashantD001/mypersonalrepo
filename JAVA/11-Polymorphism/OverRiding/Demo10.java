class Parent{

	int m1(){
			
		return 10 ;		
	}
}

class Child extends Parent{

/*	char m1(){		// Error = m1() in Child cannot override m1() in Parent
				// return type void is not  compatible with int
	
		return 'A';
	}*/

	int m1(){
	

		return 'A' ;
	}
}
