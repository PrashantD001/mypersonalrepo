class Parent{

	void m1(){
		System.out.print("In Parent - m1");
	}
}

class Child1 extends Parent{

	final void  m1(){
		System.out.println("In Child1 - m1");
	}
}

class Child2 extends Child1{

	 void m1(){
		System.out.println("In Child2 - m1");
	}

	public static void main(String[] pn){
	
		Parent p = new Child2();

		p.m1();
	}
}
