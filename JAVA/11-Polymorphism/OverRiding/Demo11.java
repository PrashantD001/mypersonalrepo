class Parent{

	double m1(){
			
		return 10.5 ;		
	}
}

class Child extends Parent{

	float m1(){			// Error = m1() in child cannot be override m1() in Parent
					// return type float is not compatible with double

		return 10.5 ;		// Error = Incompatible types : possible loosy conversion from double to int
	}
}
