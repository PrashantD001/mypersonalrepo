

/*
 * 2. WAP to print the character whose ASCII is even.
 * */

#include<stdio.h>

void main(){

	char start,end;

	printf("Enter the Starting Character : \n");
	scanf(" %c",&start);
	printf("Enter the Ending Character : \n");
	scanf(" %c",&end);

	for(char i = start ; i <= end ; i++){
	
		if(i%2==0){
		
			printf("%c \n",i);
		}
	}
}
