

/*
 * 7. WAP to calculate the LCM of given two numbers.
 */

#include<stdio.h>

void main(){

	int num1,num2,start,lcm=0;

	printf("Enter the First Number\n");
	scanf("%d",&num1);
	printf("Enter the Second Number\n");
	scanf("%d",&num2);

	if(num1>num2){
		start=num1;
	}else{
		start=num2;
	}

	for (int i=start; i<=num1*num2; i++){
	
		if(i%num1==0 && i%num2==0){
		
			printf("In if -2 \n");
			lcm=i;
			break;
		}
	}
	if(lcm!=0 && num1>=0 && num2>=0){
		printf("The LCM of %d and %d is : %d \n",num1,num2,lcm);
	}else{
		printf("Invalid Input \n");
	}
}
