

/*
 *3. WAP to print all even numbers in reverse order and odd numbers in the standard
 way. Both separately. Within a range.
 * */

#include<stdio.h>

void main(){

	int start,end;

	printf("Enter the Starting Integer : \n");
	scanf(" %d",&start);
	printf("Enter the Ending Ending : \n");
	scanf(" %d",&end);

	printf("\nEven Number between Range %d to %d is \n",start,end);
	for(int i = end ; i >= start ; i--){
		if(i%2==0){
			printf("%d \n",i);
		}
	}
	
	printf("\nOdd Number between Range %d to %d is \n",start,end);
	for(int i = start ; i <= end ; i++){
		if(i%2!=0){
			printf("%d \n",i);
		}
	}
}
