
/*
 * 6. WAP to calculate the factorial of a given number.
 */

#include<stdio.h>

void main(){

	int num,fact=1;

	printf("Enter the Number : \n");
	scanf("%d",&num);

	for(int i=num; i>=1 ; i--){
	
		fact*=i;
	}
	printf("The Factorial of %d is : %d \n",num,fact);
}
