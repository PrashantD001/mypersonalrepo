

/*
 4. WAP to Find the sum of numbers that are divisible by 5 in the given range
 */

#include<stdio.h>

void main(){

	int start,end,sum=0;

	printf("Enter the Starting Integer : \n");
	scanf(" %d",&start);
	printf("Enter the Ending Ending : \n");
	scanf(" %d",&end);

	for(int i = start ; i <= end ; i++){
		if(i%5==0){
			sum+=i;
		}
	}
	
	printf("Sum of Number between Range %d to %d which are Devided by 5 is : %d \n",start,end,sum);
}
