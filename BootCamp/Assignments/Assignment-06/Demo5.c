

/*
 * 5. WAP to take the Number input and print all the factors of that number.
 */

#include<stdio.h>


void main(){

	int num;

	printf("Enter the Number : \n");
	scanf("%d",&num);


	printf("The Factors of %d are : \n",num);
	
	for(int i=1 ; i<=num ; i++){

		if(num%i==0){
		
			printf("%d \n",i);
		}
	}
}
