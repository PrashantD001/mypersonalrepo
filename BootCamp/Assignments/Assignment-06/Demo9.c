

/*
 * 9. WAP to calculate the square root of a number ranging from 100 to 300
 */

#include<stdio.h>

void main(){

	int start,end;

	printf("Enter the Starting : \n");
	scanf("%d",&start);
	
	printf("Enter the Ending : \n");
	scanf("%d",&end);

	int num;


	for(num = 1; num*num<=end ; num++){
	
		if(num*num>=start){
		
			printf("%d * %d : %d \n",num,num,num*num);
		}
	}
}
