
/*
 * 2. WAP to count digits in given no
 * Input: 94211
 * Output: digit count is 5
 */

#include<stdio.h>

void main(){

	int num;

	printf("Enter the Number : \n");
	scanf("%d",&num);

	int digit=0;
	while(num!=0){
	
		num/=10;
		digit++;
	}

	printf("Digit Count is : %d\n",digit);
}
