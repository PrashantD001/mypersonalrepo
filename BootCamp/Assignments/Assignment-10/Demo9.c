

/*
 * 9. take no of rows from the user
 * 			1
 * 		1 	2
 * 	1 	2 	3
 * 1 	2 	3 	4
 */

#include<stdio.h>

void main(){

	int row; 
	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	for(int i=1; i<=row; i++){
	
		for(int j=1; j<=row-i; j++){
		
			printf("\t");
		}
		for(int k=1; k<=i; k++){
		
			printf("%d\t",k);
		}
		printf("\n");
	}
}
