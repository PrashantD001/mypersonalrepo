

/*
 * 7. take no of rows from the user
 * E D C B A
 * E D C B
 * E D C
 * E D
 * E
 */

#include<stdio.h>

void main(){

	int row;

	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	for(int i=1; i<=row; i++){
	
		for(int j=1; j<=(row-i+1); j++){
		
			printf("%c\t",65+row-j);
		}
		printf("\n");
	}
}
