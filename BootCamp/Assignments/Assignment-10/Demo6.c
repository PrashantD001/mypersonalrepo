

/*
 * 6. take no of rows from the user
 * D e F g
 * e D c B
 * F g H i
 * g F e D
 */

#include<stdio.h>

void main(){

	int row;

	printf("Enter the Number of Rows : \n ");
	scanf("%d",&row);

	for(int i=1; i<=row; i++){
		
		char ch = 63+row+i;
	
		for(int j=1; j<=row; j++){

			if((i+j)%2==0){
			
				printf("%c\t",ch);
			}else{
				printf("%c\t",ch+32);
			}
			
			if(i%2!=0){
				ch++;
			}else{
				ch--;
			}
		}
		printf("\n");
	}
}
