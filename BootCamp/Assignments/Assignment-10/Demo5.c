

/*
 * 5. take no of rows from the user
 * 2 	3 	5
 * 7 	11 	13
 * 17 	19 	23
 */

#include<stdio.h>

void main(){

	int row,num,temp=0;

	printf("Enter Number of Rows : \n");
	scanf("%d",&row);

	num=2;
	
	for(int i=1; i<=row*row; i++){

		temp = 0;

		while(temp!=1){

			int flag =0;

			for(int j=2; j<=(num/2); j++){

				if(num%j==0){
					flag=1;
					break;
				}
			}		

			if(flag==0){
				printf("%d\t",num);
				temp=1;
			}
			num++;
		}

		if(i%row==0){
			printf("\n");
		}
	}
}
