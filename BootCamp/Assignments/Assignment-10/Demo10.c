

/*
 * 10. take no of rows from the user
 * D 	D 	D 	D
 * 	C 	C 	C
 * 		B 	B
 * 			A
 */

#include<stdio.h>

void main(){

	int row; 
	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	for(int i=1; i<=row; i++){
	
		for(int j=1; j<i; j++){
		
			printf("\t");
		}
		for(int k=1; k<=row-i+1; k++){
		
			printf("%c\t",65+row-i);
		}
		printf("\n");
	}
}
