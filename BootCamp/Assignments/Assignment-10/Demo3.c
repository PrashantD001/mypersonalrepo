

/*
 * 3. WAP to print all the composite numbers between a given range
 * Input : Enter Start 1
 * Enter End 15
 * Output:
 * 4 6 8 9 10 12 14 15
 * Input : Enter Start 31
 * Enter End 35
 * Output:
 * 32 33 34 35
 */

#include<stdio.h>

void main(){

	int start,end;

	printf("Enter Start : \n");
	scanf("%d",&start);
	
	printf("Enter End : \n");
	scanf("%d",&end);

	for(int i=start; i<=end; i++){
	
		int flag =0;
		for(int j=2; j<=(i/2); j++){
			if(i%j==0){
				flag=1;
				break;
			}
		}
		if(flag==1){
		
			printf("%d , ",i);
		}
	}
	printf("\n");
}
