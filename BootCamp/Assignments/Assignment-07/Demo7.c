

/*
 * 7. If possible take no of rows from the user
 * 1 	2 	9 	4
 * 25 	6 	49 	8
 * 81 	10 	121 	12
 * 169 	14 	225 	16
 */


#include<stdio.h>

void main(){

	int row,num=1;

	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	for(int i = 1; i<=row; i++){
		for(int j =1 ; j<=row ; j++,num++){
		
			if(j%2==0){
				printf("%d \t",num);
			}else{
				printf("%d \t",num*num);
			}
		}
		printf("\n");
	}
}
