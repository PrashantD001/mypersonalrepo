

/*
 * 6. If possible take no of rows from the user
 * $ = $ =
 * $ = $ =
 * $ = $ =
 * $ = $ =
 */


#include<stdio.h>

void main(){

	int row;

	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	for(int i = 1; i<=row; i++){
		for(int j =1 ; j<=row ; j++){
		
			if(j%2==0){
				printf("= \t");
			}else{
				printf("$ \t");
			}
		}
		printf("\n");
	}
}
