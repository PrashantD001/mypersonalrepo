

/*
 * 8. If possible take no of rows from the user
 * 1 3 5
 * 7 9 11
 * 13 15 17
 */


#include<stdio.h>

void main(){

	int row,num=1;

	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	for(int i = 1; i<=row; i++){
		for(int j =1 ; j<=row ; j++,num+=2){
			printf("%d \t",num);
		}
		printf("\n");
	}
}
