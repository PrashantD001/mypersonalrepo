

/*
 * 10. If possible take no of rows from the user
 * D4 C3 B2 A1
 * D4 C3 B2 A1
 * D4 C3 B2 A1
 * D4 C3 B2 A1
 */


#include<stdio.h>

void main(){

	int row;
	char ch;

	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	for(int i = 1; i<=row; i++){
		ch = 64+row;
		for(int j =1 ; j<=row ; j++,ch--){
		
			printf("%c%d \t",ch,row-(j-1));
		}
		printf("\n");
	}
}
