

/*
 * 9. If possible take no of rows from the user
 * 2 5 10
 * 17 26 37
 * 50 65 82
 */


#include<stdio.h>

void main(){

	int row,num=1;

	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	for(int i = 1; i<=row; i++){
		for(int j =1 ; j<=row ; j++,num++){
			printf("%d \t",num*num+1);
		}
		printf("\n");
	}
}
