

/*
 * 1. If possible take no of rows from the user
 * 1 2 3 4
 * 2 3 4 5
 * 3 4 5 6
 * 4 5 6 7
 */


#include<stdio.h>

void main(){

	int row;

	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	for(int i = 1; i<=row; i++){
	
		for(int j =1 ; j<=row ; j++){
		
			printf("%d \t",i+j-1);
		}
		printf("\n");
	}
}
