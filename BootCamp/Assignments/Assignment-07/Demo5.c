

/*
 * 5. If possible take no of rows from the user
 * A B C D
 * B C D E
 * C D E F
 * D E F G
 */


#include<stdio.h>

void main(){

	int row;
	char ch1='A',ch2='A';

	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	for(int i = 1; i<=row; i++){
		ch2=ch1++;
		for(int j =1 ; j<=row ; j++,ch2++){
		
			printf("%c \t",ch2);
		}
		printf("\n");
	}
}
