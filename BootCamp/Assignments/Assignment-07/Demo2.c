

/*
2. If possible take no of rows from the user
1 2 3
a b c
1 2 3
a b c
 */


#include<stdio.h>

void main(){

	int row;
	char ch;

	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	for(int i = 1; i<=row; i++){
		ch='a';
		for(int j =1 ; j<=row ; j++,ch++){
		
			if(i%2==0){
				printf("%c \t",ch);
			}else{
				printf("%d \t",j);
			}
		}
		printf("\n");
	}
}
