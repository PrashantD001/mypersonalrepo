#include<stdio.h>
#include<stdlib.h>
#include<string.h>
typedef struct searching {
	int data;
	struct searching *next;
}search;
search *head = NULL;
void addnode() {
	search *newnode = (search*)malloc(sizeof(search));
	printf("enter an integer data\n");
	scanf("%d",&newnode->data);
	newnode->next = NULL;
	if (head == NULL) {
		head = newnode;
	}
	else {
		search *temp = head;
		while (temp->next != NULL) {
			temp = temp -> next;
		}
		temp->next = newnode;
	}
}
void addition_of_all_nodes() {
	search *temp = head;
	int sum = 0;
	while (temp != NULL) {
		sum = sum +  temp->data;
		temp = temp -> next;
	}
	printf("the sum of all nodes data is %d\n",sum);
}
void addition_of_first_and_last_data_of_node(int nodes) {
	search *temp = head;
	for (int i = 1;i<nodes;i++){
		temp = temp->next;
	}
	int sum = head->data + temp->data;
	printf("the addition of first and last elements is %d\n",sum);
}
void maximum_and_minimum() {
	int min = head->data;
	int max = head->data;
	search *temp = head;
	while (temp != NULL) {
		if (temp->data < min) {
			min = temp->data;
		}
		if (temp->data > max) {
			max = temp->data;
		}
		temp = temp->next;
	}
	printf("the minimum data in this linked list is %d\n",min);
	printf("the maximum data in this linked list is %d\n",max);
}
void check_prime_no() {
	search *temp = head;
	int count = 0;
	while (temp != NULL) {
		int cnt = 0;
		int var = temp->data;
		for (int i = 1;i<=var;i++) {
			if (var%i == 0) {
				cnt++;
			}
		}
		if (cnt == 2) {
			printf("%d\n",var);
			count++;
		}
		temp = temp->next;
	}

	if (count == 0) {
		printf("there is no prime no in this linked list\n");
	}
}

void main() {
	int nodes;
	printf("enter the no of nodes that you want in linked list\n");
	scanf("%d",&nodes);
	for (int i = 0;i<nodes;i++) {
		addnode();
	}
	addition_of_all_nodes();
	addition_of_first_and_last_data_of_node(nodes);
	maximum_and_minimum();
	check_prime_no();

}



