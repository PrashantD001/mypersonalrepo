

// WAP print the value of the below expressions

#include<stdio.h>

void main(){

	int x= 9;

	int ans = ++x + x++ + ++x;
	printf("ans : %d\n",ans);		// 33
	printf("x : %d\n\n",x);			// 12
	
	int ans1 = ++x + ++x + ++x + ++x;
	printf("ans : %d\n",ans1);		// 59 
	printf("x : %d\n\n",x);			// 16

	int ans2 = x++ + x++ + ++x + x++ + ++x;
	printf("ans : %d\n",ans2);		// 92
	printf("x : %d\n\n",x);			// 21
	
	int ans3 = x++ + x++ + x++ + x++;
	printf("ans : %d\n",ans3);		// 90
	printf("x : %d\n\n",x);			// 25


}
