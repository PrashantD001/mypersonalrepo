

// 10 WAP , Write a program to check if a Character is a vowel or consonant . Take all values from the user

#include<stdio.h>

void main(){

	char chr;

	printf("Enter the character : \n");
	scanf(" %c",&chr);

	if(chr=='a' || chr<='e' || chr=='i' || chr=='o' || chr=='u'){

		printf("%c is a Vowel \n",chr);

	}else if(chr=='A' || chr<='E' || chr=='I' || chr=='O' || chr=='U'){

		printf("%c is a Vowel \n",chr);

	}else{

		printf("%c is a Consonant \n",chr);

	}

}


