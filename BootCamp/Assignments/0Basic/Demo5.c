

// 5 WAP to take a numerical input from user and find whether the number is divisible by 5  and  11

# include<stdio.h>

void main(){

	int num1;

	printf("Enter the First Value : \n");
	scanf("%d",&num1);

	if(num1%5==0 && num1%11==0){
		printf("%d is divisible by 5 & 11 \n",num1);
	}else{	
		printf("%d is Not divisible by 5 & 11 \n",num1);
	}
}
