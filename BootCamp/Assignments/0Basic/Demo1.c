

// 1  Write a program to print value and size of the below variables. Take all the values from user



# include<stdio.h>

void main(){

	int num;
	char chr;
	float rs;
	double crMoney;

	printf("Enter the integer value of num : \n");
	scanf("%d",&num);

	printf("Enter the Character value of chr : \n");
	scanf(" %c",&chr);
	
	printf("Enter the float value of rs : \n");
	scanf("%f",&rs);
	
	printf("Enter the Double value of crMoney : \n");
	scanf("%lf",&crMoney);


	printf("num = %d \nsize : %lu\n\n",num,sizeof(num));

	printf("chr = %c \nsize : %lu\n\n",chr,sizeof(chr));

	printf("rs = %f \nsize : %lu\n\n",rs,sizeof(rs));

	printf("crMoney = %lf \nsize : %lu\n\n",crMoney,sizeof(crMoney));


}

