

/*
 * 10. If possible take no of rows from the user
 * D4 C3 B2 A1
 * D4 C3 B2 A1
 * D4 C3 B2 A1
 * D4 C3 B2 A1
 */


#include<stdio.h>

void main(){

	int row;
	char ch;

	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	for(int i = 1; i<=row; i++){
		if(i%2==0){
			ch = 65;
		}else{
			ch = 64+row;
		}
		for(int j =1 ; j<=row ; j++){
			if(i%2==0){
				printf("%c%d \t",ch,j);
				ch++;
			}else{
				printf("%c%d \t",ch,row-(j-1));
				ch--;
			}
		}
		printf("\n");
	}
}
