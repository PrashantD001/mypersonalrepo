

/*
 * 2. take no of rows from the user
 * 3 2 1
 * c b a
 * 3 2 1
 * c b a
 */


#include<stdio.h>

void main(){

	int row;
	char ch;

	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	for(int i = 1; i<=row; i++){
		ch = 64+row;
		for(int j =1 ; j<=row ; j++,ch--){
		
			if(i%2==0){
				printf("%c \t",ch);
			}else{
				printf("%d \t",row-(j-1));
			}
		}
		printf("\n");
	}
}
