

/*
 * 8. take no of rows from the user
 * 18 16 14
 * 12 10 8
 * 6  4  2
 */


#include<stdio.h>

void main(){

	int row,num;

	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	num=2*(row*row);
		
	for(int i = 1; i<=row; i++){
		for(int j =1 ; j<=row ; j++,num-=2){
			
			printf("%d \t",num);
		}
		printf("\n");
	}
}
