

/*
 * 9. take no of rows from the user
 * 1 	3 	8
 * 15 	24 	35
 * 48 	63 	80
 */


#include<stdio.h>

void main(){

	int row,num=2,temp=1;

	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);
		
	for(int i = 1; i<=row; i++){
		for(int j =1 ; j<=row ; j++,num++){
		
			printf("%d \t",temp);
			temp=num*num-1;
		}
		printf("\n");
	}
}
