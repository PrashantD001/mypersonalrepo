

/*
* 1. take no of rows from the user
* 4 3 2 1
* 5 4 3 2
* 6 5 4 3
* 7 6 5 4
*/


#include<stdio.h>

void main(){

	int row,num,temp;

	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	temp=row;

	for(int i = 1; i<=row; i++){
		num=temp++;
		for(int j =1 ; j<=row ; j++,num--){
		
			printf("%d \t",num);
		}
		printf("\n");
	}
}
