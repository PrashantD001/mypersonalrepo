

/*
 * 4. take no of rows from the user
 * I H G
 * F E D
 * C B A
 */


#include<stdio.h>

void main(){

	int row;
	char ch;

	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);
		
	ch = 64+(row*row);

	for(int i = 1; i<=row; i++){
		for(int j =1 ; j<=row ; j++,ch--){
		
			printf("%c \t",ch);
	
		}
		printf("\n");
	}
}
