

/*
 * 5. take no of rows from the user
 * D C B A
 * e d c b
 * F E D C
 * g f e d
 */


#include<stdio.h>

void main(){

	int row;
	char ch1,ch2,x,y;

	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);
		
	ch1 = 64+row;
	ch2 = 96+row;

	for(int i = 1; i<=row; i++){
		x=ch1;
		y=ch2;
		for(int j =1 ; j<=row ; j++,x--,y--){
		
			if(i%2==0){
				printf("%c \t",y);
			}else{
				printf("%c \t",x);
			}
	
		}
		ch1++;
		ch2++;
		printf("\n");
	}
}
