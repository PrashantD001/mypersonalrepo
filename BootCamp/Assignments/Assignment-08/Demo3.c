

/*
 * 3. take no of rows from the user
 * 4 4 4 4
 * 3 3 3 3
 * 2 2 2 2
 * 1 1 1 1
*/


#include<stdio.h>

void main(){

	int row,num;

	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	num=row;

	for(int i = 1; i<=row; i++){

		for(int j =1 ; j<=row ; j++){
		
			printf("%d \t",num);
		}
		num--;
		printf("\n");
	}
}
