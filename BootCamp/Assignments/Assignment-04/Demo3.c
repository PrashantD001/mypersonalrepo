

/*
 * Program 3 :
 * WAP to print the divisors & count of divisors of the entered num.
 * Input: 15
 * Output: the divisors are 1 3 5 15
 * The count of divisors is 4
 * Additions of divisors 24
 * */

#include<stdio.h>

void main(){

	int num,sum=0,count=0;

	printf("Enter the Number : \n");
	scanf("%d",&num);

	printf("\nThe Divisors are : ");
	for(int i=1; i<=num; i++){
	
		if(num%i==0){
			printf("%d ",i);
			count++;
			sum+=i;
		}
	}
	printf("\nThe Count of Divisors is : %d\n",count);
	printf("Addition of Divisors is : %d\n",sum);
}
