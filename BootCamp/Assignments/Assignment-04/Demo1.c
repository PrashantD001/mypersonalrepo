
/*
 * Program 1 :
 * WAP to Find the sum of numbers that are not divisible by 3 up to a given number
 * Input: 10
 * Output: sum of numbers not divisible by 3 is 37
 * */

#include<stdio.h>

void main(){

	int num,sum=0;

	printf("Enter the Number : \n");
	scanf("%d",&num);

	for(int i=1 ; i<=num ; i++){
		if(i%3!=0){
			sum+=i;
		}
	}
	printf("Sum of Numbers not Divisible by 3 is %d \n",sum);
}
