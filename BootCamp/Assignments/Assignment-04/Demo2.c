
/*
 * Program 2:
 * Write a program to print the addition of 1 to 10 with 10 to 1.
 * Output:
 * 1 + 10 = 11
 * 2 + 9 = 11
 * 3 + 8 = 11
 * .
 * .
 * 10 + 1 = 11
 * */

#include<stdio.h>

void main(){
	int x,y;
	printf("Enter Starting : \n");
	scanf("%d",&x);
	printf("Enter Ending : \n");
	scanf("%d",&y);

	int j = y;
	for(int i=x; i<=y; i++){
		printf("%d + %d = %d \n",i,j,i+j);
		j--;
	}
}
