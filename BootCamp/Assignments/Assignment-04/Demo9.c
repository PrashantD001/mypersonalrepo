
/*
 * Program 9: Take an input number from the user and print the number in reverse
 * Input: 120654
 * Output: 456021
 * */

#include<stdio.h>

void main(){

	int num,temp,num2=0;
	printf("Enter the Number : \n");
	scanf("%d",&num);

	temp=num;

	if(num>0){
		while(num!=0){
			int rem = num%10;
			num=num/10;
			num2=num2*10+rem;
		}
		printf("Reverse Number of %d is %d \n",temp,num2);
	}else{
		printf("Please Enter Positive Number \n");	
	}
}
