
/*
 * Program 8: Take input from the user and print the product of digits
 * Input = 134
 * Output = product of digits is 12
 * * */

#include<stdio.h>

void main(){

	int num,temp,mul=1;
	printf("Enter the Number : \n");
	scanf("%d",&num);

	temp=num;

	if(num>0){
		while(num!=0){
			int rem = num%10;
			num=num/10;
			mul=mul*rem;
		}
		printf("Product of Digits in %d is %d \n",temp,mul);
	}else{
		printf("Please Enter Positive Number \n");	
	}
}
