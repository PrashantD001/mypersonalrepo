
/*
 * Program 6 :
 * Take an input number from the user and count the no of digits.
 * Input = 2534
 * Output = Number of Digits in 2534 is 4
 * */

#include<stdio.h>

void main(){

	int num,temp,count=0;
	printf("Enter the Number : \n");
	scanf("%d",&num);

	temp=num;

	if(num>0){
		while(num!=0){
			num=num/10;
			count++;
		}
		printf("Number of Digits in %d is %d \n",temp,count);
	}else{
		printf("Please Enter Positive Number \n");	
	}
}
