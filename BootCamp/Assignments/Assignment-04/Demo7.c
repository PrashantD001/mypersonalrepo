
/*
 Program 7 :
Take an input number from the user and print the sum of digits.
Input = 1234
Output = sum of digits is 10
 * */

#include<stdio.h>

void main(){

	int num,temp,sum=0;
	printf("Enter the Number : \n");
	scanf("%d",&num);

	temp=num;

	if(num>0){
		while(num!=0){
			int rem = num%10;
			num=num/10;
			sum=sum+rem;
		}
		printf("Sum of Digits in %d is %d \n",temp,sum);
	}else{
		printf("Please Enter Positive Number \n");	
	}
}
