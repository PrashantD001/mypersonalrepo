

/*
 * Program 10 :
 * Take a number from the user and print the Fibonacci series up to that number.
 * Input : 10
 * Output 0 1 1 2 3 5 8
 * */

#include<stdio.h>

void main(){

	int a=0;
	int b=1;
	int  c,num;

	printf("Enter the Limit : \n");
	scanf("%d",&num);

	while(a<=num){
	
		printf("%d \n",a);
		c=a+b;
		a=b;
		b=c;
	}
}
