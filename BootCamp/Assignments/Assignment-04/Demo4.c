

/*
 * Program 4:
 * Write a program to take a number as input and print whether that is a prime
 * number or not.
 * {Note: Prime number is the one which is divisible by 1 and that
 * number only}
 * Input: 41
 * Output: 41 is the prime number!
 * */

#include<stdio.h>

void main(){

	int num,count=0;
	printf("Enter the Number : \n");
	scanf("%d",&num);

	if(num>1){
		for(int i=1; i<=num/2; i++){
			if(num%i==0){
				count++;
			}
		}
		if(count==1){
			printf("%d is a Prime Number \n",num);
		}else{
			printf("%d is Not a Prime Number \n",num);
		}
	
	}else if(num==1){
		printf("1 is Prime Number \n");
	}else{
		printf("please Enter Positive Number ");
	}
}
