
//		SEARCH AN ELEMENT IN AN ARRAY

#include<stdio.h>

void main(){

	// taking variable n for input length from user
	int n;	

	// taking variable num to take number from user
	int num;

	// taking variable temp to use as flag
	int temp=0;

	// taking length of array from user
	printf("Enter the length of Array : \n");
	scanf("%d",&n);

	// checking array length is greater than zero
	if(n>0){

		// creating int array of length n
		int arr[n] ;
		
		// taking inputs in array
		printf("Enter the Elements in Array : \n");
		for(int i=0; i<n; i++){
			scanf("%d",&arr[i]);
		}	
	
		// taking number from user
		printf("Enter the Number : \n");
		scanf("%d",&num);
	
		// loop to check given number is present or not
		for(int i=0; i<n; i++){
			if(arr[i]==num){
				temp=1;
				printf("Given Number at index : %d\n",i);
				// if number is found breaking the loop
				break;
			}
		}
		if(temp==0){
			printf("Given Number Not found \n");
		}
	}else{
		printf("Invalid Length of Array \n");
	}	

}
