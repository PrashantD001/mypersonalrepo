
//		Kth SMALLEST ELEMENT

#include<stdio.h>

void main(){

	// taking varible n to take length of array
	int n;

	// taking varible num to ask user which smallest number is he want to print
	int num;

	// taking variable temp for swaping use
	int temp;
	
	// taking length of array from user
	printf("Enter the length of Array : \n");
	scanf("%d",&n);

	// checking length of array is greater than zero
	if(n>0){

		// creating int array of length of n
		int arr[n] ;
	
		// taking input for array from user
		printf("Enter the Elements in Array : \n");
		for(int i=0; i<n; i++){
			scanf("%d",&arr[i]);
		}	
	
		//loop to sort the array in assending order
		for(int i=0; i<n; i++){
			for(int j=i+1; j<n; j++){
				if(arr[i]>=arr[j]){
					temp=arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
				}
			}	
		}/*
		// pring sorted array
		for(int i=0; i<n; i++){
			printf("%d -> ",arr[i]);
		}*/
	
		// taking input number from user
		printf("Enter the Number K : \n");
		scanf("%d",&num);
	
		// checking given number is valid or not
		if(num<=n && num>=0){
			printf("The %d smallest Number is : %d\n",num,arr[num-1]);
		}else{
			printf("Invalid Input \n");
		}	
	}else{
		// if length of array is invalid 
		printf("Invalid Length of Array \n");
	}

}
