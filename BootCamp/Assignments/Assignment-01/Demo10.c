
//			MAXIMUM PRODUCT OF TWO NUMBERS

#include<stdio.h>

void main(){

	// taking varible n for length of array
	int n;

	// taking varible max to get maximum product
	int max=0;

	// taking flag to chck array input is positive or not
	int flag =1;

	// taking length of array from user
	printf("Enter the length of Array : \n");
	scanf("%d",&n);

	// checking length of array is greater than zero
	if(n>0){

		// creating int array of length of n
		int arr[n] ;
	
		// taking input for array from user
		printf("Enter the Elements in Array : \n");
		for(int i=0; i<n; i++){
			scanf("%d",&arr[i]);
			if(arr[i]<0){
				printf("Entered Negetive Input \n");
				flag=0;
				break;
			}
		}	
		
		//checking that length of array is one or not
		if(n!=1 && flag!=0){

			// loop for making all possible products
			for(int i=0; i<n; i++){
				for(int j=i+1; j<n; j++){
					if((arr[i]*arr[j])>=max){
						max=arr[i]*arr[j];
					}
				}
			}
			printf("The maximun Product is : %d\n",max);	
		}else if(flag!=0){
			printf("Length of Array is one \n");
		}
	}else{
		// if length of array is invalid
		printf("Invalid Length of Array \n");
	}

}
