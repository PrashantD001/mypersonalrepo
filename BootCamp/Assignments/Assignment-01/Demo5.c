
//		THIRD LARGEST ELEMENT

#include<stdio.h>

void main(){

	// taking variable n for input length of array from user
	int n;	

	// taking variable max1,max2,max3 to store first,second,third maximum varibles from array
	int max1=-1;
	int max2=-1;
	int max3=-1;

	// taking length of array from user
	printf("Enter the length of Array : \n");
	scanf("%d",&n);
	
	// checking length of array is greater than zero
	if(n>0){
		
		// creating int array of length n
		int arr[n] ;
	
		// taking inputs from user for array 
		printf("Enter the Elements in Array : \n");
		for(int i=0; i<n; i++){
			scanf("%d",&arr[i]);
		}	

		// loop for checking conditions for third maximum number
		for(int i=0; i<n; i++){
			if(arr[i]>max1){
				max3=max2;
				max2=max1;
				max1=arr[i];
			}else if(arr[i]>max2 && arr[i]<max1){
				max3=max2;
				max2=arr[i];
			}else if(arr[i]>max3 && arr[i]<max2 && arr[i]<max1){
				max3=arr[i];
			}
		}	
		printf("The Third Maximum is : %d\n",max3);
	}else{
		printf("Invalid Length of Array \n");
	}

}
