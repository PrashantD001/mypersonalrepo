
// 		MAXIMUM AND MINIMUM

#include<stdio.h>

void main(){

	// taking variable n for input length from user
	int n;	

	// taking variable max for storing maximun numbre from array
	int max=0;

	// taking variable min for storing minimun numbre from array
	int min=0;

	// taking length of array from user
	printf("Enter the length of Array : \n");
	scanf("%d",&n);
	
	// checking array length is greater than zero
	if(n>0){

		// creating int array of length of n
		int arr[n] ;
	
		// taking inputs in array
		printf("Enter the Elements in Array : \n");
		for(int i=0; i<n; i++){
			scanf("%d",&arr[i]);
		}	
		// assigning first array elemnt to max and min variable
		max=arr[0];
		min=max;

		// loop to check condition for maximum and minimum element in array
		for(int i=0; i<n; i++){
			if(arr[i]<=min){
				min=arr[i];
			}
			if(arr[i]>=max){
				max=arr[i];
			}
		}	
		printf("The Maximum is : %d\n",max);
		printf("The Minimum is : %d\n",min);
	
	}else{
		printf("Invalid Length of Array \n");
	}

}
