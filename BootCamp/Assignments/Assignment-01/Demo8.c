
//			ARRANGING AN ARRAY

#include<stdio.h>

void main(){

	// taking variable n to take length of array
	int n;

	// taking length of array from user
	printf("Enter the length of First Array : \n");
	scanf("%d",&n);
	
	// checking length of array is greater than zero
	if(n>0){

		// creating int array of length n
		int arr[n] ;
		
		// taking input in array from user
		printf("Enter the Elements in Array : \n");
		for(int i=0; i<n; i++){
			scanf("%d",&arr[i]);
		}	
	
		int temp =0;

		for(int i=0; i<n-1; i++){
			int flag = 1;
			int index = 0;
			if(arr[i]>=0 ){
				for(int j=i+1;j<n;j++){
					if(arr[j]<0){
						flag=0;
						index=j;
						break;
					}
				}
				if(flag==0){
					for(int k=index; k>i; k--){
						temp=arr[k-1];
						arr[k-1]=arr[k];
						arr[k]=temp;
					}
				}
			}	
		}

		// printing desired array
		for(int i=0; i<n; i++){
			if(i!=(n-1)){
				printf("%d -> ",arr[i]);
			}else{
				printf("%d \n",arr[i]);
			}
		}
	}else{
		// if length of array is negative or equal to zero
		printf("Invalid Length of Array \n");
	}
}
