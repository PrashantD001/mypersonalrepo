
//			COUNT PAIR SUM

#include<stdio.h>

void main(){

	// taking variable n to take length of first array 
	int n;

	// taking variable m to take length of second array 
	int m;

	// taking variable sum to use in checking condition
	int sum;

	//  taking variable count to store the number of pairs
	int count=0;

	// taking length of first array and second array from user
	printf("Enter the length of First Array : \n");
	scanf("%d",&n);
	
	printf("Enter the length of Second Array : \n");
	scanf("%d",&m);

	// checking length of array's are greater than zero
	if(n>0 && m>0){

		// crating two int array of length m and n
		int arr1[n] ;
		int arr2[m] ;
	
		// taking input in first array
		printf("Enter the Elements in First Array : \n");
		for(int i=0; i<n; i++){
			scanf("%d",&arr1[i]);
		}	
	
		// taking input in second array
		printf("Enter the Elements in Second Array : \n");
		for(int i=0; i<m; i++){
			scanf("%d",&arr2[i]);
		}	

		// taking a desired sum from user
		printf("Enter the Required Sum : \n");
		scanf("%d",&sum);

		// loop to check all posible pairs
		for(int i=0; i<n; i++){
			for(int j=0; j<m; j++){
				// checking the reuired condition
				if((arr1[i]+arr2[j])==sum){
					count++;
				}
			}
		}
		if(count!=0){
			printf("The Pairs are : %d\n",count);
		}else{
			printf("No Pairs Found");
		}
	}else{
		printf("Invalid Length of Array \n");
	}

}
