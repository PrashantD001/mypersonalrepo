
// 		NUMBER OF OCCURRENCE 	

#include<stdio.h>

void main(){

	// taking variable n input length of array from user
	int n;

	// taking variable count to count occurance of that number
	int count;

	// taking variable to take number from user
	int num=0;
	
	// taking length of array from user
	printf("Enter the length of Array : \n");
	scanf("%d",&n);

	// checking length of array is greater than zero
	if(n>0){

		// creating int array of length n
		int arr[n] ;
	
		// taking inputs in array from user
		printf("Enter the Elements in Array : \n");
		for(int i=0; i<n; i++){
			scanf("%d",&arr[i]);
		}	
	
		// taking number from user
		printf("Enter the Number : \n");
		scanf("%d",&num);

		// loop to count occurrence of given number
		for(int i=0; i<n; i++){
			if(arr[i]==num){
				count++;
			}
		}	
		if(count>0){
			printf("The Output is : %d\n",count);
		}else{
			printf("No matches found \n");
		}
	}else{
		// if length of array is invalid
		printf("Invalid Length of Array \n");
	}

}
