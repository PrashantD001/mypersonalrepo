
//		COUNT OF SMALLER ELEMENTS

#include<stdio.h>

void main(){

	// taking variable n for input length from user
	int n;	

	// taking variable count to store count of numbres less than input number
	int count;

	// taking variable num to take number from user
	int num=0;


	// taking length of array
	printf("Enter the length of Array : \n");
	scanf("%d",&n);
	
	//checking array length is greater than zero
	if(n>0){

		// making int array of length n
		int arr[n] ;
	
		// taking inputs for array
		printf("Enter the Elements in Array : \n");
		for(int i=0; i<n; i++){
				scanf("%d",&arr[i]);
		}	
	
		// taking Number from user
		printf("Enter the Number : \n");
		scanf("%d",&num);

		// loop for counting array numbers less than given number
		for(int i=0; i<n; i++){
			// checking condition
			if(arr[i]<=num){
				count++;
			}
		}	
		if(count>0){
			printf("The Output is : %d\n",count);
		}else{
			printf("There is no Output ");
		}
	}else{
		// if array length is negative or zero
		printf("Invalid Length of Array \n");
	}

}
