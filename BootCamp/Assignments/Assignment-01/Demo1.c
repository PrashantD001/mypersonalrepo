
//		1-PERFECT ARRAY

#include<stdio.h>

void main(){

	// taking variable n for input length from user
	int n;	

	// taking variable m for using to compare array form ending side as using index m
	int m;

	// taking variable count to check if there is No Perfect array
	int count=0;

	// taking length from user
	printf("Enter the length of Array : \n");
	scanf("%d",&n);

	// cheking length of array is positive and greater than 0
	if(n>0){
		m=n-1;

		//making int array of length n
		int arr[n] ;
	
		printf("Enter the Elements in Array : \n");
		// taking inputs in array using for
		for(int i=0; i<n; i++){
		
			scanf("%d",&arr[i]);
		}
		//checking array length is not equal to one 
		if(n!=1){
			// using for to check array from starting and ending
			for(int i=0; i<n/2; i++,m--){
				// cheking condition for Perfect Array
				if(arr[i]!=arr[m]){
					count=1;
					// if it not perfect then breaking for
					break;
				}
			}
			// checking array is Perfect or Not using Count variable
			if(count==0)
				printf("PERFECT \n");
			else
				printf("NOT PERFECT \n");
		}else{
			// if length is One then it is by default Perfect
			printf("PERFECT \n");
		}
	}else{
		printf("Invalid Input \n");
	}
}
