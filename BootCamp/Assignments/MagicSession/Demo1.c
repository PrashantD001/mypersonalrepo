
/*
 * WAP that accepts a String from the User and prints the length
 * of the String.
 * 		Use MyStrLen()
 */

#include<stdio.h>

// User Define Function To return Length of String in int 
int MyStrLen(char *ptr){

	// taking variable to count length of String
	int count = 0;

	// checking character upto \0
	while(*ptr != '\0'){
	
		ptr++;		// increasing pointer
		count++;	// increasing count
	}

	// returning count 
	return count;
}

void main(){

	// Taking variable to take Character Array Approximate Length
	int n;
	printf("Enter the Approx length of Array or 'String': \n");
	scanf("%d",&n);

	// making Character Array of Given length 
	char str[n];


	printf("Enter the String : \n");
	// to skip the \n 
	getchar();
	
	// takingt String Input From User by Using gets();
	gets(str);


	// Calling Our user defined Function and Storing value in another variable len
	int len = MyStrLen(str);

	printf("The length of String is : %d \n",len);


	// Secong Way we can Directlly call Function in Printf()
	printf("The length of String is : %d \n",MyStrLen(str));
	
}
