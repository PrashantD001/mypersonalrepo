

/*
 * WAP that Accepts 2 Strings from User And Concat the 
 * Strings and Print the Concated String use MyStrCat()
 *
 * IP : 
 * 	Enter String 1 : yes i can
 * 	Enter String 2 :  do it
 * 
 * OP : yes i can do it
 *
 */

#include<stdio.h>

char* MyStrCat(char *dest, char* src){

	char *start = dest;	// taking pointer to return starting of Destination String 
	
	// taking dest pointer to the end of First String 
	while(*dest != '\0'){
		dest++;
	}
	
	while(*src != '\0'){
		*dest=*src;		// Storing character by character of Source String in Destination String
		dest++,src++;
	}

	*dest = '\0';		// Compliting String 

	return start;
}

void main(){
	
	// Takiung Approx Length of string from User 
	int l1,l2;
	printf("Enter the Approx length of String1 And String2 : \n");
	scanf("%d%d",&l1,&l2);
	
	// Making character Arrays of Given Length 
	char str1[l1+l2];
	char str2[l2];

	getchar();

	printf("Enter the First String : \n");
	gets(str1);					// input of First String 
	
	printf("Enter the Second String : \n");
	gets(str2);					// Input of Second String

	// Calling Fuction
	MyStrCat(str1,str2);

	printf("The Concated String is : \n");
	puts(str1);
}
