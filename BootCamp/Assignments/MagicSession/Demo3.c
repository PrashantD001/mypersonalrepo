
/*
 * WAP that accepts NUmber from user seperate digits from that number 
 * Enter them in Array then sort the Array in descending Order
 *
 * IP - Enter Number : 942111423
 * OP : 9 | 4 | 3 | 2 | 1|
 */

#include<stdio.h>

void main(){


	// taking Number from User using variable num
	int num;
	printf("Enter the Number : \n");
	scanf("%d",&num);

	// storing Input number in Temp variable for Further Operations
	int temp=num;

	int num2=0;
	int len =0;
	//checking Frequency of digits in Number
	for(int i=9; i>=0 ; i--){
		temp=num;
		int count=0;
		while(temp!=0){	
			if((temp%10)==i){
				count++;
			}
			temp/=10;
		}
		if(count>0){		
			len++;				// taking Length of Array
			num2=(num2*10)+i;		// Taking num2 to store Number without Repeating Digit in Descending Order
		}
	}
	printf("length of Array :%d\n",len);
	printf("num2 :%d\n",num2);
	int arr[len];
	temp=num2;
	//using variable as index of Array
	int ind=len-1;

	while(temp!=0){
		arr[ind]=temp%10;		// Storing Digits in 
		ind--;				// increasing index 
		temp/=10;		
	}

	printf("Printing the Sorted Array : \n");
	//printg Sorted Array
	for(int i=0; i<len; i++){
		printf("%d | ",arr[i]);
	}
	printf("\n");
}
