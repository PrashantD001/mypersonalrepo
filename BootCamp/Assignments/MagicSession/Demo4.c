
/*
 * WAP that accepts an Array on Length N From the User and 
 * Calculates Square of all Even Elements and Cubes of All Odd
 * Elements from that Array and Replaces the Elements Respectively
 * with the Answer
 *
 * IP - Enter the Length of Array : 6
 * 	Enter the Elements in array : 1	2 3 4 5 6
 * OP : 1 | 4 | 27 | 16 | 125 | 36 |
 */

#include<stdio.h>

void main(){


	// taking length from user
	int n;
	printf("Enter the Length of Array : \n");
	scanf("%d",&n);

	//making array of that length
	int arr[n];

	printf("Enter the Elements in Array : \n");
	//taking input From User
	int ans;
	for(int i=0; i<n; i++){
		scanf("%d",&ans);
		if(ans%2==0){			// Checking Given input is odd or even
			arr[i]=ans*ans;		// if Even Then Stroing Square
		}else{
			arr[i]=ans*ans*ans;	// if odd then Storing Cube 
		}
	}

	// Printing the Array 
	printf("Printing the Array : \n");
	for(int i=0; i<n; i++){
		printf("%d | ",arr[i]);
	}
	printf("\n");

}
