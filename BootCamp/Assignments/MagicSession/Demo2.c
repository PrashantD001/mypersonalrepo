
/*
 * WAP that accepts NUmber from user seperate digits from that number 
 * Enter them in Array then sort the Array in Ascending Order
 *
 * IP - Enter Number : 942111423
 * OP : 1 | 1 | 1 | 2 | 2 | 3 | 4 | 4 | 9 |
 */

#include<stdio.h>

void main(){


	// taking Number from User using variable num
	int num;
	printf("Enter the Number : \n");
	scanf("%d",&num);

	// storing Input number in Temp variable for Further Operations
	int temp=num;

	int count=0;
	// counting digits in Input num
	while(temp!=0){
		count++;
		temp/=10;
	}

	//making array of that length
	int arr[count];

	temp=num;

	//using variable as index of Array
	int ind=0;
	
	while(temp!=0){
		arr[ind]=temp%10;		// Storing Digits in 
		ind++;				// increasing index 
		temp/=10;		
	}


	printf("Printing the Array : \n");
	//printg Array
	for(int i=0; i<count; i++){
		printf("%d | ",arr[i]);
	}
	printf("\n");


	//Logic for Sorting
	for(int i=0; i<count; i++){
	
		for(int j=i+1; j<count; j++){
		
			// condition for Ascending Sorting
			if(arr[i]>arr[j]){
			
				// swapping two Elements Using Third Varible
				int temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
	}
	
	printf("Printing the Sorted Array : \n");
	//printg Sorted Array
	for(int i=0; i<count; i++){
		printf("%d | ",arr[i]);
	}
	printf("\n");

}
