/*
Program 1: WAP to take check whether the input is a leap year or not (basic leap year)
Input: 2000
Output: 2000 is a leap year
Input: 1999
Output: 1999 is not a leap year
*/


#include<stdio.h>

void main(){

	int year;
	printf("Enter the Year : \n");
	scanf("%d",&year);

	if(year>0){
		if(year%4==0 && year%100!=0){
		
			printf("%d is Leap year \n",year);

		}else if(year%100==0 && year%400==0){
		
			printf("%d is Leap year \n",year);

		}else{
		
			printf("%d is Not a Leap year \n",year);

		}
	
	}else{
	
		printf("Invalid Year \n");

	}
}
