
/*
 * Program 4: Write a program, in which according to the month number
 * print the no. of days in that month
 * (use switch case)
 * Input : month = 7
 * Output: July has 31 days
 * */

#include<stdio.h>

void main(){

	int num;
	printf("Enter the Month : \n");
	scanf("%d",&num);

	switch(num){
	
		case 1:
			printf("January has 31 Days \n");
			break;
		case 2:
			printf("February has 28 Days \n");
			break;
		case 3:
			printf("March has 31 Days \n");
			break;
		case 4:
			printf("April has 30 Days \n");
			break;
		case 5:
			printf("May has 31 Days \n");
			break;
		case 6:
			printf("June has 30 Days \n");
			break;
		case 7:
			printf("July has 31 Days \n");
			break;
		case 8:
			printf("August has 31 Days \n");
			break;
		case 9:
			printf("September has 30 Days \n");
			break;
		case 10:
			printf("October has 31 Days \n");
			break;
		case 11:
			printf("November has 30 Days \n");
			break;
		case 12:
			printf("December has 31 Days \n");
			break;
		default:
			printf("Wrong Input \n");
	}
}
