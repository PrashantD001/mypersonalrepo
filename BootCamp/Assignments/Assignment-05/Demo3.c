
/*
Program 3: WAP to find min among 3 numbers.
Input : 2 3 4
Output: minimum number is 2
*/


#include<stdio.h>

void main(){

	int a,b,c;
	printf("Enter the First Number : \n");
	scanf("%d",&a);
	printf("Enter the Second Number : \n");
	scanf("%d",&b);
	printf("Enter the Third Number : \n");
	scanf("%d",&c);

	if(a<b && a<c){
		printf("minimum Number is %d \n",a);
	}else if(b<a && b<c){
		printf("minimum Number is %d \n",b);
	}else if(c<a && c<b){
		printf("minimum Number is %d \n",c);
	}else{
		printf("You Entered Wrong Input or Same Number \n");
	}
}
