
/*
 * Program 9: WAP to print the count of divisors of the entered num.
 * Ip: 16
 * Op: Divisors of 16 are
 * 1
 * 2
 * 4
 * 8
 * */

#include<stdio.h>

void main(){

	int num;
	printf("Enter the Number : \n");
	scanf("%d",&num);

	printf("Divisors of %d are \n",num);
	for(int i=1 ; i<num ; i++){
	
		if(num%i==0){
			printf("%d\n",i);
		}
	}
}
