

/*
 * Program 5: Write a program that takes a number from 1 to 5 and prints its
 * spelling,
 * if the number is greater than 5 print entered number is greater than 5
 * (use switch case)
 * Input : var4= 4
 * Output: four
 * Input : 9
 * Op: 9 is greater than 5
 * */

#include<stdio.h>

void main(){

	int num;
	printf("Enter the Number : \n");
	scanf("%d",&num);

	if(num > 0){
		switch(num){
		
			case 1:
				printf("One\n");
				break;
			case 2:
				printf("Two\n");
				break;
			case 3:
				printf("Three\n");
				break;
			case 4:
				printf("Four\n");
				break;
			case 5:
				printf("Five\n");
				break;
			default:
				printf("%d is greater than 5 \n",num);
		}
	
	}else{
	
		printf("Please Enter Positive Number \n");
	}
}
