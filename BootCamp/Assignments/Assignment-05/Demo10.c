
/*
 * Program 10:
 * Write a program, take two characters if these characters are equal then print them as it is
 * but if they are unequal then print their difference.
 * Input: va1=s var2=s
 * Output: va1=s var2=s
 * Input: va1=a var2=p
 * Output: The difference between a and p is 15
 * */

#include<stdio.h>

void main(){

	char ch1,ch2;

	printf("Enter the First Character : \n");
	scanf(" %c",&ch1);
	printf("Enter the Second Character : \n");
	scanf(" %c",&ch2);

	if(ch1 == ch2){
		printf("va1 = %c 	va2 =  %c\n",ch1,ch2);
	}else if(ch1-ch2>0){
		printf("The Difference Between %c and %c is : %d\n",ch1,ch2,ch1-ch2);	
	}else{
		printf("The Difference Between %c and %c is : %d\n",ch1,ch2,ch2-ch1);	
	}

}
