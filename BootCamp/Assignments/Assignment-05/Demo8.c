

/*
 *Program 8:
 Write a Program that takes the angles of a triangle from the user and
 print whether that triangle is valid or not.
 {Note: Addition of angles of the triangle has to be 180 in order to
 consider it as a valid one}
Input: 30 60 70
Output: The triangle with angles 30 60 & 70 is an invalid one
*/

#include<stdio.h>

void main(){

	int a,b,c;
	printf("Enter First Angle : \n");
	scanf("%d",&a);
	printf("Enter Second Angle : \n");
	scanf("%d",&b);
	printf("Enter Third Angle : \n");
	scanf("%d",&c);

	if(a>0 && b>0 && c>0){
		if(a+b+c==180 ){
			printf("The Triangle with angles %d , %d , And %d is an Valid One\n",a,b,c);
		}else{
			printf("The Triangle with angles %d , %d , And %d is an Invalid One\n",a,b,c);
		}
	}else{
		printf("Angles cant be negative or Zero \n");
	}
}
