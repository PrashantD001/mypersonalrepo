
/*
Program 2: WAP to find max among 3 numbers.
Input : 2 3 4
Output: max number is 4
*/


#include<stdio.h>

void main(){

	int a,b,c;
	printf("Enter the First Number : \n");
	scanf("%d",&a);
	printf("Enter the Second Number : \n");
	scanf("%d",&b);
	printf("Enter the Third Number : \n");
	scanf("%d",&c);

	if(a>b && a>c){
		printf("max Number is %d \n",a);
	}else if(b>a && b>c){
		printf("max Number is %d \n",b);
	}else if(c>a && c>b){
		printf("max Number is %d \n",c);
	}else{
		printf("You Entered Wrong Input or Same Number \n");
	}
}
