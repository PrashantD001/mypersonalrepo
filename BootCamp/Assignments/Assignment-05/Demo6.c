 

/*
 * Program 6 : WAP to get 10 numbers from Users and find their sum and average.
 * IP: 1 2 3 4 5 6 7 8 9 10
 * OP :
 * sum is 55
 * Average is 55.50
 * */

#include<stdio.h>

void main(){

	int sum,x;
	float avg;

	for(int i =1 ; i<=10 ; i++){
	
		printf("%d Enter Input : \n",i);
		scanf("%d",&x);

		sum=sum+x;
	}
	printf("Sum is : %d \n",sum);

	avg = (float)sum/10.;

	printf("Average is : %f \n",avg);
}
