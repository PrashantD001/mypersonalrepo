

/*
 * Program 7:
 * WAP to check whether the given input is a Pythagorean triplet or not
 * Note: Pythagorean triplet means if given three numbers a b and c follows this pattern
 * c*c = a*a + b*b
 * Any side can be hypotenuse
 * */

#include<stdio.h>

void main(){

	int a,b,c;
	printf("Enter First Side : \n");
	scanf("%d",&a);
	printf("Enter Second Side : \n");
	scanf("%d",&b);
	printf("Enter Third Side : \n");
	scanf("%d",&c);

	if(a>0 && b>0 && c>0){
		if((a*a==b*b+c*c) || (b*b==a*a+c*c) || (c*c==a*a+b*b)){
			printf("%d , %d , And %d are the Pythagorean Triplet\n",a,b,c);
		}else{
			printf("%d , %d , And %d are Not the Pythagorean Triplet\n",a,b,c);
		}
	}else{
		printf("Sides Cant be Negative \n");
	}
}
