
/*
 * 3. take no of rows from the user
 * 4 a 3 b
 * 4 a 3 b
 * 4 a 3 b
 * 4 a 3 b
 */


#include<stdio.h>

void main(){

	int row;
	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	for(int i=1; i<=row; i++){
		int num=row;
		for(int j=1; j<=row; j++){
		
			if(j%2!=0){
				printf("%d\t",num--);
			}else{
				printf("%c\t",96+(j/2));
			}
		}
		printf("\n");
	}
}
