
/*
 * 6. take no of rows from the user
 * = = = = = =
 * $ $ $ $ $ $
 * @ @ @ @ @ @
 * = = = = = =
 * $ $ $ $ $ $
 * @ @ @ @ @ @
 */


#include<stdio.h>

void main(){

	int row;
	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	char ch = '=';

	for(int i=1; i<=row; i++){
		for(int j=1; j<=row; j++){

			printf("%c\t",ch);
		}	
		if(ch=='=')
			ch='$';
		else if(ch=='$')
			ch='@';
		else
			ch='=';
		printf("\n");
	}
}
