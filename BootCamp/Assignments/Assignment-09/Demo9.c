
/*
 * 9. take no of rows from the user
 * 0 	1 	1 	2
 * 3 	5 	8 	13
 * 21 	34 	55 	89
 * 144 	233 	377	610
 */


#include<stdio.h>

void main(){

	int row;
	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	int a=0,b=1,c;

	for(int i=1; i<=row; i++){
		for(int j=1; j<=row; j++){

				printf("%d\t",a);
				c=a+b;
				a=b;
				b=c;

		}
		printf("\n");
	}
}
