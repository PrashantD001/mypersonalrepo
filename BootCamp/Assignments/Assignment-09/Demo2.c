
/*
 * 2. take no of rows from the user
 * 3 b 1 d
 * a 2 c 0
 * 3 b 1 d
 * a 2 c 0
 */


#include<stdio.h>

void main(){

	int row;
	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	for(int i=1; i<=row; i++){
		for(int j=1; j<=row; j++){
		
			if((i+j)%2==0){
				printf("%d\t",row-j);
			}else{
				printf("%c\t",96+j);
			}
		}
		printf("\n");
	}
}
