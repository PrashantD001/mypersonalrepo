
/*
 * 10. take no of rows from the user
 * D1 C2 B3 A4
 * e5 f4 g3 h2
 * F3 E4 D5 C6
 * g7 h6 i5 j4
 */


#include<stdio.h>

void main(){

	int row,num=0;
	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	char ch ;

	for(int i=1; i<=row; i++){

		ch =64+row+i-1;

		for(int j=1; j<=row; j++){
		
			if(i%2!=0){
				printf("%c%d\t",ch+1-j,++num);
			}else{
				printf("%c%d\t",ch+32+(j-1),num);
				num--;
			}
		}
		num++;
		printf("\n");
	}
}
