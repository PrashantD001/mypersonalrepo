
/*
 * 4. take no of rows from the user
 * a B c D
 * b C d E
 * c D e F
 * d E f G
 */


#include<stdio.h>

void main(){

	int row;
	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	char ch1;

	for(int i=1; i<=row; i++){
		ch1=96+i;
		for(int j=1; j<=row; j++){
		
			if(j%2!=0){
				printf("%c\t",ch1);
			}else{
				printf("%c\t",ch1-32);
			}
			ch1++;
		}
		printf("\n");
	}
}
