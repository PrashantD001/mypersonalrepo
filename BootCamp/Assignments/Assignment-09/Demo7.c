
/*
 * 7. take no of rows from the user
 * 1 4 27
 * 4 27 16
 * 27 16 125
 */


#include<stdio.h>

void main(){

	int row;
	printf("Enter the Number of Rows : \n");
	scanf("%d",&row);

	int num;

	for(int i=1; i<=row; i++){
		int num=i;
		for(int j=1; j<=row; j++,num++){
		
			if((i+j)%2==0){
				printf("%d\t",num*num*num);
			}else{
				printf("%d\t",num*num);
			}
		}
		printf("\n");
	}
}
