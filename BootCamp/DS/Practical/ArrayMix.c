

#include<stdio.h>


// 1 - Count of Elements
int countEle(int arr[],int size){
	int max =arr[0];
	for(int i=0; i<size; i++){
		if(max<arr[i]){	
			max = arr[i];
		}
	}
	int count =0;
	for(int i=0 ; i<size; i++){
		if(arr[i] <max){	
			count++;
		}
	}
	return count;
}

// Good Pair
int goodPair(int arr[],int size, int key){

	for(int i=0;i<size;i++){
		for(int j=i+1; j<size; j++){
			if(arr[i]+arr[j] == key){		
			return 1;}
		}
	}
	return 0;
} 

// Reverse Array In Range
void revArrayInRange(int arr[] , int size , int start , int end){

	while(start<end){	
		int temp = arr[start];
		arr[start] = arr[end];
		arr[end] = temp;
		start++;
		end--;
	}
}

int printArr(int arr[] , int size){

	for(int i =0; i<size; i++){
		printf("%d ",arr[i]);
	}
	printf("\n");
	return 0;
}

int print2Arr(int arr[][3]){

	for(int i =0; i<3; i++){
		for(int j =0; j<3; j++){
			printf("%d ",arr[i][j]);
		}
		printf("\n");
	}
	return 0;
}

// Reverse Array
void revArray(int arr[] , int size ){

	int start = 0;
	int end = size-1;

	while(start<end){	
		int temp = arr[start];
		arr[start] = arr[end];
		arr[end] = temp;
		start++;
		end--;
	}
}

// Array Rotation 
int rotateArr(int arr[] , int size, int key){

	while(key>0){
		int temp = arr[size-1];
		for(int i=size-1; i>=0; i--){
			arr[i] = arr[i-1];	
		}
		arr[0] =  temp;
		key--;
	}
	return 0;
}

// Coloumn Sum
void coloumnSum(int arr[][4], int row , int col, int ans[]){

	for(int i = 0; i<row; i++){
		for(int j= 0; j<col ; j++){
			ans[j] += arr[i][j];
		}
	}
}

// Row Sum
void rowSum(int arr[][4], int row , int col, int ans[]){

	for(int i = 0; i<row; i++){
		for(int j= 0; j<col ; j++){
			ans[i] += arr[i][j];
		}
	}
}

// Mian Diagonal
int mainDiago(int arr[][3], int n){

	//print2Arr(arr);
	int sum =0;
	for(int i = 0; i<n; i++){
		sum+=arr[i][i];
	//	printf("%d - ",arr[i][i]);
	}
	//printf("\n");
	return sum;
}

// Minor Diagonal
int minorDiago(int arr[][3], int n){

	int sum =0;
	for(int i = 0; i<n; i++){
		sum+=arr[i][n-1-i];
	//	printf("%d - ",arr[i][n-1-i]);
	}
	//printf("\n");
	return sum;
}


void main(){


	int arr1[] = {3,1,2};
	int ans = countEle(arr1,3);
	printf("1 ANS  -> %d \n",ans);
	
	int arr2[] = {1,2,3,4};
	ans = goodPair(arr2,4,7);
	printf("1 ANS  -> %d \n",ans);
	
	int arr3[] = {1,2,3,4};
	revArrayInRange(arr3,4,2,3);
	printf("3 ANS  ->  \n");
	printArr(arr3,4);

	int arr4[] ={1,2,3,4,5};
	revArray(arr4,5);
	printf("4 ANS  ->  \n");
	printArr(arr4,5);

	int arr5[] = {1,2,3,4};
	rotateArr(arr5,4,2);
	printf("5 ANS  ->  \n");
	printArr(arr5,4);
	
	int arr6[] = {2,5,1,4,8,0,8,1,3,8};
	ans = countEle(arr6,10);
	printf("6 ANS  -> %d \n",ans);

	int arr7[][4]={{1,2,3,4},{5,6,7,8},{9,2,3,4}};
	int temp1[4]={0,0,0,0};
	coloumnSum(arr7,3,4,temp1);
	printf("7 ANS  -> \n");
	printArr(temp1,4);
	
	int arr8[][4]={{1,2,3,4},{5,6,7,8},{9,2,3,4}};
	int temp2[3]={0,0,0};
	rowSum(arr8,3,4,temp2);
	printf("8 ANS  -> \n");
	printArr(temp2,3);
	
	int arr9[][3]={1,-2,-3,-4,5,-6,-7,-8,9};
	ans = mainDiago(arr9,3);
	printf("9 ANS  -> %d\n",ans);
	
	int arr10[][3]={1,-2,-3,-4,5,-6,-7,-8,9};
	ans = minorDiago(arr10,3);
	printf("10 ANS  -> %d\n",ans);


}
