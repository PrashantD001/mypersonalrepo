

#include<stdio.h>

// Special Subsequences "AG"
int subSequences(char arr[], int size){

	char *ptr = arr;
	int count =0;
	int countA=0;
	int countG=0;
	int pair = 0;
	while(*ptr != '\0'){
		if(*ptr == 'A'){
			countA++;
		}else if(*ptr == 'G'){
			countG++;
			pair = pair + countA;	
		}
		ptr++;
	}
	return pair;
}

//Leaders Element in Array
void leadersEle(int arr[] , int size){

	int max = arr[size-1];
	printf("%d ",max);
	for(int i=size-2; i>=0; i--){
	
		if(arr[i]>max){
			max= arr[i];
			printf("%d ",max);
		}
	}
	printf("\n");
}

// Sum of All sub Array
int sumAllSubArray(int arr[] , int size){

	int sum =0;
	for(int i=0 ; i<size; i++){
		for(int j=i ; j<size ; j++){
			for(int k=i; k<=j; k++){
				sum+=arr[k];
			}
		}
	}
	return sum;
}

int goodSubArray(int arr[] , int size , int key){

	int sum = 0, ans = 0;
	for(int i=0 ; i<size; i++){
		for(int j=i ; j<size ; j++){
			sum =0;
			for(int k=i; k<=j; k++){
				sum+=arr[k];
			}
			if((j-i)%2==1 ){
			       if(sum < key){
					ans++;
			       }
			}else{
			       if(sum > key){
					ans++;
			       }
			}
		}
	}
	return ans;
}

void main(){

	char arr2[20] = "ABCGAG";
	int ans = subSequences(arr2,20);
	printf("Ans 22  -> %d\n",ans);
	printf("\n");
	
	int arr3[] = {16,17,4,3,5,2};
	//leadersEle(arr3,6);
	printf("Ans 23  -> \n");
	leadersEle(arr3,6);
	printf("\n");
	
	
	int arr4[] = {1,2,3};
	ans = sumAllSubArray(arr4,3);
	printf("Ans 24  -> %d\n",ans);
	printf("\n");
	
	int arr5[] = {1,2,3,4,5};
	ans = goodSubArray(arr5,5,4);
	printf("Ans 25  -> %d\n",ans);
	printf("\n");
}
