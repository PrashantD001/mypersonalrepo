

#include<stdio.h>

// Anti Diagonal
void antiDiago(int arr[][3], int row , int col, int ans[5][3]){

	int m=0,n=0;
	for(int i = 0; i<row; i++){
		m=i;
		n=i;
		for(int j= 0; j<col ; j++){
			if((i+j)>=row){
				n--;
			}
			ans[m][n]=arr[i][j];
			m++;
		}
	}
}

void printArr1(int arr[],int row){

	for(int i=0; i<row ;i++){
		printf("%d ",arr[i]);
	}printf("\n");
}

	
void printArr(int arr[][3],int row, int col){

	for(int i=0; i<row ;i++){
		for(int j=0;j<col;j++){
			printf("%d ",arr[i][j]);
		}
		printf("\n");
	}
}

void printArr4(int arr[][4],int row, int col){

	for(int i=0; i<row ;i++){
		for(int j=0;j<col;j++){
			printf("%d ",arr[i][j]);
		}
		printf("\n");
	}
}


// Matrix Transpose
void matrixTrans(int arr[][3], int n){

	for(int i = 0; i<n; i++){
		for(int j= i; j<n ; j++){
			int temp = arr[i][j];
			arr[i][j] = arr[j][i];
			arr[j][i] = temp;
		}
	}
}

// Rotat Array
void rotateArr(int arr[][3], int size){

	int m=size-1,n=0,i=0,j=size-1,temp=0;

	for(int k = 0 ; k<size-1 ; k++){
	
		temp=arr[n][i];
		arr[n][i] = arr[j][n];
		arr[j][n] = arr[m][j];
		arr[m][j] = arr[i][m];
		arr[i][m] = temp;

		i++;
		j--;
	}
}

// Are Two Matrices are same 
int isSameArr(int arr1[][3],int arr2[][3], int n){

	for(int i = 0; i<n; i++){
		for(int j= 0; j<n ; j++){
			if(arr1[i][j] != arr2[i][j]){
				return 0;
			}
		}
	}
	return 1;
}

// Add the Matrices 
void addArr(int arr1[][3],int arr2[][3],int ans[][3], int n){

	for(int i = 0; i<n; i++){
		for(int j= 0; j<n ; j++){
			ans[i][j] = arr1[i][j] + arr2[i][j];
		}
	}
}

// Row to Coloumn Zero

int isPresent(int arr[],int size,int key){

	for(int i=0;i<size;i++){
		if(arr[i]==key){
			return 1;
		}
	}
	return 0;
}

void rowToColZero(int arr[][4],int m,int n){
/*
	for(int i=0; i<m ;i++){
		for(int j=0;j<n;j++){
			printf("%d ",arr[i][j]);
		}
		printf("\n");
	}*/
	int arrRow[m],arrCol[n];
	for(int i=0 ; i<m; i++){
		arrRow[i]=-1;
	}
	for(int i=0 ; i<n; i++){
		arrCol[i]=-1;
	}
	int p=0,q=0;
	for(int i = 0; i<m; i++){
		for(int j= 0; j<n ; j++){
			if(arr[i][j]==0){
				printf("Zero Found \n");
				arrRow[p]=i;
				arrCol[p++]=j;
			}	
		}
	}

	for(int i = 0; i<m; i++){
		for(int j= 0; j<n ; j++){
			if(isPresent(arrRow,m,i) || isPresent(arrCol,n,j)){
				arr[i][j]=0;
			}	
		}
	}
}

// In Place Prefix Sum
void prefixSum(int arr[] , int size){

	for(int i = 1; i<size; i++){
		arr[i]+=arr[i-1];
	}
}

// Equillibrium index of the array
int equillibriumArray(int arr[] , int size){

	int prefix[size];
	prefix[0]=arr[0];
	for(int i = 1; i<size; i++){
		prefix[i]=prefix[i-1]+arr[i];
	}

	for(int i=1;i<size-1;i++){
	
		if(prefix[i-1] == prefix[size-1]-prefix[i]){
			return i;	
		}
	}
	return -1;
}

// Product Array Puzzle
void productArray(int arr[] , int size){

	double mul = 1;
	for(int i=0; i<size; i++){
		mul*=arr[i];
	}

	for(int i=0 ;i<size; i++){
	
		arr[i]= mul/arr[i];
	}
}

// Even Number in a Range
void evenInRange(int arr[], int size, int que[][2], int querry, int ans[]){
	int prefix[size];

	int count =0;
	if(arr[0]%2==0){
		prefix[0]=1;
	}else{
		prefix[0]=0;
	}
	for(int i = 1; i<size; i++){
		if(arr[i]%2==0){
			prefix[i]=prefix[i-1]+1;
		}else{
			prefix[i]=prefix[i-1];
		}
	}
/*
	for(int i=0 ; i<size; i++){	
		printf("%d -> ",prefix[i]);
	}
	printf("\n");*/
	for(int i=0; i<querry;i++){
		if(que[i][0] != 0){
			ans[i] = (prefix[que[i][1]]-prefix[que[i][0]-1]);
			//printf("| %d - ",prefix[que[i][1]]);
			//printf("%d |",prefix[que[i][0]]);
			//printf("\n");
		}else{
			ans[i] = prefix[que[i][1]];		
		}
	}
}

// Range Sum Query
void sumInRange(int arr[], int size, int que[][2], int querry, int ans[]){
	int prefix[size];
	prefix[0]=arr[0];
	
	for(int i = 1; i<size; i++){
		prefix[i]=prefix[i-1]+arr[i];
	}
/*
	for(int i=0 ; i<size; i++){	
		printf("%d -> ",prefix[i]);
	}
	printf("\n");*/
	for(int i=0; i<querry;i++){
		if(que[i][0] != 0){
			ans[i] = (prefix[que[i][1]]-prefix[que[i][0]-1]);
			//printf("| %d - ",prefix[que[i][1]]);
			//printf("%d |",prefix[que[i][0]]);
			//printf("\n");
		}else{
			ans[i] = prefix[que[i][1]];		
		}
	}
}
void main(){

	int arr1[][3]={1,2,3,4,5,6,7,8,9};
	int temp[5][3]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	antiDiago(arr1,3,3,temp);
	printf("11 ANS  -> \n");
	printArr(temp,5,3);
	printf("\n");
	
	int arr2[][3]={1,2,3,4,5,6,7,8,9};
	matrixTrans(arr2,3);
	printf("12 ANS  -> \n");
	printArr(arr2,3,3);
	printf("\n");
	
	int arr3[][3]={1,2,3,4,5,6,7,8,9};
	rotateArr(arr3,3);
	printf("13 ANS  -> \n");
	printArr(arr3,3,3);
	printf("\n");
	
	int arr41[][3]={1,2,3,4,5,6,7,8,9};
	int arr42[][3]={1,2,3,4,5,6,7,8,9};
	int ans = isSameArr(arr41,arr42,3);
	printf("14 ANS  -> %d\n",ans);
	printf("\n");
	
	int arr51[][3]={1,2,3,4,5,6,7,8,9};
	int arr52[][3]={9,8,7,6,5,4,3,2,1};
	int arr5ans[][3]={0,0,0,0,0,0,0,0,0};
	addArr(arr51,arr52,arr5ans,3);
	printf("15 ANS  -> \n");
	printArr(arr5ans,3,3);
	printf("\n");
	
	int arr6[3][4]={1,2,3,4,5,6,7,0,9,2,0,4};
	rowToColZero(arr6,3,4);
	printf("16 ANS  -> \n");
	printArr4(arr6,3,4);
	printf("\n");

	int arr7[]={1,2,3,4,5};
	prefixSum(arr7,5);
	printf("17 ANS  -> \n");
	printArr1(arr7,5);
	printf("\n");

	int arr8[]={-7,1,5,2,-4,3,0};
	ans = equillibriumArray(arr8,7);
	//int arr8[]={1,2,3,2,1};
	//ans = equillibriumArray(arr8,5);
	printf("18 ANS  -> %d\n",ans);
	printf("\n");
	
	int arr9[]={1,2,3,4,5};
	productArray(arr9,5);
	printf("19 ANS  -> \n");
	printArr1(arr9,5);
	printf("\n");

	int arr10[]={2,1,8,3,9,6};
	int que10[][2]={{0,3},{3,5},{1,3},{2,5}};
	int ans10[]={0,0,0,0};
	evenInRange(arr10,6,que10,4,ans10);
	printf("20 ANS  -> \n");
	printArr1(ans10,4);
	printf("\n");
	
	int arr11[]={1,2,3,4,5};
	int que11[][2]={{0,3},{1,2}};
	int ans11[]={0,0};
	sumInRange(arr11,5,que11,2,ans11);
	printf("21 ANS  -> \n");
	printArr1(ans11,2);
	printf("\n");

}
