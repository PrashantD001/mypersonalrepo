#include<stdio.h>
#include<stdlib.h>

struct TreeNode{
    int data ; 
    struct TreeNode *left ;
    struct TreeNode *right ;
    
};

struct TreeNode* createNode(int level){

	level = level + 1 ;

	struct TreeNode *newNode  = (struct TreeNode *)malloc(sizeof(struct TreeNode));
	printf("Enter The Data : \n");
	scanf("%d",&(newNode->data));
	char ch ;
	getchar();
	printf("Want to create left Subtree for Level : %d \n",level);
	scanf("%c",&ch);
	if(ch == 'y' || ch == 'Y' ){
	
		newNode->left = createNode(0);
	}else{
	
		newNode->left = NULL;
	}
	
	getchar();
	printf("Want to create right Subtree for Level : %d \n",level);
	scanf("%c",&ch);
	if(ch == 'y' || ch == 'Y' ){
	
		newNode->right = createNode(0);
	}else{
	
		newNode->right = NULL;
	}
	return newNode;
}

void preOrder(struct TreeNode *root){

	if(root == NULL){
		return ;
	}
	printf("%d\n",root->data);
	preOrder(root->left);
	preOrder(root->right);
}

void inOrder(struct TreeNode *root){

	if(root == NULL){
		return ;
	}
	
	inOrder(root->left);
	printf("%d\n",root->data);
	inOrder(root->right);
}

void postOrder(struct TreeNode *root){

	if(root == NULL){
		return ;
	}
	
	postOrder(root->left);
	postOrder(root->right);
	printf("%d\n",root->data);
}

void printTree(struct TreeNode *root){

	char ch ; 
	do{
	
		printf("1. PreOrder\n");
		printf("2. InOrder\n");
		printf("1. PostOrder\n");
		int choice ; 
		printf("Enter the Choice : \n");
		scanf("%d",&choice);

		switch(choice){
		
			case 1 : 
				preOrder(root);
				break;
			case 2 : 
				inOrder(root);
				break;
			case 3 : 
				postOrder(root);
				break;
			default : 
				printf("Please Enter the Valid Choice ...! \n");
				break;
		}

		getchar();
		printf("Do You Want to Continue ....? \n");
		scanf("%c",&ch);
	}while(ch == 'y' || ch == 'Y');
}

void main(){

	printf("Creating Binary Tree ....!\n");
	struct TreeNode *root  = (struct TreeNode *)malloc(sizeof(struct TreeNode));
	printf("Enter The Data : \n");
	scanf("%d",&(root->data));

	printf("\t\tTree rooted with %d\n",root->data);
	char ch ;
	getchar();
	printf("Want to create left Subtree for Root Node ? \n");
	scanf("%c",&ch);
	if(ch == 'y' || ch == 'Y' ){
	
		root->left = createNode(0);
	}else{
	
		root->left = NULL;
	}
	
	getchar();
	printf("Want to create right Subtree for Root Node ? \n");
	scanf("%c",&ch);
	if(ch == 'y' || ch == 'Y' ){
	
		root->right = createNode(0);
	}else{
	
		root->right = NULL;
	}

	printTree(root);
}
