#include<stdio.h>
#include<stdlib.h>

struct TreeNode{
    int data ; 
    struct TreeNode *left ;
    struct TreeNode *right ;
    
};

void preOrder(struct TreeNode *root){

	if(root == NULL){
		return ;
	}
	printf("%d\n",root->data);
	preOrder(root->left);
	preOrder(root->right);
}

void inOrder(struct TreeNode *root){

	if(root == NULL){
		return ;
	}
	
	inOrder(root->left);
	printf("%d\n",root->data);
	inOrder(root->right);
}

void postOrder(struct TreeNode *root){

	if(root == NULL){
		return ;
	}
	
	postOrder(root->left);
	postOrder(root->right);
	printf("%d\n",root->data);
}

void printTree(struct TreeNode *root){

	char ch ; 
	do{
		printf("1. PreOrder\n");
		printf("2. InOrder\n");
		printf("1. PostOrder\n");
		int choice ; 
		printf("Enter the Choice : \n");
		scanf("%d",&choice);
		printf("\n");

		switch(choice){
		
			case 1 : 
				preOrder(root);
				break;
			case 2 : 
				inOrder(root);
				break;
			case 3 : 
				postOrder(root);
				break;
			default : 
				printf("Please Enter the Valid Choice ...! \n");
				break;
		}

		getchar();
		printf("Do You Want to Continue ....? \n");
		scanf("%c",&ch);
	}while(ch == 'y' || ch == 'Y');
}

struct TreeNode* createTree(int in[] , int inSt , int inEnd , int pre[] , int preSt , int preEnd ){

	struct TreeNode *newNode  = (struct TreeNode *)malloc(sizeof(struct TreeNode));
	newNode->data = pre[preSt];

	int temp = pre[preSt];
	int index = -1 ;
	for(int i  = inSt ; i<=inEnd ; i++){
		if(in[i] == temp){
			index = i;
			break;
		}
	}
	if(index > inSt){
		newNode->left = createTree(in , inSt , index-1 , pre , preSt+1 , index );
	}else{
		newNode->left = NULL;
	}
	if(index < inEnd){
		newNode->right = createTree(in , index+1 , inEnd , pre , preSt+(index-inSt)+1 , preEnd );
	}else{
		newNode->right = NULL;
	}
	return newNode;

}
void main(){

	int in[] = {2,5,4,1,6,3,8,7};
	int pre[] = {1,2,4,5,3,6,7,8};
	struct TreeNode *root =  createTree(in,0,7,pre,0,7);

	printTree(root);
}
