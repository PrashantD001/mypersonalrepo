#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct TreeNode{
    int data ; 
    struct TreeNode *left ;
    struct TreeNode *right ;
    
}tree;

typedef struct StackFrame{

	tree *data ;
	struct StackFrame *next;
}sFrame;

struct TreeNode* createNode(int level){

	level = level + 1 ;

	struct TreeNode *newNode  = (struct TreeNode *)malloc(sizeof(struct TreeNode));
	printf("Enter The Data : \n");
	scanf("%d",&(newNode->data));
	char ch ;
	getchar();
	printf("Want to create left Subtree for Level : %d \n",level);
	scanf("%c",&ch);
	if(ch == 'y' || ch == 'Y' ){
	
		newNode->left = createNode(0);
	}else{
	
		newNode->left = NULL;
	}
	
	getchar();
	printf("Want to create right Subtree for Level : %d \n",level);
	scanf("%c",&ch);
	if(ch == 'y' || ch == 'Y' ){
	
		newNode->right = createNode(0);
	}else{
	
		newNode->right = NULL;
	}
	return newNode;
}

sFrame *top = NULL;

void push(tree *Node){

	sFrame *newFrame = (sFrame *)malloc(sizeof(sFrame));

	newFrame->next = top;
	newFrame->data = Node ;

	top = newFrame;
}

bool isEmpty(){

	if(top == NULL){
	
		return true;
	}
	return false;
}

tree* pop(){

	sFrame *temp = top ; 

	tree *item = top->data;

	top = top->next ;
	free(temp);

	return item ;
}

void inOrderIterative(struct TreeNode *root){

	tree *temp = root;

	while(!isEmpty() || temp!=NULL){
	
		if(temp != NULL){
		
			push(temp);
			temp=temp->left;
		}else{
		
			temp = pop();
			printf("%d\n",temp->data);
			temp = temp->right;
		}
	}
}

void printTree(struct TreeNode *root){

	char ch ; 
	do{
	
		printf("1. InOrder - Itrative \n");

		int choice ; 
		printf("Enter the Choice : \n");
		scanf("%d",&choice);

		switch(choice){
			case 1 : 
				inOrderIterative(root);
				break;
			default : 
				printf("Please Enter the Valid Choice ...! \n");
				break;
		}

		getchar();
		printf("Do You Want to Continue ....? \n");
		scanf("%c",&ch);
	}while(ch == 'y' || ch == 'Y');
}

void main(){

	printf("Creating Binary Tree ....!\n");
	struct TreeNode *root  = (struct TreeNode *)malloc(sizeof(struct TreeNode));
	printf("Enter The Data : \n");
	scanf("%d",&(root->data));

	printf("\t\tTree rooted with %d\n",root->data);
	char ch ;
	getchar();
	printf("Want to create left Subtree for Root Node ? \n");
	scanf("%c",&ch);
	if(ch == 'y' || ch == 'Y' ){
	
		root->left = createNode(0);
	}else{
	
		root->left = NULL;
	}
	
	getchar();
	printf("Want to create right Subtree for Root Node ? \n");
	scanf("%c",&ch);
	if(ch == 'y' || ch == 'Y' ){
	
		root->right = createNode(0);
	}else{
	
		root->right = NULL;
	}

	printTree(root);
}
