
// Construct a Graph

#include<stdio.h>
#include<stdlib.h>

struct node{

	char ch ;
	int noOfNodes;
	struct node* arr[];
};

void main(){

	int nodes ;
	printf("How Many Nodes : \n");
	scanf("%d",&nodes);
	
	struct node* arr[nodes];
	for(int i = 0 ; i<nodes ; i++){
		struct node* nn = (struct node*)malloc(sizeof(struct node));
		nn->ch = 65+i;
		arr[i] = nn;
	}

	for(int i = 0 ; i<nodes ; i++){
		printf("How many Nodes Are Connected To node %c \n",(arr[i])->ch);
		scanf("%d",&((arr[i])->noOfNodes));
		struct node* nn = arr[i];
		printf("Enter the Connected Nodes for Node  %c \n",nn->ch);
		for(int j = 0 ; j<(arr[i]->noOfNodes) ; j++){
		
			char ch2 ;
			scanf(" %c",&ch2);
			(nn->arr)[j] = arr[ch2-65];

		}
	}


	for(int i = 0 ; i<nodes ; i++){
		
		struct node* nn = arr[i];
		
		for(int j = 0 ; j< nn->noOfNodes; j++){
		
			printf("%c -> %c \n",nn->ch,((nn->arr)[j])->ch);
		}
	}

}
