

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct Movie {

	char mName[20];
	int ticCount;
	float Rating;
}obj1={"Drishyam2",5,3.8};

void main(){

	typedef struct Movie mv;

	mv obj2 ={"Kantara",10,9.9};

	mv *ptr1 = &obj1;
	mv *ptr2 = &obj2;

	puts(ptr1->mName);
	printf("%d\n",ptr1->ticCount);
	printf("%f\n\n",ptr1->Rating);
	
	puts(ptr2->mName);
	printf("%d\n",ptr2->ticCount);
	printf("%f\n",ptr2->Rating);
}
