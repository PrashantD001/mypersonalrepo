

/*      Implementing Stack Using SinglyLinkedList      */


#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node * next;
}node;

node *head = NULL;


// variable to take size of stack
int size = 0;


// using flag to handle condition to -1 value from user
int flag = 1;


// function to creat Node using malloc
node * createNode(){

	node * newNode = (node *)malloc(sizeof(node));

	printf("Enter Data : \n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	// returning the address of created node
	return newNode;
}


// function to add node at last position
int addNode(){

	// calling function creatNode and storint it in strucure pointer newNode
	node *newNode = createNode();

	// checking if no Node is present
	if(head == NULL){
	
		head = newNode;
		return -1;
	}else{
	
		// taking temp pointer to travel 
		node *temp = head;
		while(temp->next != NULL){
		
			temp = temp->next;
		}
		temp->next = newNode;
	}
	return 0;
}


// function to count Node present in stack 
int nodeCount(){

	int nCount = 0;
	
	// taking temp pointer to travel 
	node *temp = head;
	
	while(temp != NULL){
		
		temp = temp->next;
		nCount++;
	}

	return nCount;
}

// function to push data in our stack
int push(){

	// taking count of present nodes in stack
	int count = nodeCount();

	// checking if stack is full or not
	if(count == size){
	
		return -1;
	}else{
	
		addNode();

		return 0;
	}
}


// function 
int deleteLast(){

	// checking stack is empty or not
	if(head->next == NULL){
	
		int data = head->data;
		free(head);
		head = NULL;
		return data;
	}else{
	
		node *temp = head;

		while(temp->next->next != NULL){
		
			temp = temp->next;
		}
		int data = temp->next->data;
		free(temp->next);
		temp->next = NULL;
		return data;
	}
}

// function to pop data in our stack
int pop(){

	// Checking stack in empty or not
	if(head == NULL){
	
		flag = 0;
		return -1;
	}else{
	
		// deleting last node and storing data
		int data = deleteLast();

		flag =1;

		// returning data
		return data;
	}
}

// function to peek data in our stack
int peek(){

	// Checking stack in empty or not
	if(head == NULL){
	
		flag = 0;
		return -1;
	}else{
	
		node *temp = head;
		while(temp->next != NULL){
		
			temp = temp->next;
		}

		int data = temp->data;

		flag =1;
		
		// returning data
		return data;
	}
}

// function to print stack 
int printStack(){

	// Checking stack in empty or not
	if(head == NULL){
	
		return -1;
	}else{
	
		node *temp = head;

		while(temp->next != NULL){
		
			printf("| %d |->\t",temp->data);
			
			temp = temp->next;

		}
		printf("| %d |\n",temp->data);

		return 0;
	}
}

void main(){

	// taking Stack size from user
	printf("Enter Stack Size : \n");
	scanf("%d",&size);

	char choice;

	// do-while for user choice
	do{
	
		printf("1. push \n");
		printf("2. pop \n");
		printf("3. peek \n");
		printf("4. print \n");

		int ch ;
		printf("Enter the Choice : \n");
		scanf("%d",&ch);

		switch(ch){
		
			case 1 :
				if(push()==-1){
                                        printf("Stack OverFlow \n");
                                }
				break;
			case 2 :
				{
					int data = pop();
					
					if(flag == 1){
					
						printf("%d is Poped \n",data);
					}else{
					 
						printf("Stack is UnderFlow \n");
					}
				}
				break;
			case 3 :
				{
					int data = peek();
					
					if(flag == 1){
					
						printf("%d is Peek \n",data);
					}else{
					 	
						printf("Stack is UnderFlow \n");
					}
				}
				break;
			case 4 :
				{
				
					if(printStack()==-1){
					
						printf("Stacck is Empty \n");
					}
				}
				break;
			default :
				printf("Invalid Choice \n");
				break;
		}
		printf("Do You want to Continue : \n");
		getchar();
		scanf("%c",&choice);
	
	}while(choice =='y' || choice == 'Y');

}
