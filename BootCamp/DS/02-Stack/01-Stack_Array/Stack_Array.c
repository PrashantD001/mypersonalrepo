

/*      Implementing Stack Using Array      */


#include<stdio.h>


// taking variable top to mange the top position of stack 
int top = -1;

// variable to take size of stack 
int size = 0;

// using flag to handle condition to -1 value from user 
int flag = 1;


// function to push data in our stack
int push(int stack[]){

	// checking if stack is full or not
	if(top == size-1){
	
		// returning -1 for Abortive return 
		return -1;
	}else{
	
		// taking data in stack from user 
		printf("Enter the Data : \n");
		scanf("%d",&stack[++top]);


		// Successful return 
		return 0;
	}
}

// function to pop data in our stack
int pop(int stack[]){

	// Checking stack in empty or not
	if(top == -1){
	
		flag = 0;
		return -1;
	}else{
	
		flag = 1;

		// storing data using top
		int data = stack[top];

		// decrementing top 
		top --;

		// returning data which is popped
		return data;
	}
}

// function to peek data in our stack
int peek(int stack[]){

	// Checking stack in empty or not
	if(top == -1){

		flag = 0;
		return -1;
	}else{
		
		flag = 1;
	
		// storing data using top
		int data = stack[top];

		// returning data 
		return data;
	}
}

// function to print data in our stack
int printStack(int stack[]){

	// Checking stack in empty or not
	if(top == -1){
	
		return -1;
	}else{
	
		for (int i=0; i<=top; i++){
		
			printf("|%d|",stack[i]);
		}
		printf("\n");

		return 0;

	}
}


void main(){

	// taking Stack size from user
	printf("Enter Stack Size : \n");
	scanf("%d",&size);

	// creating stack of given size
	int stack[size];

	char choice;
	
	// do-while for user choice
	do{
	
		printf("1. push \n");
		printf("2. pop \n");
		printf("3. peek \n");
		printf("4. print \n");

		int ch ;
		printf("Enter the Choice : \n");
		scanf("%d",&ch);

		switch(ch){
		
			case 1 :
				if(push(stack)==-1){
					printf("Stack OverFlow \n");
				}
				break;
			case 2 :
				{
					int data = pop(stack);
					
					if(flag == 1){
					
						printf("%d is Poped \n",data);
					}else{
					
						printf("Stack is UnderFlow \n");
						flag = 0;
					}
				}
				break;
			case 3 :
				{
					int data = peek(stack);
					
					if(flag == 1){
					
						printf("%d is Peek \n",data);
					}else{

						printf("Stack is UnderFlow \n");
						flag = 0;
					}
				}
				break;
			case 4 :
				if(printStack(stack)==-1){
					printf("Stack is empty \n");
				}
				break;
			default :
				printf("Invalid Choice \n");
				break;
		}
		printf("Do You want to Continue : \n");
		getchar();
		scanf("%c",&choice);
	
	// condition to check if user want to continue or not 
	}while(choice =='y' || choice == 'Y');

}
