

/*      Implementing Stack Using Array      */


#include<stdio.h>

int top = -1;
int size = 0;
int flag = 1;

int push(int stack[]){

	if(top == size-1){
	
		printf("Stack OverFlow \n");
		return -1;
	}else{
	
		printf("Enter the Data : \n");
		scanf("%d",&stack[++top]);

		return 0;
	}
}

int pop(int stack[]){

	if(top == -1){
	
		printf("Stack UnderFlow \n");
		flag = 0;
		return -1;
	}else{
	
		int data = stack[top];

		if(data == -1){
			flag =1;
		}

		top --;

		return data;
	}
}

int peek(int stack[]){

	if(top == -1){
	
		printf("Stack UnderFlow \n");
		flag = 0;
		return -1;
	}else{
	
		int data = stack[top];
		
		if(data == -1){
			flag =1;
		}

		printf("In Peek Else \n");
		return data;
	}
}

void main(){

	printf("Enter Stack Size : \n");
	scanf("%d",&size);

	int stack[size];

	char choice;
	
	do{
	
		printf("1. push \n");
		printf("2. pop \n");
		printf("3. peek \n");

		int ch ;
		printf("Enter the Choice : \n");
		scanf("%d",&ch);

		switch(ch){
		
			case 1 :
				push(stack);
				break;
			case 2 :
				{
					int data = pop(stack);
					
					if(flag == 1){
					
						printf("%d is Poped \n",data);
					}else{
					
						flag = 0;
					}
				}
				break;
			case 3 :
				{
					int data = peek(stack);
					
					if(flag == 1){
					
						printf("%d is Peek \n",data);
					}else{
					
						flag = 0;
					}
				}
				break;
			default :
				printf("Invalid Choice \n");
				break;
		}
		printf("Do You want to Continue : \n");
		getchar();
		scanf("%c",&choice);
	
	}while(choice =='y' || choice == 'Y');

}
