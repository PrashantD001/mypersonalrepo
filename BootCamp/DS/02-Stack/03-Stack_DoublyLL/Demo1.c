

/*      Implementing Stack Using DoublyLinkedList      */


#include<stdio.h>
#include<stdlib.h>

typedef struct Stack{

	struct Stack * prev;
	int data;
	struct Stack * next;
}stack;

stack *head = NULL;

int size = 0;
int count = 0;
int flag = 1;



stack * createNode(){

	stack * newNode = (stack *)malloc(sizeof(stack));

	newNode->prev = NULL;
	printf("Enter Data : \n");
	scanf("%d",&newNode->data);
	newNode->next = NULL;

	return newNode;
}

int addNode(){

	stack *newNode = createNode();

	if(head == NULL){
	
		head = newNode;
		return -1;
	}else{
	
		stack *temp = head;
		while(temp->next != NULL){
		
			temp = temp->next;
		}
		temp->next = newNode;
		newNode->prev = temp;
		count++;
	}
	return 0;
}

int nodeCount(){

	if(head == NULL){
	
		return 0;
	}else{
	
		int nCount = 0;
		stack *temp = head;
		while(temp != NULL){
		
			temp = temp->next;
			nCount++;
		}

		return nCount;
	}
}

int push(){

	count = nodeCount();

	if(count == size){
	
		printf("Stack OverFlow \n");
		return -1;
	}else{
	
		addNode();

		return 0;
	}
}



int deleteLast(){

	if(head->next == NULL){
	
		int data = head->data;
		free(head);
		head = NULL;
		return data;
	}else{
	
		stack *temp = head;

		while(temp->next->next != NULL){
		
			temp = temp->next;
		}
		int data = temp->next->data;
		free(temp->next);
		temp->next = NULL;
		return data;
	}
}


int pop(){

	if(head == NULL){
	
		printf("Stack UnderFlow \n");
		flag = 0;
		return -1;
	}else{
	
		int data = deleteLast();

		if(data == -1){
			flag =1;
		}

		return data;
	}
}

int peek(){

	if(head == NULL){
	
		printf("Stack UnderFlow \n");
		flag = 0;
		return -1;
	}else{
	
		stack *temp = head;
		while(temp->next != NULL){
		
			temp = temp->next;
		}

		int data = temp->data;

		if(data == -1){
			flag =1;
		}
		return data;
	}
}

void main(){

	printf("Enter Stack Size : \n");
	scanf("%d",&size);

	char choice;
	
	do{
	
		printf("1. push \n");
		printf("2. pop \n");
		printf("3. peek \n");

		int ch ;
		printf("Enter the Choice : \n");
		scanf("%d",&ch);

		switch(ch){
		
			case 1 :
				push();
				break;
			case 2 :
				{
					int data = pop();
					
					if(flag == 1){
					
						printf("%d is Poped \n",data);
					}else{
					
						flag = 0;
					}
				}
				break;
			case 3 :
				{
					int data = peek();
					
					if(flag == 1){
					
						printf("%d is Peek \n",data);
					}else{
					
						flag = 0;
					}
				}
				break;
			default :
				printf("Invalid Choice \n");
				break;
		}
		printf("Do You want to Continue : \n");
		getchar();
		scanf("%c",&choice);
	
	}while(choice =='y' || choice == 'Y');

}
