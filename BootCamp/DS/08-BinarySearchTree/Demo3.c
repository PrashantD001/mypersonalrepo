
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>


typedef struct BSTNode{

	int data;
	struct BSTNode *left;
	struct BSTNode *right;
} bst;

bst* constructBST(bst *root , bst *newNode){
	
	bst *temp = root;
	while(root != NULL){
		if(root->data > newNode->data){
			root = root->left;
		}else{
			root = root->right;
		}
	}
	root = newNode;
	//*root = *newNode;
	if(temp != NULL)
		return temp;
	else
		return root;
}

void printBST(bst *root){

	if(root == NULL){
		return ;
	}

	//printf("%d ",root->data);
	printBST(root->left);
	printf("%d ",root->data);
	printBST(root->right);
}

void main(){

	bst *root = NULL;

	int num,val;

	printf("Enter the number of Elements : \n");
	scanf("%d",&num);
	
	printf("Enter the Elements : \n");
	for(int i=0 ; i<num ; i++){
	
		scanf("%d",&val);
		bst *newNode = malloc(sizeof(bst));

		newNode->data = val;
		newNode->left = NULL;
		newNode->right = NULL;
		root = constructBST(root,newNode);
	}

	printBST(root);

}
