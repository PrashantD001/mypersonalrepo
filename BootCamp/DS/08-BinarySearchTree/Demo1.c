
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>


typedef struct BSTNode{

	int data;
	struct BSTNode *left;
	struct BSTNode *right;
} bst;

bst* constructBST(bst *root , int ele){

	if(root == NULL){
	
		bst *newNode = malloc(sizeof(bst));

		newNode->data = ele;
		newNode->left = NULL;
		newNode->right = NULL;

		root = newNode;
		return root;
	}

	if(root->data > ele){
	
		root->left = constructBST(root->left , ele );
	}else {
	
		root->right = constructBST(root->right , ele );
	}
	return root;
}

void printBST(bst *root){

	if(root == NULL){
		return ;
	}

	//printf("%d ",root->data);
	printBST(root->left);
	printf("%d ",root->data);
	printBST(root->right);
}

bool itrSearchInBST(bst *root , int ele){

	bst *temp = root;

	while(temp != NULL){
	
		if(temp->data == ele){
			return true;
		}

		if(temp->data > ele)
			temp = temp->left;
		else
			temp = temp->right;

	}
	return false;
}

bool searchInBST(bst *root , int ele){

	if(root == NULL){
	
		return false;
	}

	if(root->data == ele){
	
		return true;
	}

	if(root->data > ele){
	
		searchInBST(root->left,ele);
	}else{
		searchInBST(root->right,ele);
	}
}

void main(){

	bst *root = NULL;

	int num,val;

	printf("Enter the number of Elements : \n");
	scanf("%d",&num);
	
	printf("Enter the Elements : \n");
	for(int i=0 ; i<num ; i++){
	
		scanf("%d",&val);
		root = constructBST(root,val);
	}

	printBST(root);


	int search;
	printf("Enter the Element to Search  : \n");
	scanf("%d",&search);

	bool find = itrSearchInBST(root,search);

	if(find == true){
		printf("Fount\n");
	}else{
		printf("Not Fount\n");
	}
}
