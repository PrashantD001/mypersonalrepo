

// call by address

#include<stdio.h>
void fun(int *a){

	*a=*a+10;
	printf("%d\n",*a);
}

void main(){

	int a=10;
	int *ptr =&a;
	fun(ptr);
	printf("%d\n",a);
}
