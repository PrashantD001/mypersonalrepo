
// User Input
// Head Global


#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Movie{

	char mName[20];
	float imdb;
	struct Movie *next;
}mv;

mv *head =  NULL;

void addNode(){

	mv *newNode = (mv *)malloc(sizeof(mv));

	printf("Enter the Movie Name : \n");
	fgets(newNode->mName,20,stdin);
	printf("Enter the IMDB rating  : \n");
	scanf("%f",&newNode->imdb);
	newNode->next = NULL;

	getchar();

	if(head == NULL){
		head =newNode;
	}else{
		mv *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

void printll(){

	mv *temp = head;

	while(temp != NULL){
	
		printf("%s ->",temp->mName);
		printf("%f \n",temp->imdb);

		temp=temp->next;
	}
}

void main(){

	addNode();
	printll();
	
	addNode();
	printll();
}
