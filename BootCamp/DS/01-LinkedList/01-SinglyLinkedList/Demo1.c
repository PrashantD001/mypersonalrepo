

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Node{

	int data ;
	struct Node *next;
}node;

void printll(node *ptr){

	while(ptr != NULL){
	
		printf("%d \n",ptr->data);
		ptr = ptr->next;
	}
}


void main(){

	node *head = NULL;

	//  1st Node
	node *newNode = (node *)malloc(sizeof(node));
	newNode->data = 10;
	newNode->next = NULL;
	head = newNode;
	
	//  2nd Node
	newNode = (node *)malloc(sizeof(node));
	newNode->data = 20;
	newNode->next = NULL;
	head->next = newNode;
	
	//  3rd Node
	newNode = (node *)malloc(sizeof(node));
	newNode->data = 30;
	newNode->next = NULL;
	head->next->next = newNode;

	printll(head);
}
