
// User Input
// Head Global


#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Movie{

	char mName[20];
	float imdb;
	struct Movie *next;
}mv;

mv *head =  NULL;

void addNode(){

	mv *newNode = (mv *)malloc(sizeof(mv));

	getchar();
	printf("Enter the Movie Name : \n");
	fgets(newNode->mName,20,stdin);
	printf("Enter the IMDB rating  : \n");
	scanf("%f",&newNode->imdb);
	newNode->next = NULL;

	if(head == NULL){
		head =newNode;
	}else{
		mv *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

void printll(){

	mv *temp = head;

	while(temp != NULL){

		// for removing \n of fgets()
		temp->mName[strlen(temp->mName)-1] = '\0';
		
		printf("| %s ->",temp->mName);
		printf("%f |",temp->imdb);

		temp=temp->next;
	}
	printf("\n");
}

void main(){

	int num;
	printf("Enter the length of Linked List : \n");
	scanf("%d",&num);

	for(int i=1; i<=num; i++){
	
		addNode();
	}

	printf("Printing the Linked List : \n");
	printll();
}
