

// call by address

#include<stdio.h>
void fun(int *a){

	*a=*a+10;
	printf("%d\n",*a);
}

void main(){

	int a=10;
	fun(&a);
	printf("%d\n",a);
}
