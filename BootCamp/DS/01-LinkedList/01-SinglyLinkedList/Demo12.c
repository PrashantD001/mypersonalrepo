
#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{
	int data;
	struct Demo *next;
}demo;

demo * head = NULL;

// 0
demo * createNode(){

	demo * newNode = (demo *)malloc(sizeof(demo));

	printf("Enter the Data  : \n");
	scanf("%d",&(newNode->data));

	newNode->next = NULL;

	return newNode;
}

// 1
void addNode(){

	demo *newNode = createNode();

	if(head==NULL){
	
		head = newNode;
	}else{
	
		demo *temp = head;
		while(temp->next != NULL){
		
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

// 2
void addFirst(){

	demo *newNode = createNode();

	if(head == NULL){
	
		head = newNode;
	}else{
	
		newNode->next = head;
		head = newNode;
	}
}

//3
void addLast(){
	addNode();
}

//5
int nodeCount(){

	demo *temp = head;
	int count = 0;

	while(temp != NULL){
	
		temp = temp->next;
		count++;
	}

	return count;
}

//4
void addAtPos(int pos){

	int count = nodeCount();

	if(pos>count || pos<=0){
	
		printf("Please Enter Valid Position \n");
	}else if(pos == 1){
	
		addFirst();
	}else{
	
		demo *temp = head;
		while(pos-2){
		
			temp= temp->next;
			pos--;
		}
		demo *newNode = createNode();

		newNode->next = temp->next;
		temp->next = newNode;
	}
}

//6
void deleteFirst(){

	demo *temp = head;

	//head = head->next;				// 1 St Way 
	head = temp->next;				// 2 Nd Way
							
	free(temp);
}

//7
void deleteLast(){

	demo *temp = head;

	if(head->next ==NULL){
	
		free(temp);
		printf("DEleted Last Node of Linked List No Nodes Present Now \n");
	}else if(head->next !=NULL){
	
		while(temp->next->next != NULL){
		
			temp = temp->next;
		}
		free(temp->next);
		temp->next = NULL;
	}
}

//8
void printll(){

	demo *temp =head;
	while(temp != NULL){
	
		printf("|%d|->",temp->data);
		temp=temp->next;
	}
	printf("\n");
}


void main(){

	printf("\n\n <----------------START Main----------------->\n\n");
	
	char choice;

	do{
	
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addLast\n");
		printf("4.addAtPos\n");
		printf("5.countNode\n");
		printf("6.deleteFirst\n");
		printf("7.deleteLast\n");
		printf("8.printll\n");

		int ch;
		printf("Enter the Choice : \n");
		scanf("%d",&ch);

		switch(ch){
		
			case 1:
				addNode();
				break;
			case 2:
				addFirst();
				break;
			case 3:
				addLast();
				break;
			case 4:
				{
				       int pos;
				       printf("Enter the Position : \n");
				       scanf("%d",&pos);
				       addAtPos(pos);
			       }
				break;
			case 5:
				{
					int count = nodeCount();
					printf("Count : %d \n",count);
				}
				break;
			case 6:
				deleteFirst();
				break;
			case 7:
				deleteLast();
				break;
			case 8:
				printll();
				break;
			default :
				printf("Enter the Valid Choice \n");
				break;
		}

		getchar();
		printf("Do you wnat to Continue : \n");
		scanf("%c",&choice);

	}while(choice=='Y' || choice=='y');


	printf("\n\n <----------------END Main----------------->\n\n");
}
