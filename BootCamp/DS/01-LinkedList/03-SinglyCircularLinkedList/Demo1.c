
#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{
	int data;
	struct Demo *next;
}demo;

demo * head = NULL;

// 0
demo * createNode(){

	demo * newNode = (demo *)malloc(sizeof(demo));

	printf("Enter the Data  : \n");
	scanf("%d",&(newNode->data));

	newNode->next = NULL;

	return newNode;
}

// 1
void addNode(){

	demo *newNode = createNode();

	if(head==NULL){
	
		head = newNode;
		newNode->next = head;
	}else{
	
		demo *temp = head;
		while(temp->next != head){
		
			temp = temp->next;
		}
	
		temp->next=newNode;
		newNode->next=head;
	}
}

// 2
void addFirst(){

	demo *newNode = createNode();

	if(head == NULL){
	
		head = newNode;
		newNode->next = head;
	}else{
	
		demo *temp = head;
		while(temp->next != head){
			temp = temp->next;
		}
		temp->next = newNode;
		newNode->next = head;
		head = newNode;
	}
}

//3
void addLast(){
	addNode();
}

//5
int nodeCount(){

	if(head==NULL){
		printf("No Node Present \n");
		return 0;
	}else if(head->next ==head){
	
		return 1;
	}else{
		demo *temp = head;
		int count = 1;

		while(temp->next != head){
	
			temp = temp->next;
			count++;
		}

		return count;
	}
}

//4
void addAtPos(int pos){

	int count = nodeCount();

	if(pos> (count+1) || pos<1){
	
		printf("Please Enter Valid Position \n");
	}else if(pos == 1){
	
		addFirst();
	}else if(pos == count+1){
	
		addLast();
	}else{
	
		demo *temp = head;
		while(pos-2){
		
			temp= temp->next;
			pos--;
		}
		demo *newNode = createNode();

		newNode->next = temp->next;
		temp->next = newNode;
	}
}

//6
void deleteFirst(){

	if(head==NULL){
		printf("No Nodes Are present \n");
	}else if(head->next == head){
	
		free(head);
		head = NULL;
		printf("Deleted Last Node of Linked List No Nodes Present Now \n");
	}else{
		demo *temp = head;
		while(temp->next != head){
		
			temp = temp->next;
		}
		temp->next = head->next;
		temp=head;
		head = head->next;				// 1 St Way 
		//head = temp->next;				// 2 Nd Way			
		free(temp);
	}
}

//7
void deleteLast(){

	demo *temp = head;

	if(head->next == head){
	
		free(temp);
		head = NULL;
		printf("Deleted Last Node of Linked List No Nodes Present Now \n");

	}else if(head->next != head){
	
		while(temp->next->next != head){
		
			temp = temp->next;
		}
		free(temp->next);
		temp->next = head;
	}else{
	
		printf("No Nodes Are present \n");
	}
}

// 8
void delAtPos(int pos){

	int count = nodeCount();
	
	if(pos<=0 || pos>count){
		
		printf("Invalid Position Inturred \n");
	}else{
	
		if(pos==1){

			deleteFirst();

		}else if(pos == count){
		
			deleteLast();
		}else{
		
			demo *temp =head;

			while(pos-2){
				temp=temp->next;
				pos--;
			}
			demo *temp2 = temp->next;
			temp->next = temp->next->next;
			free(temp2);
		}
	}
}


//9
void printll(){

	if(head == NULL){
	
		printf("No Nodes Are Present \n");
	}else{
		demo *temp =head;
		while(temp->next != head){
	
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}


void main(){

	printf("\n\n <----------------START Main----------------->\n\n");
	
	char choice;

	do{
	
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addLast\n");
		printf("4.addAtPos\n");
		printf("5.countNode\n");
		printf("6.deleteFirst\n");
		printf("7.deleteLast\n");
		printf("8.deleteAtPos\n");
		printf("9.printll\n");

		int ch;
		printf("Enter the Choice : \n");
		scanf("%d",&ch);

		switch(ch){
		
			case 1:
				addNode();
				break;
			case 2:
				addFirst();
				break;
			case 3:
				addLast();
				break;
			case 4:
				{
				       int pos;
				       printf("Enter the Position : \n");
				       scanf("%d",&pos);
				       addAtPos(pos);
			       }
				break;
			case 5:
				{
					int count = nodeCount();
					printf("Count : %d \n",count);
				}
				break;
			case 6:
				deleteFirst();
				break;
			case 7:
				deleteLast();
				break;
			case 8:
				{
				       int pos;
				       printf("Enter the Position : \n");
				       scanf("%d",&pos);
				       delAtPos(pos);
			       }
				break;
			case 9:
				printll();
				break;
			default :
				printf("Enter the Valid Choice \n");
				break;
		}

		getchar();
		printf("Do you wnat to Continue : \n");
		scanf("%c",&choice);

	}while(choice=='Y' || choice=='y');


	printf("\n\n <----------------END Main----------------->\n\n");
}
