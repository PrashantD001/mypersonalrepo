
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Employee{

	int empId;
	char empName[20];
	float sal;
	struct Employee *next;
}Emp;

void main(){

	Emp *e1 = (Emp *)malloc(sizeof(Emp));
	Emp *e2 = (Emp *)malloc(sizeof(Emp));
	Emp *e3 = (Emp *)malloc(sizeof(Emp));

	e1->empId = 1;
	strcpy(e1->empName,"kanha");
	e1->sal=50.00;
	e1->next = e2;

	e2->empId = 2;
	strcpy(e2->empName,"Rahul");
	e2->sal=60.00;
	e2->next = e3;

	e3->empId = 3;
	strcpy(e3->empName,"Ashish");
	e3->sal=65.00;
	e3->next = NULL;

	printf("%d\n",e1->empId);
	printf("%s\n",e1->empName);
	printf("%f\n",e1->sal);
	
	printf("%d\n",e2->empId);
	printf("%s\n",e2->empName);
	printf("%f\n",e2->sal);
	
	printf("%d\n",e3->empId);
	printf("%s\n",e3->empName);
	printf("%f\n",e3->sal);
}
