
#include<stdio.h>


int isPresent(int arr[] , int size, int key){

	for(int i =0 ; i<size; i++){
	
		if(arr[i] == key){
			return i;
		}
	}
	return -1;
}

int lastOccur(int arr[],int size, int key){

	for(int i = size-1 ; i>=0; i--){
	
		if(arr[i] == key){
			return i;
		}
	}
	return -1;
}

int seclastOccur(int arr[],int size, int key){

	int count = 0 ;
	for(int i = size-1 ; i>=0; i--){
	
		if(arr[i] == key){
			count++;
			if(count ==2){
				return i;
			}
		}
	}
	return -1;
}

void main(){

	int num;

	printf("Enter the Size of Array \n");
	scanf("%d",&num);

	int arr[num];

	printf("Enter the Elements in Array :\n");
	for(int i =0; i<num; i++){
	
		scanf("%d",&arr[i]);
	}

	int key;
	printf("Enter the Number you Want to Serach : \n");
	scanf("%d",&key);

	int ans = isPresent(arr,num,key);

	if(ans==-1){
	
		printf("Given Key Not Present in Array \n");
	}else{

		printf("Given Key is Present in Array at Position %d \n",ans);
	}
	
	printf("Enter the Number you Want to find Last Occurance  : \n");
	scanf("%d",&key);

	ans = lastOccur(arr,num,key);

	if(ans==-1){
	
		printf("Given Key Not Present in Array \n");
	}else{

		printf("%d last occurance is : %d \n",key,ans);
	}
	
	printf("Enter the Number you Want to find Second last Occurance : \n");
	scanf("%d",&key);

	ans = seclastOccur(arr,num,key);

	if(ans==-1){
	
		printf("Given Key Not Present in Array \n");
	}else{

		printf("%d Second last occurance is : %d \n",key,ans);
	}
}
