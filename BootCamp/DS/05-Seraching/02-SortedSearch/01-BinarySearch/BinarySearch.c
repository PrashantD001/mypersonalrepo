
#include<stdio.h>


int isPresent(int arr[] , int size, int key){

	int start =0;
	int end = size-1;
	int mid =-1;

	while(start<=end){
	
		mid = (start+end)/2;

		if(arr[mid]==key){
		
			return mid;
		}else if ( arr[mid] <key){
		
			start = mid + 1;
		}else{
		
			end = mid - 1;
		}
	}
	return -1;
}

void main(){

	int num;

	printf("Enter the Size of Array \n");
	scanf("%d",&num);

	int arr[num];

	printf("Enter the Elements in Array :\n");
	for(int i =0; i<num; i++){
	
		scanf("%d",&arr[i]);
	}

	int key;
	printf("Enter the Number you Want to Serach : \n");
	scanf("%d",&key);

	int ans = isPresent(arr,num,key);

	if(ans==-1){
	
		printf("Given Key Not Present in Array \n");
	}else{

		printf("Given Key is Present in Array at Position %d \n",ans);
	}
}
