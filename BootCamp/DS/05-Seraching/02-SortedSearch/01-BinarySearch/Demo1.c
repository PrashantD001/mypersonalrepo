
#include<stdio.h>


int isPresent(int arr[] , int size, int key){

	int start =0;
	int end = size-1;
	int mid =-1;

	while(start<=end){
	
		mid = (start+end)/2;

		if(arr[mid]==key){
		
			return mid;
		}else if ( arr[mid] <key){
		
			start = mid + 1;
		}else{
		
			end = mid - 1;
		}
	}
	return -1;
}
int floorValue(int arr[] , int size, int key){

	if(arr[size-1]>key){
		return arr[size-1];
	}else{
		int start =0;
		int end = size-1;
		int mid =-1;
		int ans =-1;

		while(start<=end){
	
			mid = (start+end)/2;

			if(arr[mid]==key){
		
				return mid;
			}else if ( arr[mid] <key){
		
				ans = arr[mid];
				start = mid + 1;
			}else{
		
				end = mid - 1;
			}
		}
		return ans;
	}
}
int ceilingValue(int arr[] , int size, int key){

	if(arr[0]>key){
		return -1;
	}else{
		int start =0;
		int end = size-1;
		int mid =-1;
		int ans =-1;

		while(start<=end){
	
			mid = (start+end)/2;

			if(arr[mid]==key){
		
				return mid;
			}else if ( arr[mid] <key){
		
				start = mid + 1;
			}else{
		
				ans = arr[mid];
				end = mid - 1;
			}
		}
		return ans;
	}
}

int firstOccurance(int arr[] , int size, int key){

	if(arr[size-1]<key || arr[0]>key){
		return -1;
	}else{
		int start = 0;
		int end = size-1;
		int mid = -1;
		int ans = -1;

		while(start<=end){
	
			mid = (start+end)/2;

			if(arr[mid]>=key){
		
				if(arr[mid]==key){
					ans = mid;
				}
				end = mid -1;
			}else {
				start = mid + 1;
			}
		}
		return ans;
	}
}

int rotatedArray(int arr[] , int size, int key){

		
	int start =0;
	int end = size-1;
	int mid =-1;

	int temp = size-1;

	for(int i = 1; i < size; i++){
	
		if(arr[i]<arr[i-1]){
		
			temp = i-1;
			break;
		}
	}

	if(arr[size-1]>=key){
	
		start = temp+1;
		end = size-1;
	}else{
	
		start = 0;
		end = temp;
	}

	while(start<=end){
	
		mid = (start+end)/2;

		if(arr[mid]==key){
		
			return mid;
		}else if ( arr[mid] <key){

			start = mid + 1;
		}else{
		
			end = mid - 1;
		}
	}
	return -1;
}

void main(){

	int num;

	printf("Enter the Size of Array \n");
	scanf("%d",&num);

	int arr[num];

	printf("Enter the Elements in Array :\n");
	for(int i =0; i<num; i++){
	
		scanf("%d",&arr[i]);
	}

	int key;
	printf("Enter the Number you Want to Serach : \n");
	scanf("%d",&key);

	int ans = isPresent(arr,num,key);

	if(ans==-1){
	
		printf("Given Key Not Present in Array \n");
	}else{

		printf("Given Key is Present in Array at Position %d \n",ans);
	}
	
	printf("Enter the Number you Want to find Last Occurance  : \n");
	scanf("%d",&key);

	//ans = lastOccur(arr,num,key);

	if(ans==-1){
	
		printf("Given Key Not Present in Array \n");
	}else{

		printf("%d last occurance is : %d \n",key,ans);
	}
	
	printf("Enter the Number you Want to find Second last Occurance : \n");
	scanf("%d",&key);

	//ans = seclastOccur(arr,num,key);

	if(ans==-1){
	
		printf("Given Key Not Present in Array \n");
	}else{

		printf("%d Second last occurance is : %d \n",key,ans);
	}
}
