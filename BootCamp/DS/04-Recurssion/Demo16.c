

#include<stdio.h>

int NonTailDemo(int x){

	if(x==1){
	
		return 1;
	}

	return 3 + NonTailDemo(--x);
}

void main(){

	int ans = NonTailDemo(4);

	printf("%d\n",ans);
}
