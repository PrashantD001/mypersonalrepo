
//strlen-->string length

#include<stdio.h>
#include<string.h>

void main(){

	char name[10]={'K','L','R','a','h','u','l','\0'}; //11

	char *str="Virat Kohli"; //7

	int lenname=strlen(name);
	int lenstr=strlen(str);

	printf("%d\n",lenname);
	printf("%d\n",lenstr);

}
