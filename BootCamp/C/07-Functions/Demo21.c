



#include<stdio.h>

void fun(int a, int b);		// Hybrid Declaration

void main(){

	fun(10,20);	// Call and Arguments
}

void fun(int x, int y){	// Parameter and Body

	printf("%d \n",x+y);
}
