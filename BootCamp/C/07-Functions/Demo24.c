
// Call By Value

// Swapping of Two Number Using Third Variable

#include<stdio.h>

void swap(int,int);

void main(){

	int x=10, y=20;

	printf("x = %d\n",x);	//10
	printf("y = %d\n",y);	// 20

	swap(x,y);

	printf("x = %d\n",x);	// 10 
	printf("y = %d\n",y);	// 20
}

void swap(int x, int y){

	printf("x = %d\n",x);	// 10
	printf("y = %d\n",y);	// 20

	int temp =x;
	x = y;
	y = temp;

	printf("x = %d\n",x);	// 10
	printf("y = %d\n",y);	// 20
}
