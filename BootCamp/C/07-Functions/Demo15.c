

#include<stdio.h>

void funarr(int (*ptr)[]){

	printf("%d \n",**(ptr));		// 10
	printf("%d \n",**(ptr+2));		// Error
	printf("%d \n",*(*ptr+2));		// 30
}


void main(){

	int arr[]={10,20,30,40};
	
	funarr(&arr);
}
