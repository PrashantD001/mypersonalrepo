

#include<stdio.h>

void fun(int (*ptr)[][3][3] ,int arrSize){

	for(int i=0; i<arrSize; i++){
			printf("%d\t",*(***ptr+i));
	}
	printf("\n\n");
}

void fun1(int (*ptr)[][3][3] ,int pl, int ro, int co){

	int num = 0;
	for(int i=0; i<pl; i++){
	
		for(int j=0; j<ro; j++){
		
			for(int k=0; k<co; k++){
				printf("%d\t",*(***ptr+num));
				num++;
			}
			printf("\n");
		}
		printf("\n");
	}
}

void fun2(int (*ptr)[][3] ,int pl, int ro, int co){

	int num = 0;
	for(int i=0; i<pl; i++){
	
		for(int j=0; j<ro; j++){
		
			for(int k=0; k<co; k++){
				printf("%d\t",*(**ptr+num));
				num++;
			}
			printf("\n");
		}
		printf("\n");
	}
}

void fun3(int (*ptr)[] ,int pl, int ro, int co){

	int num = 0;
	for(int i=0; i<pl; i++){
	
		for(int j=0; j<ro; j++){
		
			for(int k=0; k<co; k++){
				printf("%d\t",*(*ptr+num));
				num++;
			}
			printf("\n");
		}
		printf("\n");
	}
}

void fun4(int *ptr ,int pl, int ro, int co){

	int num = 0;
	for(int i=0; i<pl; i++){
	
		for(int j=0; j<ro; j++){
		
			for(int k=0; k<co; k++){
				printf("%d\t",*(ptr+num));
				num++;
			}
			printf("\n");
		}
		printf("\n");
	}
}


void main(){

	int arr[2][3][3] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18};

	for(int i=0; i<2; i++){
	
		for(int j=0; j<3; j++){
		
			for(int k=0; k<3; k++){
				printf("%d\t",*(*(*(arr+i)+j)+k));
			}
			printf("\n");
		}
		printf("\n");
	}

	fun(&arr,18);
	fun1(&arr,2,3,3);
	fun2(arr,2,3,3);
	fun3(arr[0],2,3,3);
	fun4(arr[0][0],2,3,3);
}
