
// Passing Address of Array of Pointer to an Array

#include<stdio.h>

void arrPtrArr(int (*(*x)[2])[2]);

void main(){

	int arr1[2] = {10,20};
	int arr2[2] = {30,40};

	int (*pArr[2])[2] = {&arr1,&arr2};


	arrPtrArr(&pArr);
}

void arrPtrArr(int (*(*x)[2])[2]){

	printf("SuccessFul \n");
}
