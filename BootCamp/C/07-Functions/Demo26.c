
// 206

#include<stdio.h>

void callByAdress(int*);

void main(){

	int x =10;

	printf("%d \n",x);	// 10

	callByAdress(&x);	
	
	printf("%d \n",x);	// 50
}

void callByAdress(int *ptr){

	printf("%p \n",ptr);
	printf("%d \n",*ptr);	// 10

	*ptr = 50;
	
	printf("%d \n",*ptr);	// 50
}

