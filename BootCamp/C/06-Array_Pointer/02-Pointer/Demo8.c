

#include<stdio.h>

void main(){

	int x =10;
	int y =20;

	int *ptr1 = &x;

	printf("%d\n",*ptr1);		// 10 

	printf("%d\n",*(ptr1+1));	// 20
	printf("%d\n",*ptr1+1);		// 11

	printf("%p\n",(ptr1+'A'));		// Segmentasion fault or garbadge Address
	printf("%d\n",*(ptr1+'A'));

}
