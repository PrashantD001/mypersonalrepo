


#include<stdio.h>

void main(){

	int x= 10;

	printf("%d\n",x);		//10
	printf("%p\n",&x);		// 100


	int *iptr = &x;
	
	printf("%p\n",iptr);		// 100
	printf("%d\n",*iptr);		// 10

	*iptr = 30;

	printf("%p\n",iptr);		// 100
	printf("%d\n",*iptr);		// 30
	
	
	printf("%d\n",x);		// 30
}
