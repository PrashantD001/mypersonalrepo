

#include<stdio.h>

void main(){

	int iarr[3]={10,20,30};

	printf("Elements In Array : \n");
	for(int i=0; i<3; i++){

		printf("%d\n",iarr[i]);

	}

	int iarr2[3];

	//iarr2=iarr;		// Error
	//iarr2[]=iarr;		// Error
	iarr2[0]=iarr;		// Warning
	iarr2[1]=iarr[1];

	
	printf("Elements In Array-2 are  : \n");
	for(int i=0; i<3; i++){

		printf("%d\n",iarr2[i]);

	}
	

}
