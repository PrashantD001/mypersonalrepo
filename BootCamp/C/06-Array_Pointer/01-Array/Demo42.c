

//		3-D Array


#include<stdio.h>

void main(){

	int arr[2][3][3]={{{1,2,3},{4,5,6},{7,8,9}},{{10,11,12},{13,14,15},{16,17,18}}};


	printf("%ld\n",sizeof(arr));		// 72

	printf("%d\n",arr[0][1][1]);		// 5
	printf("%d\n",arr[1][0][1]);		// 11
}
