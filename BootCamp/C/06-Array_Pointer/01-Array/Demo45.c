

//		Pointer to an Array of 2-D array


//		172


#include<stdio.h>

void main(){

	int arr[2][3]= {1,2,3,4,5,6};

	int **ptr1 = &arr;
	
	int ***ptr2 = &arr;
	
	int (*ptr3)[2][3] = &arr;
}
