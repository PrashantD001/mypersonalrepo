//	Array of Pointer to an Array
//
//	166

#include<stdio.h>

void main(){

	int arr1[]={10,20,30};
	int arr2[]={40,50,60};

	printf("%p\n",&arr1);
	printf("%p\n",&arr2);

	int (*ptr1)[3]=&arr1;
	int (*ptr2)[3]=&arr2;

	//int (*ptr3)[3] = {ptr1, ptr2};	
	//printf("%p\n",*ptr3);

	//int* arr3[]={&arr1,&arr2}

	//int(*)[3] arr4[]= {&arr1,&arr2};
	
	//int (*arr4)[3]= {&arr1,&arr2};

//	int (*arr4[2])[3]= {&arr1,&arr2};
	
	int (*arr4[2])[3]= {ptr1,ptr2};

	printf("*arr4[0] = %p\n",*arr4[0]);
	printf("*arr4[1] = %p\n",*arr4[1]);
	
	printf("**arr4[0] = %d\n",**arr4[0]);
	printf("**arr4[1] = %d\n",**arr4[1]);
	
	*(arr4[0]++);

	printf("*arr4[0] = %p\n",*arr4[0]);
	printf("*arr4[1] = %p\n",*arr4[1]);
	
	printf("**arr4[0] = %d\n",**arr4[0]);
	printf("**arr4[1] = %d\n",**arr4[1]);

}
