

#include<stdio.h>

void main(){

	int x=10;

	const int *ptr = &x;

	printf("%d\n",x);		// 10

	*ptr = 30;		// Error
	
	printf("%d\n",x);	

}
