

#include<stdio.h>

void main(){

	int x=10;
	int y=20;

	const int* ptr = &x;

	printf("%d\n",x);		// 10
	printf("%d\n",*ptr);	// 10

	x = 30;		
	
	printf("%d\n",x);	// 30
	printf("%d\n",*ptr);	// 30

	*ptr=50;		// Error
	
	printf("%d\n",x);	
	printf("%d\n",*ptr);	

	ptr=&y;		// No Error

}
