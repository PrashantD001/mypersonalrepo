

#include<stdio.h>

void main(){

	int x=10;
	int y=20;

	const int* const ptr = &x;

	printf("%d\n",x);		// 10

	*ptr = 30;		// Error
	
	printf("%d\n",x);	// 30

	ptr = &y;		// Error

}
