

//		MISCELLANCOUS OPERATOR

#include<stdio.h>

void main(){
	/*
	int x = 10,20,30;	// Error : Identifier expected
	printf("%d\n",x);
	*/
	int y = {10,20,30};	// Warning : excess elements in scalar initilize
	printf("%d\n",y);	//10
	
	int z = (10,20,30);
	printf("%d\n",z);	// 30
	
}
