
#include<stdio.h>

#include<stdlib.h>

#include<string.h>

struct Society{

	char sname[20];

	int wings;

	double avrrent;

};

void main(){

	struct Society *ptr=(struct Society*)malloc(sizeof(struct Society));

	strcpy((*ptr).sname,"Omkar");
	ptr->wings=8;
	(*ptr).avrrent=50.00;

	printf("%s\n",(*ptr).sname);
	printf("%d\n",(*ptr).wings);
	printf("%f\n",ptr->avrrent);

}
