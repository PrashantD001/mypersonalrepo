//union

#include<stdio.h>

union Demo{

	int empid;

	float sal;

}obj2;

void main(){

	union Demo emp1={10,50.60};

	printf("%d\n",emp1.empid); //10

	printf("%d\n",emp1.sal);

	union Demo emp2;

	emp2.empid=15;

	printf("%d\n",emp2.empid); //15

	emp2.sal=70.65;

	printf("%f\n",emp2.sal); //70.65

}
