
//nested structure

#include<stdio.h>
#include<string.h>

struct movieinfo{

	char actor[20];

	float imdb;

};

struct movie{

	char mname[20];
	struct movieinfo obj1;

};
void main(){

	struct movie obj2={"tumbbad",{"sohanshan",8.9}};

	printf("%s\n",obj2.mname);
	printf("%s\n",obj2.obj1.actor);
	printf("%f\n",obj2.obj1.imdb);

}
