

class GGParent1(object):
    def __init__(self):
        super().__init__()
        print("Grand Grand Parent-1 Constructor ")

class GParent1(GGParent1):
    def __init__(self):
        super().__init__()
        print("Grand Parent-1 Constructor ")

class GParent2(GGParent1):
    def __init__(self):
        super().__init__()
        print("Grand Parent-2 Constructor ")

class GParent3(GGParent1):
    def __init__(self):
        super().__init__()
        print("Grand Parent-3 Constructor ")

class Parent1(GParent1,GParent2):
    def __init__(self):
        super().__init__()
        print("Parent-1 Constructor ")

class Parent2(GParent2,GParent3):
    def __init__(self):
        super().__init__()
        print("Parent-2 Constructor ")

class Child1(Parent1,Parent2):
    def __init__(self):
        super().__init__()
        print("Child1-1 Constructor ")

class GChild(Child1):
    def __init__(self):
        super().__init__()
        print("Grand Child - Constructor ")

print(GChild.mro())
obj=GChild()
