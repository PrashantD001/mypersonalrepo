

class Parent1:
    def __init__(self):
        print("Parent-1 Constructor ")

class Parent2:
    def __init__(self):
        print("Parent-2 Constructor ")

class Child(Parent1,Parent2):
    def __init__(self):
        super().__init__()
        print("Child - Constructor ")

obj=Child()
