

class GParent1(object):
    def __init__(self):
        super().__init__()
        print("Grand Parent-1 Constructor ")

class Parent1(GParent1):
    def __init__(self):
        super().__init__()
        print("Parent-1 Constructor ")

class Parent2(GParent1):
    def __init__(self):
        super().__init__()
        print("Parent-2 Constructor ")


class Child(Parent1,Parent2):
    def __init__(self):
        super().__init__()
        print("Child - Constructor ")

print(Child.mro())
obj=Child()
