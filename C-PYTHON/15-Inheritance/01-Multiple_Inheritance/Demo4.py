

class GParent1(object):
    def __init__(self):
        super().__init__()
        print("Grand Parent-1 Constructor ")

class GParent2(object):
    def __init__(self):
        super().__init__()
        print("Grand Parent-2 Constructor ")

class GParent3(object):
    def __init__(self):
        super().__init__()
        print("Grand Parent-3 Constructor ")

class Parent1(GParent1):
    def __init__(self):
        super().__init__()
        print("Parent-1 Constructor ")

class Parent2(GParent2):
    def __init__(self):
        super().__init__()
        print("Parent-2 Constructor ")

class Parent3(GParent3):
    def __init__(self):
        super().__init__()
        print("Parent-3 Constructor ")


#class Child(Parent2,Parent1):
class Child1(Parent1,Parent2):
    def __init__(self):
        super().__init__()
        print("Child1-1 Constructor ")

class Child2(Parent2,Parent3):
    def __init__(self):
        super().__init__()
        print("Child-2 Constructor ")

class GChild(Child1,Child2):
    def __init__(self):
        super().__init__()
        print("Grand Child - Constructor ")

print(GChild.mro())
obj=GChild()
