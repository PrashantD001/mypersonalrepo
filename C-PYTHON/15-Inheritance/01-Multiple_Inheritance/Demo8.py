

class GGParent1(object):
    def __init__(self):
        super().__init__()
        print("Grand Grand Parent-1 Constructor ")

class GParent1(GGParent1):
    def __init__(self):
        super().__init__()
        print("Grand Parent-1 Constructor ")

class GParent2(GGParent1):
    def __init__(self):
        super().__init__()
        print("Grand Parent-2 Constructor ")

class Parent1(GParent1,GParent2):
    def __init__(self):
        super().__init__()
        print("Parent-1 Constructor ")

class Child1(Parent1):
    def __init__(self):
        super().__init__()
        print("Child-1 Constructor ")

class Child2(Parent1):
    def __init__(self):
        super().__init__()
        print("Child-2 Constructor ")

class GChild(Child1,Child2):
    def __init__(self):
        super().__init__()
        print("Grand-Child - Constructor ")

print(GChild.mro())
obj=GChild()



