
class Parent :
    x = 100
   
class Child1(Parent):
    def disp(self):
        print(self.x)

class Child2(Parent):
    def disp(self):
        print(self.x)

obj1=Child1()
obj2=Child2()

print("At Begining : ")
obj1.disp()
obj2.disp()

print("After change using class name : ")
Parent.x=200
obj1.disp()
obj2.disp()

print("After change using Child one object  :")
obj1.x = 300
obj1.disp()
obj2.disp()

print("After change using class name : ")
Parent.x=400
obj1.disp()
obj2.disp()

print("after changing child one varible equal to Parent class variable : ")
obj1.x = 400
obj1.disp()
obj2.disp()

print("After change using class name : ")
Parent.x=500
obj1.disp()
obj2.disp()
