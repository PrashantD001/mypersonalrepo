

class RBI:
    NoOfEmployee = 1500
    Currancy = "Rupee"

    def __init__(self):
        print("Your are an Under of Reserve Bank of India ")
        self.RateOfInterest = 2.5

    @classmethod
    def Rules(cls):
        print("Common Rules")

    def Work(self):
        print("Working as a Employee of RBI ")


class BOM(RBI):

    def __init__(self,income):
        self.income=income
        super().__init__()
        print("Your are an Member of Bank of Maharastra ")
        self.AdditionalROI = 4.5

    def Loan(self):
        print("No. of employee in RBI : ",self.NoOfEmployee)
        print("Currancy : ",self.Currancy)
        self.Rules()
        print("Rate of Interest By RBI : ",self.RateOfInterest)
        print("Rate of Interest By BOM : ",self.AdditionalROI)
        if(self.income<500000):
            print("Your Loan Is Denied")
        else:
            print("Your Loan is Approved")

i1=int(input("Enter your Annual Income : "))
Person1=BOM(i1)
Person1.Loan()

i2=int(input("Enter your Annual Income : "))
Person2=BOM(i2)
Person2.Loan()
