
class Demo:
    temp = 1
    num = 0
    print("Before If ")
    if(temp==1):
        print("In If ")
        def __init__(self,*a):
            print("In Constructor - 1 ")
            print(self.__class__)
            Demo.temp=0
            Demo.num=Demo.num+1
            if(Demo.num<=2):
                self.__class__.__init__(self,10,20)
            print("After if ")
    elif(temp==0):
        print("In Elif")
        def __init__(self,x=10,y=20,z=30):
            print("In Constructor - 2 ")
            Demo.temp=1

print("Before Object")

obj1=Demo()
Demo.temp=0
#obj2=Demo(10,20)



