

#           179

str1="core2web"

# capitalize
print(str1.capitalize())
str2=str1.capitalize()

# casefold()
print(str2.casefold())
print(str1.casefold()==str2.casefold())

# cemter()
print(str1.center(20))


# count()
print(str1.count('e'))
print(str1.count('X'))
print(str1.count('e',0,5))
print(str1.endswith("web"))


str1="Core2web Core2web"


# find()
print(str1.find("web"))
print(str1.rfind("web"))

#  index()
print(str1.index("web"))
print(str1.rindex("web"))

# join()
print(str1.join("X"))
print("#".join(str1))


str1="Core2web"


# ljust()
print(str1.ljust(10))
print(str1.rjust(10))
print(str1.ljust(10,"X"))
print(str1.rjust(10,"X"))

# lower()
print(str1.lower())


str1="     Core2Web     "


# lstrip()
print(str1.lstrip())
print(str1.rstrip())
print(str1.strip())


str1="C2W,Core2web,BienCaps"


# partition()
print(str1.partition(","))
print(str1.rpartition(","))


str1="Core2Web"


# replace
print(str1.replace("Web","Mobile"))


str1="C2W Core2web BienCaps"


# split()
print(str1.split(" "))
str2=str1.split(" ")
print(str2)

# startswith()
print(str1.startswith("Core2"))
print(str1.startswith("Cow"))

# swapcase()
print(str1.swapcase())


str3="coew python"


# title()
print(str3.title())

## upper()
print(str1.upper())


str3="python"


# zfill()
print(str1.zfill(10))
print(str3.zfill(10))


































