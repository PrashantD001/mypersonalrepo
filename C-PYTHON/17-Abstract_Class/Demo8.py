

from abc import ABC,abstractmethod

class Shop(ABC):
    
    owner = "PN"

    def __init__(self):
        print("Wel-Come To Shop ")
        self.counter = "Access "


    @abstractmethod
    def Payment(self):
        pass

    def Shoping(self):
        print("DO Shoping")

    @classmethod
    def Display(cls):
        print("Show-Case Display of Products ")


class Customor1(Shop):

    def Payment(self):
        print("Payment By Cash-Mode")

class Customor2(Shop):

    def Payment(self):
        print("Payment By Online-Mode")


rohit=Customor1()
rohit.Shoping()
rohit.Display()
rohit.Payment()

print()

rahul=Customor2()
rahul.Shoping()
rahul.Display()
rahul.Payment()

