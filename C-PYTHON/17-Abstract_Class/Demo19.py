
from abc import ABC,abstractmethod

class MyAbstractClass(type):

    def __new__(cls,clsname,basecls,clsdict):
        print(cls)
        print(clsdict)
       # print(clsdict[__qualname__])
        print(clsname)
        print(type(clsname))
        if(len(clsname.__abstractmethods__)!=0):
            raise TypeError("Abstracted method la body dili nahi ")

        return super().__new__(cls,clsname,basecls,clsdict)

#print(MyAbstractClass.__abstractmethods__)
#print(dir(MyAbstractClass))


class Demo(metaclass=MyAbstractClass):
    def __init__(self):
        print("In Constructor ")
    @abstractmethod
    def fun(self):
        pass
'''
print(dir(Demo))
print(Demo.__abstractmethods__)
print(len(Demo.__abstractmethods__))
#print(Demo.__abstractmethods__.frozenset)
#print(Demo.__isabstractmethod__)
#obj = Demo()
'''

class Child(Demo):
    
    def fun(self):
        print("In Fun")

#print(dir(Child))

obj=Child()

#obj=Child(Child,(),Child.__dict__)

'''
print(Child.__abstractmethods__)
print(len(Child.__abstractmethods__))
'''
