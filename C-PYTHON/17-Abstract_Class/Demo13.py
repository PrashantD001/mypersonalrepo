

from abc import ABCMeta,ABC

class Parent(ABC):
    pass

print(type(Parent))     # <class 'abc.ABCMeta'>


class Parent(ABCMeta):
    pass

print(type(Parent))     # <class 'type'>


class Parent(metaclass=ABCMeta):
    pass

print(type(Parent))     # <class 'abc.ABCMeta'>
