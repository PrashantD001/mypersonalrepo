
from abc import ABC,abstractmethod

class Parent(ABC):

    @abstractmethod
    def info(self):
        None

class Child(Parent):

    def info(self):
        print("In info")

obj=Child()
obj.info()


print(type(Parent))     # <class 'abc.ABCMeta'>

print(type(Child))      # <class 'abc.ABCMeta'>
