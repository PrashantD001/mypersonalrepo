

class CWMeta(type):

    def __new__(cls,clsname,basecls,clsdict):
        print(cls)
        print(clsname)
        print(basecls)
        print(clsdict)

        return super().__new__(cls,clsname,basecls,clsdict)

class Child(CWMeta):
    pass

#obj1 = CWMeta('CWMeta',(),{})
#obj2 = Child('Child',(),{'a':10,'b':20})

class Child2(metaclass=CWMeta):
    a=20
    b=30
print()
print(type(CWMeta))
print(dir(CWMeta))
print()
print(type(Child))
print(dir(Child))
print()
print(type(Child2))
print(dir(Child2))
'''
obj3 = Child2('Child2',(),{'a':10,'b':20})
#obj3 = Child2()
obj3 = Child2('Child2',(),{'a':10,'b':20})
'''
