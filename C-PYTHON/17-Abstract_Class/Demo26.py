
from abc import ABC , abstractmethod , abstractproperty

class Company(ABC):
    @abstractmethod
    def info(self):
        pass

    @abstractproperty
    def name(self):
        pass

class Employee(Company):
    def __init__(self,ename):
        self.ename=ename

    def info(self):
        print(self.ename)

    #@property
    def name(self):
        return self.ename

obj=Employee("Rajesh")
obj.info()
#obj.name()
print(obj.name)
obj.name()
