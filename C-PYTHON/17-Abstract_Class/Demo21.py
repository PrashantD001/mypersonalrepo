
#                   SINLETON DESIGN PATTERN-01

class MetaClass(type):

    _instance = {}

    def __call__(cls, *args, **kwargs):

        if cls not in cls._instance:
            cls._instance[cls] = super(MetaClass,cls).__call__(*args, **kwargs)
            return cls._instance[cls]
        else:
            raise TypeError("Object Alreay Created")


class A(metaclass=MetaClass):

    def __init__(self):
        print("Constructor - A")

a=A()
print(a)

b=A()
print(b)

