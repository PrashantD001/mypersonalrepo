
from abc import ABC,abstractmethod

class Parent(ABC):

    #@classmethod
    @abstractmethod
   # @classmethod
    def Marry(self):
        print("Disha Patani")
        pass

class Child(Parent):

    @classmethod
    def Marry(self):
       # super().Marry()     #Disha Patani
        print("Alia Bhatt")

obj = Child()
obj.Marry()         #Alia Bhatt

print()
print(dir(obj))
print()
print(dir(Parent))
