

class XYZ:

    def fun(self,cls):
        print("In fun")
        cls.gun()


class PQR:

    @classmethod
    def gun(cls):
        print("In gun")

obj = XYZ()
obj.fun(PQR)
