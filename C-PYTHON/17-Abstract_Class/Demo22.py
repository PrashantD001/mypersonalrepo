
#                   SINLETON DESIGN PATTERN-02

class MetaClass(type):

    _instance = None

    def __new__(cls, clsname, basecls, clsdict):

        if (cls._instance==None):
            cls._instance = cls.__call__(clsname,basecls, clsdict)
            return cls._instance
        else:
            cls._instance

        return super().__new__(cls, clsname, basecls, clsdict)


class A(metaclass=MetaClass):
    pass

a=A()
print(a)

b=A()
print(b)

