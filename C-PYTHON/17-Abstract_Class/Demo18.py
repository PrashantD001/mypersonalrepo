
from abc import ABC,abstractmethod

class Demo(ABC):
    def __init__(self):
        print("In Constructor ")
    @abstractmethod
    def fun(self):
        pass
print(dir(Demo))
print(Demo.__abstractmethods__)
print(len(Demo.__abstractmethods__))
#print(Demo.__abstractmethods__.frozenset)
#print(Demo.__isabstractmethod__)
#obj = Demo()


class Child(Demo):
    
    def fun(self):
        print("In Fun")


print(Child.__abstractmethods__)
print(len(Child.__abstractmethods__))

