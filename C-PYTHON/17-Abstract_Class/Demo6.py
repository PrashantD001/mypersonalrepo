
from abc import ABC,abstractmethod

class Parent(ABC):

    @abstractmethod
    def Marry(self):
        print("Disha Patani")
        pass

class Child(Parent):
    
    def Marry(self):
        #super().Marry()     #Disha Patani
        print("Alia Bhatt")

obj = Child()
obj.Marry()         #Alia Bhatt
