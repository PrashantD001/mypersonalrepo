


num=int(input("Enter the Number of Rows : "))

temp=num//2

for i in range(num):
    
    for j in range(num):
        if(i<=temp):
            if(j<temp-i):
                print("   ",end="\t")
            elif(j<(i*2)+1+(temp-i)):
                if(j<=temp):
                    print(" ",j+1," ",end="\t")
                else:
                    print(" ",(num-j)," ",end="\t")

        else:
            if(j<i-temp):
                print("   ",end="\t")
            elif(j<(i*2)+1+(temp-i)):
                if(j<=temp):
                    print(" ",j+1," ",end="\t")
                elif(j>i):
                    print(" ",(num-j)," ",end="\t")

    print()
