
class Addition:
    def add(self,x,y):
        return x+y

    def add(self,x,y,z):
        return x+y+z

obj = Addition()

print(obj.add(10,20,30))    # 60
print(obj.add(10,20))       # Type Error
