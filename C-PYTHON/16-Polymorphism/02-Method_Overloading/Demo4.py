
from multipledispatch import dispatch

class Addition:

    @dispatch(int,int)
    def add(self,x,y):
        return x+y

    @dispatch(int,int,int)
    def add(self,x,y,z):
        return x+y+z
    
    @dispatch(float,float)
    def add(self,x,y):
        return x+y

    @dispatch(float,float,float)
    def add(self,x,y,z):
        return x+y+z


obj = Addition()

print(obj.add(10,20))           # 30
print(obj.add(10,20,30))        # 60
print(obj.add(5.5,10.2))           # 15.7
print(obj.add(20.5,30.5,10.5))        # 61.5
