
class Xyz:
    def __init__(self):
        self.x=10
        self.y=20

    def show(self):
        print(self.x)
        print(self.y)

    def __add__(self,obj):          # Addition
        return self.x+obj.x

    def __le__(self,obj):           # less than equal to
        if(self.x<=obj.x):
            return True
        else:
            return False

    def __eq__(self,obj):           # equal to
        if(self.x==obj.x):          
            return True
        else:
            return False

    def __ge__(self,obj):           # Gretater than equal to
        if(self.x>=obj.x):
            return True
        else:
            return False

    def __gt__(self,obj):           # Greater than
        if(self.x>obj.x):
            return True
        else:
            return False

    def __lt__(self,obj):           # less than
        if(self.x<obj.x):
            return True
        else:
            return False
    
    def __ne__(self,obj):           # Not equal to
        if(self.x!=obj.x):          
            return True
        else:
            return False



obj1=Xyz()
obj2=Xyz()

obj1.show()
obj2.show()

a=50
b=60
print(a+b)

print(obj1+obj2)
print(obj1<=obj2)
print(obj1<obj2)
print(obj1>=obj2)
print(obj1>obj2)
print(obj1==obj2)
print(obj1!=obj2)

