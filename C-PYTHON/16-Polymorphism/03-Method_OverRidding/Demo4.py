
class Parent: 

    x=10

    @classmethod
    def show(self):
        print(self.x)

class Child(Parent):

    x=20

    @classmethod
    def show(self):
        print(self.x)
        super().show()      


obj=Child()
obj.show()      # 20 10
