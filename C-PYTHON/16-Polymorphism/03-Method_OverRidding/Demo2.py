
class Parent:
    x=10
    def view(self):
        print(self.x)

class Child(Parent):
    x=20
    def show(self):
        print(self.x)           # 20


obj=Child()
obj.show()      # 20
obj.view()      #20
