
class Parent:
    x=10
    @classmethod
    def show(self):
        print(self.x)

class Child(Parent):

    x=20

    def __init__(self):
        self.y=20

    def show(self):
        print(self.y)       # 20
        super().show()      


obj=Child()
obj.show()      # 20 10
