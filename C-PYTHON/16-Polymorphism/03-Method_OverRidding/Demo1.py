
class Parent:
    x=10
    def show(self):
        print(self.x)

class Child(Parent):
    x=20
    def show(self):
        print(self.x)           # 20
        print(super().x)        # 10
        print(Parent.x)         # 10
        #print(super.x)

obj1=Parent()
obj1.show()

obj2=Child()
obj2.show()
