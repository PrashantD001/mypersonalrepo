
class Parent:
    x=10
    @classmethod
    def show(self):
        print(self.x)

class Child(Parent):

    def __init__(self):
        self.x=20

    def show(self):
        print(self.x)
        super().show()      


obj=Child()
obj.show()      # 20 10
