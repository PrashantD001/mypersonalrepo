
#               WAP to print the Pattern of FiccoNobi Series 

rows=int(input("Enter the Number of Rows : "))

num1=0
num2=1

for x in range(rows):

    for y in range(rows-x-1):
        print(" ", end="\t")

    for z in range(x+1):
        print(num1,end="\t")

        num3=num1+num2
        num1=num2
        num2=num3

    print()
