import array

iarr = array.array('i',[])

a = int(input("Enter the Range of Array : "))

print (" Enter The Values : ")

for x in range (a):
    iarr.append(int(input()))

print("The Prime Numbers Are :", end=" : ")

for y in range (len(iarr)):
    count=0
    if(iarr[y]!=0):
        for z in range(2,iarr[y]//2):
            if(iarr[y]%z==0):
                count+=1
                break
        if(count==0):
            print(iarr[y],end=" , ")
    else:
        print(iarr[y],end=" , ")

