import array

iarr = array.array('i',[])

a = int(input("Enter the Range of Array : "))

print (" Enter The Values : ")

for x in range (a):
    iarr.append(int(input()))

print("Before Reverse : ")
print(iarr)

iarr.reverse()
print("After Reverse : ")
print(iarr)
