a=int(input("Enter the Number of Row : "))

for x in range(a):
    num=97+x
    temp=1+x

    for y in range(a+1):
        if(y<(a-x)):
            print(" ",end=" ")

        else:
            if(x%2!=0):
                print(temp,end=' ')
            else:
                print(chr(num),end=' ')

            num-=1
            temp-=1
    print()
