
class Outer:
    x=10
    def __init__(self):
        print("Outer - Constructor ")
        self.out=10

    class Inner:
        y=20
        def __init__(self):
            print("Inner - Conastructor ")
            self.inn=20

        def DispInner(self):
            print(self.inn)
            print(self.y)

    def DispOuter(self):
        print(self.out)
        print(self.x)

outObj = Outer()
outObj.DispOuter()

innObj = outObj.Inner()
innObj.DispInner()


innObj2 = Outer().Inner()
innObj2.DispInner()

print(outObj.__class__)
print(innObj.__class__)
