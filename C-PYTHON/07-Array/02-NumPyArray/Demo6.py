#           161

import numpy

#           1.view()          ---> Shallow Copy
arr1=numpy.array([10,20,30,40,50])
arr2=arr1.view()

print(id(arr1))
print(id(arr2))

print(arr1)
print(arr2)
print(id(arr1[2]))
print(id(arr2[2]))
arr2[2]=60
print(id(arr1[2]))
print(id(arr2[2]))
print(arr1)
print(arr2)


print()
print()



#       2.copy()    ----> Deep Copy
arr3=numpy.array([10,20,30,40,50])
arr4=arr3.copy()

print(id(arr1))
print(id(arr2))

print(arr3)
print(arr4)
print(id(arr3[2]))
print(id(arr4[2]))
arr4[2]=60
print(id(arr3[2]))
print(id(arr4[2]))
print(arr3)
print(arr4)
