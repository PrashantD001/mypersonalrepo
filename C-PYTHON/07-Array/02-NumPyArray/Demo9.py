import numpy

arr1=numpy.array([10,20,30])
arr2=numpy.array([[10,20,30],[40,50,6]])

# ndim
print(arr1.ndim)        # 1
print(arr2.ndim)        # 2

# size
print(arr1.size)        # 3
print(arr2.size)        # 6

#itemsize
print(arr1.itemsize)    # 8
print(arr2.itemsize)    # 8

# nbytwe
print(arr1.nbytes)    # 24
print(arr2.nbytes)    # 48

# shape
print(arr1.shape)      # (3, )
print(arr2.shape)      # (2,3, )  --> 2==row    3==col

# dtype
print(arr1.dtype)      # int64
print(arr1.dtype)      # int64

