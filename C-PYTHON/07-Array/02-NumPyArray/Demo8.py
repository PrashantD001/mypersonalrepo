

import numpy

#       1.array()

arr=numpy.array([[10,20,30,40,50],[60,70,80,90,100]])
print(arr)
print(type(arr))

'''
arr=numpy.array([10,20,30],[60,70,80])          # TypeError
print(arr)
'''

print(arr[0][2])        # 30
print(arr[1][3])        # 90
print(arr[1][4])        # 100
print(arr[0][4])        # 50
'''
print(arr[2][4])        # IndexError
'''


#       2.zeros()


num1=int(input("Enter no. of Rows : "))
num2=int(input("Enter no. of Col : "))

arr1=numpy.zeros((num1,num2),int)

print(arr1)

print(arr1[0])          # [1 2 3]
print(arr1[1])          # [4 5 6]
print(arr1[0][2])       # 3
'''
print(arr1[1][3])       # IndexError
'''


#       3. eye()


'''
arr2=numpy.eye(3,int)       # TypeError
'''

arr2=numpy.eye(3,dtype=int)
print(arr2)


arr3=numpy.eye(3,k=1,dtype=int)
print(arr3)




#       3.reshape()


arr4 = numpy.array([0,1,2,3,4,5,6,7,8,9])
rarr1=numpy.reshape(arr4,(2,5))
rarr2=numpy.reshape(arr4,(5,2))

print(rarr1)
print(rarr2)



















