        # 170

import numpy

arr = numpy.array([[1,2,3,],[4,5,6],[7,8,9]])

print(arr[0])
print(arr[0][2])
print(arr[0,2])
print(arr[0: , 0: ])


# Row 

#print(arr[0,2,1])    # IndexError

print(arr[0:2:1])
print(arr[0:2:1,])
print(arr[2: : , ])
print(arr[-1:-3: , ])
print(arr[-3:1:  ])


# Row-Col

print(arr[-3:-1:-2,1: ])
print(arr[2,1:])
print(arr[-1: , -3:-1])
print(arr[-2: ,-3:-1:1])
