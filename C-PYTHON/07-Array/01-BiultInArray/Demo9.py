

import array

iarr=array.array('I',[10,20,30,40])


print(iarr)

# 1.append(data)

iarr.append(50)
iarr.append(60)
iarr.append(70)
iarr.append(80)
iarr.append(30)
print(iarr)

# 2.count()
print(iarr.count(30))
print(iarr.count(40))

# 3.index()
print(iarr.index(60))

# 4.insert()
print(iarr.insert(3,35))
print(iarr)

# 5.pop()
iarr.pop()
print(iarr)

# 6.remove(object)
iarr.remove(50)
print(iarr)

# 7.reverse()
iarr.reverse()
print(iarr)

# 8.tolist(()
lst=iarr.tolist()
print(lst)
print(type(lst))

lst=[1,2,3,4]

# 9.fromlist
print(iarr)
iarr.fromlist(lst)
print(iarr)

# size of Arraty  --> 4 bytes of Integer
print(iarr.itemsize)

iarr1=array.array('u',['a','b','c'])
print(iarr1.itemsize)

# 10.buffer_info()
print(iarr.buffer_info())
print(iarr1.buffer_info())






























