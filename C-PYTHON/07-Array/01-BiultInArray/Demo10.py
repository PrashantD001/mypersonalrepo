        # 169


import array

arr=array.array('i',[10,20,30,40,50])

print(arr[2])   #30
print(arr[-3])  #30
print(arr[0:])   #[10 20 30 40 50 ]
print(arr[0:4])   #[10 20 30 40 ]
print(arr[0:4:2])   #[10 30 ]
print(arr[0:6:2])   #[10 30 50 ]
print(arr[6:2:-1])   #[50 40 ]
print(arr[-3:5:1])   #[30 40 50 ]
print(arr[4:0:1])   #[]
print(arr[4:0:-1])   #[50  40 30 20 ]


# Negative 

print(arr[-3:-5:])   #[]
print(arr[-3:-5:-1])   #[30 20 ]
print(arr[-3:-1:])   #[30 40 ]
print(arr[-3:-1:-1])   #[]


# Mix Mode 


print(arr[2:-1])   #[]
print(arr[4:-1])   #[]
print(arr[0:-1:2])   #[]
print(arr[-4:2:-1])   #[]
print(arr[-4:4])   #[]
