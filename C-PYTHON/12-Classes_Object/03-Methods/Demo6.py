
##  implementation of Decorator of @Classmethod  

def fun():
    print("in Fun ")
'''
def myclassmethod(fun):

    print("In myclassmethod ")
    def __get__(fun,cls):
        return lambda *args,**kw:fun(cls,*args,**kw)


    def magic(cls=None):
        if(cls==None):
            print("Call Using Class")
        else:
            print("Call Using Class")
        '''

class Cric:
    format = "T-20"
    
    def myclassmethod(self,*fun):
        def __get__(self,*cls):
            print("In MyDecorator")
            print(cls)
            return 0
        return __get__


    def __init__(self,name,JerNo):
        self.name = name
        self.JerNo = JerNo
        #fun()

    @myclassmethod          
    def DisplayData(cls):      
        print(cls)
        cls.format = "OneDay"
    

player1 = Cric("Dhoni",7)
player2 = Cric("Virat",18)

print(player1.format)       # T-20
print(player2.format)       # T-20

#Cric.DisplayData()
player1.DisplayData()
"""
player1.printData()
print(player1.format)       # Test
print(player2.format)       # T-20

Cric.DisplayData()
print(player1.format)       # Test
print(player2.format)       # OneDay

player1.DisplayData()
#player1.PrintData()
print(player1.format)       # Test
print(player2.format)       # OneDay
"""
