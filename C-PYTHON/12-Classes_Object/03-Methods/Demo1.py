
#               CLASS METHODS


class Cafe:
    menucard = 1
    def __init__(self,dish):
        self.dish = dish

    @classmethod
    def PrintData(cls):
            cls.menucard = 0;
    
obj1 = Cafe("Burger")
obj2 = Cafe("Large Fries")

print(obj1.menucard)
print(obj2.menucard)

obj1.PrintData()

print(obj1.menucard)
print(obj2.menucard)

