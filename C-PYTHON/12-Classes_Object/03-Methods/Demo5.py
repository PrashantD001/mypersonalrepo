
#               CLASS VARIABLES

# Change ic class Variable uing class Varibale it can reflected in all Objects.
# but the Change in class Variable using Object it will reflected in only that object


class Cafe:
    menucard = 1
    def __init__(self,dish):
        self.dish = dish

    def Order(self):
            print(self.dish)
            self.menucard = 0;
            #Cafe.menucard = 0          # Output --> 1
                                        #            1
                                        #            burger
                                        #            0
                                        #            0

obj1 = Cafe("Burger")
obj2 = Cafe("Large Fries")

print(obj1.menucard)
print(obj2.menucard)

obj1.Order()

print(obj1.menucard)
print(obj2.menucard)

