
## 256 

class Cric:
    format = "T-20"

    def __init__(self,name,JerNo):
        self.name = name
        self.JerNo = JerNo

    @classmethod          
    def DisplayData(cls):      
        cls.format = "OneDay"
    
    def printData(self):      
        self.format = "Test"

player1 = Cric("Dhoni",7)
player2 = Cric("Virat",18)

print(player1.format)       # T-20
print(player2.format)       # T-20

player1.printData()
print(player1.format)       # Test
print(player2.format)       # T-20

Cric.DisplayData()
print(player1.format)       # Test
print(player2.format)       # OneDay

player1.DisplayData()
#player1.PrintData()
print(player1.format)       # Test
print(player2.format)       # OneDay

