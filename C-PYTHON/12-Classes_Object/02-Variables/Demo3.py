
# Making Variable Private which cannot be access in Ouside of the Class

class Demo:
    def __init__(self,var):
        self.__var = var

    def fun(self):
       # print(self.var)             # Atribute Error
        print(self.__var)

x=int(input("Enter the Value : "))
obj=Demo(x)
obj.fun()
#print(obj.__var)                   # Atribute Error
