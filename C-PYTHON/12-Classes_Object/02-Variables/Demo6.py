

class Demo:
    x=20
    def __init__(self):
        self.y=10

    def fun(self):
        #print(self.y)
        print(self.x)

obj1=Demo()
obj2=Demo()

print("At  Begining : ")
obj1.fun()
obj2.fun()

print("After changing Class variable on Object one : ")
obj1.x=50
obj1.fun()
obj2.fun()

print("After changing class variiable with class name : ")
Demo.x=80
obj1.fun()
obj2.fun()

print("After changing changed class variable of Object one equal to class Variable : ")
obj1.x=80
obj1.fun()
obj2.fun()

print("changing class variable with class name when all variables have same data : ")
Demo.x=100
obj1.fun()
obj2.fun()


