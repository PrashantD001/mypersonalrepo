

'''
def fun(num):
    if(num>0):
        return num*fun(num-1)       # Type Error

print(fun(5))

'''
print()

'''
def fun(num):
    if(num==1):
        return    
    return num*fun(num-1)       # Type Error

print(fun(5))
'''




def fun(num):
    if(num==1):
        return 1
    return num*fun(num-1)

print(fun(5))
