

#           228



def rev(fun):
    def Inner(*args):
        str2 = fun(*args)
        return str2[::-1]
    return Inner

def Concat(fun):
    def Inner(*args):
        retTup=fun(*args)
        str2 = ""
        for x in retTup:
            str2+=x
        return str2
    return Inner

@rev
@Concat
def PrintStr(str1,str2):
    return str1,str2

print(PrintStr("Shashi","Bagal"))



