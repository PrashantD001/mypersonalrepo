
def fun(**args):
    for x in args:
        print(x)

fun(a=10)
fun(a=10,b=20,c=30)
fun(a=10,b=20)


def fun1(**args):
    for x,y in args.items():
        print(x," = ",y)

fun1(a=10)
fun1(a=10,b=20,c=30)
fun1(a=10,b=20)

