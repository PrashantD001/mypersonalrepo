

def Palin(fun):
    def Inner(x):
        temp1=x
        #print(temp1)
        temp2 = fun(x)
        #print(temp2)
        if(temp1==temp2):
            return "Given Number is Palindrome "
        else:    
            return "Given Number is Not Palindrome "
        
    return Inner

def Rev(fun):
    def Inner(x):
        temp1=0
        num=0
        #print(x)
        while(x!=0):
            rem=x%10
            x=x//10
            num=num*10+rem
            #print(num)
        return num
    return Inner

@Palin
@Rev
def PrintNum(x):
    return x


a=int(input("Enter the Number : "))

print(PrintNum(a))



