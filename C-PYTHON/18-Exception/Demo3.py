

# EOFError and  KeyboardInturrupt Error
#end of file --> EOF

print("Before fun ")

def fun():
    print("In fun")

a=input("Enter Value : ")                   # 1 - ctr + D --> EOFError
                                            # 2 - ctr + C --> KeyboardInturrupt
print(a)

print("After fun")
