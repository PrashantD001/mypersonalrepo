

try:
    print("In Outer Try ")
    try:
        print("In Inner Try ")
    except:
        print("In Inner Except ")
    finally:
        print("In Inner Finally ")
except:
    print("In Outer Except ")
finally:
    print("In Outer Finally ")
