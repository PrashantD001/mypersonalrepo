

try:
    print("In Outer Try ")
    #print(10/0)
    try:
        print("In Inner Try ")
        print(10/0)
    except ValueError:
        print("In Inner Except ")
    finally:
        print("In Inner Finally ")
except ZeroDivisionError:
    print("In Outer Except ")
finally:
    print("In Outer Finally ")
