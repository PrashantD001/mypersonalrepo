

def fun(a,b):
    try:
        x=a/b
    except ZeroDivisionError:
        print("Divide by Zero")
    print(x*x)

fun(10,2)
fun(10,0)
