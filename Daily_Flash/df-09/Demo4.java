import java.io.*;

class Demo4{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the First Numbers : ");
		int x = Integer.parseInt(br.readLine());
		
		System.out.println("Enter the Second Numbers : ");
		int y = Integer.parseInt(br.readLine());

		System.out.println("Enter the sign : ");
		char ch = (char)br.read();
		br.skip(1);

		switch(ch){
		
			case '+':
				System.out.println("The Addition is "+(x+y));
				break ;

			case '-' :
				System.out.println("The Subtraction is"+(x-y));
				break ;

			case '*' :
				System.out.println("The Multiplication is"+(x*y));
				break ;

			case '/' :
				System.out.println("The Division is"+(x/y));
				break ;

			default :
				System.out.println("Invalid Input");

		}
	}
}
