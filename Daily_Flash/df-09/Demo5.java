import java.io.*;

class Demo5{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Number of Rows = ");
		int x = Integer.parseInt(br.readLine());
		
		for(int i=1; i<=x; i++){
		
			for(int j=1; j<=i; j++){
			
				if(i%2==0){
					System.out.print(" $ ");
				}else {
					System.out.print(" * ");
				}
			}
			System.out.println();
		}
	}
}
