import java.io.*;

class Demo3{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Current(I) : ");
		int I = Integer.parseInt(br.readLine());
		
		System.out.println("Enter the Resistance(R) : ");
		int R = Integer.parseInt(br.readLine());

		System.out.println("Voltage 'V' = "+(I*R));
		
	}
}
