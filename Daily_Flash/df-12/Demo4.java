import java.io.*;

class Demo4{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Number of Rows : ");
		int x = Integer.parseInt(br.readLine());

		int a = 1 ;
		for(int i=1; i<=x ; i++){
		
			for(int j=1; j<=i; j++){
			
				System.out.print(" "+(a*a*a)+" ");
				a++;
			}
			System.out.println();
		}
	}
}
