import java.io.*;

class Demo2{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Numbers : ");
		int x = Integer.parseInt(br.readLine());
		int y = Integer.parseInt(br.readLine());

		int fact ; 

		for(int i=x; i<=y ; i++){
			fact = 1 ; 
			for(int j=i; j>=1; j--){
			
				fact = fact*j;
			}
			System.out.println("Factorial of "+i+" : "+fact);
		}
	}
}
