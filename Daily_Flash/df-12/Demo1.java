import java.io.*;

class Demo1{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Number : ");
		int x = Integer.parseInt(br.readLine());

		int sum = 0 ; 

		for(int i=1; i<=x/2 ; i++){
		
			if(x%i==0){
				
				sum = sum + i ;
			}
		}
		if(x==sum){
			System.out.println(x+" is a Perfect Number");
		}else{
			System.out.println(x+" is a Not Perfect Number");
		}
	}
}
