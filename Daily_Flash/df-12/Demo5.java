import java.io.*;

class Demo5{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the First Number : ");
		int x = Integer.parseInt(br.readLine());

		System.out.println("Enter the Second Number : ");
		int y = Integer.parseInt(br.readLine());

		System.out.println("Enter the Third Number : ");
		int z = Integer.parseInt(br.readLine());
		
		if(x<y && x<z){
			System.out.println("The Minimum number amongst "+x+" , "+y+" & "+z+" is : "+x);
		}else if(y<x && y<z){
			System.out.println("The Minimum number amongst "+x+" , "+y+" & "+z+" is : "+y);
		}else if(z<x && z<y){
			System.out.println("The Minimum number amongst "+x+" , "+y+" & "+z+" is : "+z);
		}else{
			System.out.println("Invalid Inputs");
		}
	}
}
