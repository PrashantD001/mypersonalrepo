
import java.util.*;

class Demo3{

	public static void main(String[] pn){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the Year = ");
		int x = sc.nextInt();

		if(x%4==0 && x%400==0){
		
			System.out.println(x+" is Leap year");
		}else{
		
			System.out.println(x+" is not a Leap Year");
		}
	}
}
