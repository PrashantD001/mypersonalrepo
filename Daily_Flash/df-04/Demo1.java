import java.io.*;

class Demo1{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Side 1 = ");
		int a = Integer.parseInt(br.readLine());

		System.out.println("Side 2 = ");
		int b = Integer.parseInt(br.readLine());

		System.out.println("Hypotenuse = ");
		int c = Integer.parseInt(br.readLine());

		if((c*c)==((a*a)+(b*b))){
		
			System.out.println("Triangle Satisfies the Pythagorean Theorem");

		}else{
		
			System.out.println("Triangle Does not Satisfies the Pythagorean Theorem");

		}

	}
}
