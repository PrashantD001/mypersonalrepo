
import java.util.*;

class Demo5{

	public static void main(String[] pn){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the Marks of Physics = ");
		int a = sc.nextInt();

		System.out.println("Enter the Marks of Chemistry = ");
		int b = sc.nextInt();

		System.out.println("Enter the Marks of Biology = ");
		int c = sc.nextInt();

		System.out.println("Enter the Marks of Mathematics = ");
		int d = sc.nextInt();

		System.out.println("Enter the Marks of Computer = ");
		int e = sc.nextInt();

		double per = (double)(a+b+c+d+e)/5;
		
		if(per>=90){
		
			System.out.println("Grade A");
		}else if(per>=80 && per<90){
		
			System.out.println("Grade B");
		}else if(per>=70 && per<80){
		
			System.out.println("Grade C");
		}else if(per>=60 && per<70){
		
			System.out.println("Grade D");
		}else if(per>=40 && per<60){
		
			System.out.println("Grade E");
		}else {
		
			System.out.println("Grade F");
		}

	}
}
