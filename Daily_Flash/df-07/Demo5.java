import java.io.*;

class Demo5{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Number of rows and column = ");
		int x = Integer.parseInt(br.readLine());			

		for(int i=1; i<=x; i++){

			for(int j=1; j<=x; j++){

				if(i%2==0){
				
					System.out.print(" 1 ");

				}else{
				
					System.out.print(" 0 ");
				}
			
			}
			System.out.println();
		}
	}
}
