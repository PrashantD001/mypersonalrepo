import java.io.*;

class Demo2{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Number = ");
		int x = Integer.parseInt(br.readLine());			// Taking user Input

		int sum = 0;

		for(int i=1; i<=x; i++){

			sum = sum +i;
		}

		System.out.println("The sum is = "+sum);
	}
}
