import java.io.*;

class Demo3{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Integer Number = ");
		int x = Integer.parseInt(br.readLine());			// Taking user Input

		if(x%2==0){
		
			System.out.println(x+" is a Even number");

		}else{

			System.out.println(x+" is a Odd number");

		}
	}
}
