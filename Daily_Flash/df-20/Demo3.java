import java.io.*;

class Demo3{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int[] arr = new int[10];

		for(int i=0; i<10; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i] <0){

				System.out.println("Negative Number Entered , Exiting the loop ! ");
				break;
			}
		}
	}
}
