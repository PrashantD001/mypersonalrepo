import java.io.*;

class Demo5{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Number of Values You Entering : ");
		int temp = Integer.parseInt(br.readLine());

		int[] arr = new int[temp];

		System.out.println("Enter the Values : ");
		for(int i=0; i<temp; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
			if(i!=0){
				if(arr[i] <arr[i-1]){
	
					System.out.println("Loop is Exited with Input "+arr[i]);
					break;
				}
			}
		}
	}
}
