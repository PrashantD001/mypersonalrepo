import java.io.*;

class Demo5{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


		System.out.println("Enter the First Number : ");
		int x = Integer.parseInt(br.readLine());

		System.out.println("Enter the Second Number : ");
		int y = Integer.parseInt(br.readLine());

		int a ;
		int flag =0;
		int temp =0;

		if(x>=y){
			a=x;
		}else{
			a=y;
		}

		for(int i=1 ; i<=a; i++){
		
			if(x%i==0 && y%i==0){
			
				temp  = i;
				flag = 1;
			}
		}
		if(flag==1){
			System.out.println("The GCD of "+x+" & "+y+" is : "+temp);
		}else{
			System.out.println("There is No GCD for Given Inputes ");
		}
	}
}
