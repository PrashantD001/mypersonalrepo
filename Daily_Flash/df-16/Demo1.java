import java.io.*;

class Demo1{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


		System.out.println("Enter the Number : ");
		int x = Integer.parseInt(br.readLine());

		for(int i=x ; i>=1; i--){
		
			if(i%2!=0){
			
				System.out.print(" , "+i);
			}
		}
	}
}
