import java.util.*;

class Demo4{

	public static void main(String[] pn){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the Number of Rows and Column : ");
		int x = sc.nextInt();
		
		int p = 1 ; 
		
		for(int i=1 ; i<=x ; i++ ){
		
			for(int j=1 ; j<=i ; j++){
			
				System.out.print(" "+(p*p)+" ");

				p++;

			}
			System.out.println();
		}
	}

}
