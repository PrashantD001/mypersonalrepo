import java.util.*;

class Demo3{

	public static void main(String[] pn){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the Distance : ");
		int x = sc.nextInt();
		
		System.out.println("Enter the Time : ");
		int y = sc.nextInt();

		double v = (double) (x/y);

		System.out.println("The Velocity of a Particle roaming in space is "+v+" m/sec ");
	
	}

}
