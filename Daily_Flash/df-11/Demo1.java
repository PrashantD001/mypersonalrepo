import java.util.*;

class Demo1{

	public static void main(String[] pn){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Min of Series : ");
		int x = sc.nextInt();
		
		System.out.println("Max of Series : ");
		int y = sc.nextInt();

		System.out.println("Series of even  Numbers Ranging Between  "+x+" & "+y+" is : ");
		
		for(int i = x ; i <= y ; i++ ){
		
			if(i%2==0){
			
				System.out.println(i);

			}
		}
	}

}
