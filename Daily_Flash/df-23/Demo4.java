import java.io.*;

class Demo4{

	public static void main(String[] pn)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Number Of Rows : ");
		int num = Integer.parseInt(br.readLine());

		for(int row=1 ; row<=num; row++){
		
			for(int col=1 , p=3; col<=num ;  col++ ){
			
				if(col==(num-row)+1){
				
					System.out.print("3 ");
				}else if( col<=(num-row)){
				
					System.out.print("  ");
				}else{
				
					p++;
					System.out.print((p*(col-1))+" ");
				}
			}
			System.out.println();
		}
	}
}
