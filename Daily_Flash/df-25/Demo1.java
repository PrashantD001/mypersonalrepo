import java.io.*;

class Demo1{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new  InputStreamReader(System.in));
		
		System.out.println("Enter the Limit : ");
		int x = Integer.parseInt(br.readLine());

		System.out.println(" The Deficient Numbers are : ");

		for(int num =1 ; num<=x ; num++){
			int sum = 0;
			int temp = 0;
			for(int  i=1 ; i<=num/2 ; i++){
		
				if(num%i==0){
					sum=sum+i;
				}
			}

			if(sum<num){

				System.out.println(num+" , ");

			}
		}
	}
}
