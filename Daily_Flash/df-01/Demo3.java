import java.io.*;

class Demo3{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Numbers = ");
		int x = Integer.parseInt(br.readLine());

		if(x>0){

			if(x%2==0){
				System.out.println(x+" is Even Number");
			}
			else{
				System.out.println(x+" is Odd Number");
			}

		}else{

			System.out.println(x+" is Negative or Zero ");

		}

	}
}
