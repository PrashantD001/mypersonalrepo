import java.io.*;

class Demo2{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Numbers = ");
		int x = Integer.parseInt(br.readLine());

		if(x>0){

			System.out.println(x+" is the Positive number");

		}else if(0>x){

			System.out.println(x+" is the Negative number");

		}else{

			System.out.println(x+" is Equal to Zero ");

		}

	}
}
