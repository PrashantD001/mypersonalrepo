import java.io.*;

class Demo3{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new  InputStreamReader(System.in));

		System.out.println("Enter the Number : ");
		int i = Integer.parseInt(br.readLine());

		int mul =1;

		while(i!=0){
		
			int rem = i%10;
			i=i/10;

			if(rem%2==0){
			
				mul =mul*rem;
			}
		}

		System.out.println("The Multiplication of all Even Digits is : "+mul);
	}
}
