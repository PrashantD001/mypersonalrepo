import java.util.*;

class Demo5{

	public static void main(String[] pn){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the Number : ");
		int a = sc.nextInt();

		System.out.println("The Prime Digits From The Number "+a+" is :  ");

		while(a!=0){
		
			int num = a%10;
			a= a/10;
			int count =1;

			for(int i=2 ; i<=num/2; i++){

				if(num%i==0){
					count++;
				}
			}
			if(num!=1){
			
				if(count<2){
				
					System.out.print(num+" , ");
				}
			}else{
				System.out.print(num+" , ");
			}
		}
		
	}
}
