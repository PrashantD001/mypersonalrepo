import java.io.*;

class Demo4{

	public static void main(String[] pn)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Number Of Rows : ");
		int num = Integer.parseInt(br.readLine());

		int sum=0;

		for(int row=1 ; row<=num; row++){

			int temp = num - row +1 ;
			
			for(int col1=0 ; col1<row ;  col1++ ){
			
				System.out.print("	");
			}
			
			for(int col2=num ; col2>=row ;  col2-- ){
			
				System.out.print(temp+"	");
				temp--;		
			}
			System.out.println();
		}
	}
}
