import java.io.*;

class Demo3{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new  InputStreamReader(System.in));

		System.out.println("Enter the Number : ");
		int i = Integer.parseInt(br.readLine());

		while(i!=0){
		
			int rem = i%10;
			i=i/10;

			System.out.println("The Square Of "+rem+" is : "+rem*rem);
		}
	}
}
