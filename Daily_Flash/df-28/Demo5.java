import java.util.*;

class Demo5{

	public static void main(String[] pn){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the Number : ");
		int a = sc.nextInt();

		while(a!=0){
		
			int num = a%10;
			a= a/10;
			int fact =1;

			for(int i=num ; i>0; i--){
			
				fact=fact*i;
			}

			System.out.println("The Factorial of "+num+" is :  "+fact);

		}
		
	}
}
