import java.io.*;

class Demo4{

	public static void main(String[] pn)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Number Of Rows : ");
		int num = Integer.parseInt(br.readLine());

		int sum=0;

		for(int i=num; i>0; i--){
			sum =sum+i;
		}

		for(int row=1 ; row<=num; row++){
			
			for(int col1=num ; col1>row ;  col1-- ){
			
				System.out.print("	");
			}
			
			for(int col2=1 ; col2<=row ;  col2++ ){
			
				System.out.print(sum*sum+"	");
				sum--;		
			}
			System.out.println();
		}
	}
}
