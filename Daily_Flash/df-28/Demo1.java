import java.io.*;

class Demo1{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new  InputStreamReader(System.in));
		
		System.out.println("Enter the Number : ");
		int num = Integer.parseInt(br.readLine());

		System.out.println("The Prime Numbers Are : ");

		while(num!=0){

			int count =0;
		
			int rem =num%10;
			num=num/10;

			if(rem!=1){
				for(int i=2; i<=rem/2 ; i++){
				
					if(rem%i==0){
						count++;
					}
				}
				if(count==0){
					System.out.print(rem+" , ");
				}
			}else{
			
				System.out.print(rem+" , ");
			}

		}
	}
}
