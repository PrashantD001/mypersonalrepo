import java.io.*;

class Demo4{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Number of Rows : ");
		int num = Integer.parseInt(br.readLine());

		char ch2 = 65;
		char ch;

		for(int i=0 ; i<=num; i++){
		
			ch=(char)(ch2+num-i);
			for(int k=0; k<num-i ; k++){
			
				System.out.print("  ");
			}	
			for(int j=1; j<=i ; j++){
			
				System.out.print(" "+ch);
				ch++;
			}
			System.out.println();
		}
	}
}
