import java.io.*;

class Demo5{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Number of Rows : ");
		int num = Integer.parseInt(br.readLine());

		for(int i=1 ; i<=num; i++){
	
			for(int j=1; j<=num ; j++){
			
				if(i==j){
					System.out.print(" =");
				}else{
					System.out.print(" "+j);
				}
			}
			System.out.println();
		}
	}
}
