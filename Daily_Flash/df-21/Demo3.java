import java.io.*;

class Demo3{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Limit : ");
		int num  = Integer.parseInt(br.readLine());

		for(int i=1; i<=num; i++){
			
			if(i%7!=0){

				System.out.print(i+" ,");
			}
		}
	}
}
