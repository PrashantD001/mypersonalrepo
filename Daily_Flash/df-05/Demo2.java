import java.io.*;

class Demo2{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the total Unit = ");
		double x = Double.parseDouble(br.readLine());	

		double sum =0 ;
		double temp = x;

		if(x>=250){
			temp = x-250;
			sum = sum + (temp*1.5);
			x=250;

		}
		if(x>=150){
			temp = x - 150;
			sum = sum + (temp*1.2);
			x=150;
		}
		if(x>=100){
			temp = x - 50;
			sum = sum + (temp*0.75);
			x=50;
		}
		if(x>=0){
			temp = x;
			sum = sum + (temp*0.50);
		}
		
		System.out.println("Total Electricity bill is = "+sum);
			
	}
}
