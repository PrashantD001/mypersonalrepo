import java.io.*;

class Demo1{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Radius = ");
		int r = Integer.parseInt(br.readLine());

		double p = 3.14;

		double area = (double)p*r*r;

		System.out.println("The area of Circle is = " +area);
	}
}
