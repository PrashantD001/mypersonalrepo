import java.io.*;

class Demo3{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the 'M' for male and 'F' for female =");
		char ch = (char)br.read();
		br.skip(1);
		
		System.out.println("Enter the martial status 'Y' for married 'N'for unmarried =");
		char ch2 = (char)br.read();
		br.skip(1);

		if(ch==109 || ch==77){
		
			System.out.println("Enter the Age = ");
			int x = Integer.parseInt(br.readLine());

			if(x<40 && x>=20){
				System.out.println("He can Work in Anywhere");
			}else if(x<=60 && x>=40){
				System.out.println("He can Work in Urben Areas only");
			}else{
				System.out.println(x+" ERROR");
			}
		}else if(ch==102 || ch==70){
			
			System.out.println("She will work in Urban Areas only");
		}else{
		
			System.out.println("Invalid Choice");
		}
	}
}
