import java.util.*;

class Demo4{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the no. of Rows and Column = ");
		int x = sc.nextInt();

		int temp;
		for(int i=1; i<=x; i++){
				
			temp=2;
			for(int j=1; j<=x; j++){
			
				System.out.print(" "+temp+" ");	
				temp = temp + 2;

			}
			System.out.println();

		}
	}
}
