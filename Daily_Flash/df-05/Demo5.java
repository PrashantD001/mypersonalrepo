import java.io.*;

class Demo5{

	public static void main(String[] args)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Character = ");
		char ch = (char)br.read();
		br.skip(1);

		int x = ch;
		System.out.println(x);
	}
}
