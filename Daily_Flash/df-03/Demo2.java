import java.util.*;			// importing util package for Scanner class

class Demo2{

	public static void main(String[] pn){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the Number from 0 to 6 = ");
		int x = sc.nextInt();


		switch(x){					// Expression which gives only Integer or Character value
		
			case 0:
				System.out.println("Monday");
				break;

			case 1:
				System.out.println("Tuesday");
				break;

			case 2:
				System.out.println("Wednesday");
				break;

			case 3:
				System.out.println("Thursday");
				break;

			case 4:
				System.out.println("Friday");
				break;

			case 5:
				System.out.println("Saturday");
				break;

			case 6:
				System.out.println("Sunday");
				break;

			default :				// if no case matches then it goes to Deafault
				System.out.println("Invalid Input");
				break;
		}
	}
}
