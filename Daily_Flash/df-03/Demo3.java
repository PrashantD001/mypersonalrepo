import java.util.*;

class Demo3{

	public static void main(String[] pn){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the No.  of month = ");
		int x = sc.nextInt();

		switch(x){
		
			case 1:
				System.out.println("January is a 31 Day's month");
				break;

			case 2:
				System.out.println("February is a 28/29 Day's month");
				break;

			case 3:
				System.out.println("March is a 31 Day's month");
				break;

			case 4:
				System.out.println("April is a 30 Day's month");
				break;

			case 5:
				System.out.println("May is a 31 Day's month");
				break;

			case 6:
				System.out.println("June is a 30 Day's month");
				break;

			case 7:
				System.out.println("July is a 31 Day's month");
				break;

			case 8:
				System.out.println("August is a 31 Day's month");
				break;

			case 9:
				System.out.println("September is a 30 Day's month");
				break;

			case 10:
				System.out.println("October is a 31 Day's month");
				break;

			case 11:
				System.out.println("November is 30 Day's month");
				break;

			case 12:
				System.out.println("December is a 31 Day's month");
				break;

			default :
				System.out.println("Invalid Input");
		}
	}
}
