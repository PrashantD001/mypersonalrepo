import java.io.*;				// Importing io package for user Input

class Demo1{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Charactr = ");
		char ch = (char)br.read();
		br.skip(1);					

		if(ch>=97 && ch<=122){				// comparing input ascii value for Lowercase
		
			System.out.println("Letter "+ch+" is in LowerCase");

		}else if(ch>=65 && ch<=90){			// Comparing Input ascii value for Uppercase
		
			System.out.println("Letter "+ch+" is in UpperCase");

		}else{
		
			System.out.println("Given input is not an Character");

		}
	}
}
