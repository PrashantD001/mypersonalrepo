import java.util.*;

class Demo3{

	public static void main(String[] pn){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter The First Number : ");
		int x = sc.nextInt();

		System.out.println("Enter The Last Number : ");
		int y = sc.nextInt();

		for(int i=x; i<=y; i++){
		
			if(i%2!=0){
			
				System.out.print(i+" , ");
			}
		}
	}
}
