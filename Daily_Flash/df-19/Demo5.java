import java.util.*;

class Demo5{

	public static void main(String[] pn){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("EnterThe Number of Rows : ");
		int num = sc.nextInt();

		for(int i=1; i<=num ; i++){

			int temp = num-(i-1);

			for(int j=num-i; j>0 ; j--){
			
				System.out.print("   ");
			}

			for(int k =1 ; k<=i ; k++){
				System.out.print(" "+temp+" ");
				temp++;
			}
			System.out.println();
		}
		
	}
}
