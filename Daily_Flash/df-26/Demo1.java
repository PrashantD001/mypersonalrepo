import java.io.*;

class Demo1{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new  InputStreamReader(System.in));
		
		System.out.println("Enter the Number : ");
		int x = Integer.parseInt(br.readLine());

		int sum =0;
		int temp=x;

		while(x!=0){
		
			int rem = x%10;
			x=x/10;

			int mul =1 ;

			for(int i=rem ; i>0 ; i--){
			
				mul = mul * i ;
			}
			sum=sum+mul;
		}

		if(sum==temp){
			System.out.println(temp+" is a Strong Number .");
		}else{
			System.out.println(temp+" is Not a Strong Number .");
		}
	}
}
