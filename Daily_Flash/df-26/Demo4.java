import java.io.*;

class Demo4{

	public static void main(String[] pn)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Number Of Rows : ");
		int num = Integer.parseInt(br.readLine());

		char ch ='A' ;
		ch--;

		for(int row=1 ; row<=num; row++){
			ch = (char)(ch+row);
			for(int col1=num ; col1>row ;  col1-- ){
			
				System.out.print("   ");
			}
			int temp =3;
			for(int col2=1 ; col2<=row ;  col2++ ){
			
				System.out.print(ch+""+temp+" ");
				ch--;
				temp++;
			}
			System.out.println();
		}
	}
}
