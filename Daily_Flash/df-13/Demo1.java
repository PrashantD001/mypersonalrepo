
import java.io.*;

class Demo1{

	public static void main(String[] pn) throws IOException{
	
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		System.out.println("Enter the Number : ");
		int n = Integer.parseInt(br.readLine());

		for(int i=10 ; i>=1 ; i--){
		
			System.out.println(n+" * "+i+" = "+(i*n));
		}
	}
}
