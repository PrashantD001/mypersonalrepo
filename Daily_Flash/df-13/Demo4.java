
import java.io.*;

class Demo4{

	public static void main(String[] pn) throws IOException{
	
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		System.out.println("Enter the Number of Rows : ");
		int n = Integer.parseInt(br.readLine());

		int temp = n ;

		for(int i=1 ; i<=n ; i++){
		
			for(int j=0 ; j<i ; j++){
			
				System.out.print(" "+(temp-i+j)+" ");
			}
			System.out.println();
		}
	}
}
