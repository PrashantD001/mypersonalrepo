
import java.io.*;

class Demo2{

	public static void main(String[] pn) throws IOException{
	
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		System.out.println("Enter the Number : ");
		int n = Integer.parseInt(br.readLine());

		System.out.println("Second Predecessor : "+(n-2));
		
		System.out.println("Second  Successor : "+(n+2));
		
	}
}
