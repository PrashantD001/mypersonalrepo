import java.io.*;

class Demo4{

	public static void main(String[] pn)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Number Of Rows : ");
		int num = Integer.parseInt(br.readLine());

		for(int row=1 , p=1 ; row<=num; row++){
		
			for(int col=1 , count=0, sum=0 ; col<=num ;  col++ ){
			
				if(col==(num-row)+1){
				
					System.out.print("= ");
				}else if( col<=(num-row)){
				
					System.out.print("  ");
				}else{
				
					p = p + (count*count);
					System.out.print(p+" ");
					sum=sum+p;
					count++;
					if(col==num){
					
						p=sum;
					}
				}
			}
			System.out.println();
		}
	}
}
