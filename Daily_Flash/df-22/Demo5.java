import java.util.*;

class Demo5{

	public static void main(String[] pn){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the Length of Array : ");
		int a = sc.nextInt();

		int[] arr = new int[a];

		System.out.println("Enter the Elements In Array : ");

		for(int i=0; i<a; i++){
			
			arr[i] = sc.nextInt();
		}

		System.out.println("Before : ");

		for(int i=0 ; i<a; i++){
			System.out.print(arr[i]+" , ");
		}
		
		for(int x=0 , y=a-1 ; x<=a/2; x++){
		
			int temp = arr[x];
			arr[x] = arr[y];
			arr[y] = temp; 
			y--;
		}

		System.out.println("After : ");

		for(int i=0 ; i<a; i++){
			System.out.print(arr[i]+" , ");
		}
	}
}
