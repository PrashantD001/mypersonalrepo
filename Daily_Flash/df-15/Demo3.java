import java.util.*;

class Demo3{

	public static void main(String[] pn){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the Number of Rows : ");
		int x = sc.nextInt();

		char ch ='A';

		for(int i=1; i<=x ; i++){
			
			for(int j=i; j<=x; j++){
				System.out.print(" "+ch+" ");
			}
			System.out.println();
			ch++;
		}

	}
}

