import java.io.*;

class Demo1{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new  InputStreamReader(System.in));
		
		System.out.println("Enter the Ending Number : ");
		int end = Integer.parseInt(br.readLine());

		for(int x1=1; x1<=end ; x1++){
			int sum =0;
			int x=x1;
			while(x!=0){
		
				int rem = x%10;
				x=x/10;

				int mul =1 ;

				for(int i=rem ; i>0 ; i--){
			
					mul = mul * i ;
				}
				sum=sum+mul;
			}

			if(sum==x1){
			System.out.print(x1+" , ");
			}
		}
	}
}
