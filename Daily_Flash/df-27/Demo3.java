import java.io.*;

class Demo3{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new  InputStreamReader(System.in));

		System.out.println("Enter the Number : ");
		int i = Integer.parseInt(br.readLine());

		float sum =0;
		int num = i;
		float count =0;


		while(i!=0){
		
			int rem = i%10;
			i=i/10;

			sum=sum+rem;
			count++;
		}

		System.out.println("The Average of  Digits From Number "+num+" is : "+(float)(sum/count));

	}
}
