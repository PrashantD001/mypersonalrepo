import java.io.*;

class Demo2{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Mass : ");
		double m = Double.parseDouble(br.readLine());
		System.out.println("Enter the Velocity : ");
		double v = Double.parseDouble(br.readLine());

		double KE = (0.5)*v*m*v;


		System.out.println("Kinetic Energy of that Object is  : "+KE);

	}
}
