import java.io.*;

class Demo1{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the 'A' : ");
		int a = Integer.parseInt(br.readLine());
		System.out.println("Enter the 'B' : ");
		int b = Integer.parseInt(br.readLine());

		System.out.println("Before Swap : " );
		System.out.println("A : "+a+  " B : "+b );

		a=a+b;
		b=a-b;
		a=a-b;
		
		System.out.println("After Swap : " );
		System.out.println("A : "+a+  " B : "+b );
	}
}
