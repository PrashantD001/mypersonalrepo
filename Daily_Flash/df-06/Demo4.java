import java.io.*;

class Demo4{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Number of even no. you want = ");
		int x = Integer.parseInt(br.readLine());

		int count = 1;
		for(int i=2; count<=x; i=i+2){
		
			System.out.println(i);
			count++;
		}
	}
}
