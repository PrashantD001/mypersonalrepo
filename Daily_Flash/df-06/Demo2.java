import java.io.*;

class Demo2{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Number you want to sum = ");
		int x = Integer.parseInt(br.readLine());

		int sum = 0 ;

		for(int i=1; i<=x; i++){

			sum = sum + i;
		}

		System.out.println(sum);
	}
}
