import java.io.*;						// import package for input using BufferedReader

class Demo1{

	public static void  main(String[] pn)throws IOException{		// throws exception 
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));		// making Object

		System.out.println("Enter the Character = ");
		char c = (char)br.read();					// taking Input character
		br.skip(1);

		if((c>=97 && c<=122) || (c>=65 && c<=90)){			// if condition to confirm that given data is alphabae or not
		
			System.out.println(c+" is an alphabet");
		}else{
		
			System.out.println(c+" is not alphabet");
		}
	}
}
