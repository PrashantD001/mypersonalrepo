import java.io.*;					// import package for user Input

class Demo5{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Three Numbers = ");		// taking user input for three integer
		int a = Integer.parseInt(br.readLine());
		int b = Integer.parseInt(br.readLine());
		int c = Integer.parseInt(br.readLine());

		if((a>b)&&(a>c)){						// checking a is greater than b,c

			System.out.println(a+" is max number among "+a+","+b+"&"+c);

		}else if((b>c)&&(b>a)){						// checking b is greater than a,c

			System.out.println(b+" is max number among "+a+","+b+"&"+c);

		}else if((c>b)&&(c>a)){						// checking c is greater than a,b

			System.out.println(c+" is max number among "+a+","+b+"&"+c);

		}else{								// if there are same numbers
		
			System.out.println("There are Similar Numberes");
		}
	}
}
