import java.io.*;					// importing package

class Demo2{

	public static void  main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));			// creating object for taking input from user

		System.out.println("Enter the Character = ");
		char c = (char)br.read();
		br.skip(1);

		if(c==97 || c==101 || c==105 || c==111 || c==118 || c==65 || c==69 || c==73 || c==79 || c==85){		// condition to check given char is vowel or not
		
			System.out.println(c+" is an Vowel");
		}else{
		
			System.out.println(c+" is not Vowel");
		}
	}
}
