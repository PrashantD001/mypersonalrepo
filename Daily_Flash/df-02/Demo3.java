import java.io.*;			// importing package beacause BufferedReader is in io package

class Demo3{

	public static void  main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));		// Object to take input from user

		System.out.println("Enter the Character = ");
		char c = (char)br.read();
		br.skip(1);										// it is IMP

		if((c>=97 && c<=122) || (c>=65 && c<=90)){						// condition to check it is alphabate or Special Character
		
			System.out.println(c+" is not a special character");
		}else{
		
			System.out.println(c+" is a special character");
		}
	}
}
