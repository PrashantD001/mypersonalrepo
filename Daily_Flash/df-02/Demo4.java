import java.io.*;			// imorting io package due to BufferedReader

class Demo4{

	public static void  main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the no. of rows = ");		// taking no. of rows
		int num = Integer.parseInt(br.readLine());		// storing input in int num

		System.out.println("Enter the Integer number that you want to print = ");	// taking value which will be printed in pattern
		int x = Integer.parseInt(br.readLine());
		
		for(int i=1; i<=num; i++){				// for row
		
			for(int j=0; j<=num; j++){			// for coloumn
			
				System.out.print(x+" ");
			}
			System.out.println();				// for new line
		}
	}
}
