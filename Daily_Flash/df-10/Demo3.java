import java.io.*;

class Demo3{

	public static void main(String[] pn)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the number : ");
	
		int y = Integer.parseInt(br.readLine());

		for(int i = 1 ; i<=y ; i++){
		
			if(i%2==0){
				System.out.println("Cube of "+i+" : "+(i*i*i)+" And Square of "+i+"  : "+(i*i));
			}
		}
	
	}
}
