import java.io.*;

class Demo4{

	public static void main(String[] pn)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the number of Rows : ");
		int y = Integer.parseInt(br.readLine());

		for(int i = 1 ; i<=y ; i++){
		
			for(int j = 1 ; j<=i ; j++){
			
				if(i%2==0){
				
					System.out.print(" A ");
				}else{

					if(j==1 && (i+j)>2){
					
						System.out.print(" A ");
					}else{
					
						System.out.print(" a ");
					}
				}
			
			}
			System.out.println();
		}
	
	}
}
