import java.io.*;

class Demo3{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new  InputStreamReader(System.in));

		System.out.println("Enter the Number : ");
		int num = Integer.parseInt(br.readLine());

		int i =0;

		if(num%2==0){
			i = num;
		}else{
			i = num-1;
		}

		while(i>=0){
		
			System.out.print(i+" ,");
			i=i-2;
		}

	}
}
