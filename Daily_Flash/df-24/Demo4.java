import java.io.*;

class Demo4{

	public static void main(String[] pn)throws IOException {
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Number Of Rows : ");
		int num = Integer.parseInt(br.readLine());

		char ch='A';

		for(int row=1 ; row<=num; row++){
		
			ch = (char)(ch+(row-1));

			for(int col=1 ; col<=num ;  col++ ){
			
				if( col<=(num-row)){
				
					System.out.print("  ");
				}else{
				
					System.out.print(ch+" ");
					ch--;
				}
			}
			System.out.println();
			ch='A';
		}
	}
}
