import java.io.*;

class Demo1{

	public static void main(String[] pn)throws IOException{
	
		BufferedReader br = new BufferedReader(new  InputStreamReader(System.in));

		System.out.println("Enter the Number : ");
		int num = Integer.parseInt(br.readLine());

		int sum = 0;
		int temp = 0;

		for(int  i=1 ; i<=num/2 ; i++){
		
			if(num%i==0){
				sum=sum+i;
			}
		}

		if(sum<num){

			System.out.println(num+" Is an Deficient Number .");

		}else if(sum==num){
		
			System.out.println(num+" Is Perfect Number");

		}else{
		
			System.out.println(num+" Is Not an Deficient Number .");

		}
	}
}
