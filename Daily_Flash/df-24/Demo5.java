import java.util.*;

class Demo5{

	public static void main(String[] pn){
	
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the Number : ");
		int a = sc.nextInt();

		int count =0; 
		int temp=a;

		while(a!=0){
		
			int num = a%10;
			a= a/10;
			if(num%2==0){
				count++;
			}
		}

		System.out.println("The Number "+temp+" has "+count+" Even Digits ");
		
	}
}
