class Parent {
    protected String variable;

    public Parent() {
        variable = "Parent Variable";
    }
}

class Child extends Parent {
    protected String variable;

    public Child() {
        super();
        variable = "Child Variable";
    }

    public void printVariables() {
        System.out.println("Parent variable: " + super.variable);
        System.out.println("Child variable: " + variable);
    }
}

class GrandChildren extends Child {
    protected String variable;

    public GrandChildren() {
        super();
        variable = "GrandChild Variable";
    }

    public void printVariables() {
        System.out.println("Parent variable: " + super.super.variable);
        System.out.println("Child variable: " + super.variable);
        System.out.println("GrandChild variable: " + variable);
    }
}

class Demo {
    public static void main(String[] args) {
        GrandChildren grandchild = new GrandChildren();
        grandchild.printVariables();
    }
}

