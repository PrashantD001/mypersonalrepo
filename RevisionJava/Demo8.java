

class Demo{


	static int A = 150;

	void fun(Demo obj2 ){
		
		System.out.println("In Fun Obj2 : "+System.identityHashCode(obj2));
		System.out.println("In Fun Obj2.A : "+obj2.A+" : "+System.identityHashCode(obj2.A));
		
		Demo obj = new Demo();
		
		System.out.println("In Fun Obj.A : "+obj.A+" : "+System.identityHashCode(obj.A));
		System.out.println("In Demo.A : "+Demo.A+" : "+System.identityHashCode(Demo.A));
		System.out.println("In Fun A : "+A+" : "+System.identityHashCode(A));
	
	
	}

	public static void main(String[] pn){
	
		int[] arr = {100,150,200};

		System.out.println("In Main 1 : 150 "+System.identityHashCode(arr[2]));
		System.out.println("In Main 1 : 150 "+System.identityHashCode(arr[2]));

		Demo obj = new Demo();
		
		System.out.println("In Obj.A : "+obj.A+" : "+System.identityHashCode(obj.A));
		System.out.println("In Demo.A : "+Demo.A+" : "+System.identityHashCode(Demo.A));
		System.out.println("In A : "+A+" : "+System.identityHashCode(A));

		if(obj == obj){
	
			System.out.println("In Obj 1 : "+System.identityHashCode(obj));
			System.out.println("In Obj 2 : "+System.identityHashCode(obj));
			System.out.println("Same Addressses .....! ");
		}
		obj.fun(obj);
/*
		for(int i =0 ;i<3 ; i++){
		
			System.out.println("In Main 1 : "+System.identityHashCode(arr[i]));
		}
		
		obj.fun(arr);
		
		for(int i =0 ;i<3 ; i++){
		
			System.out.println("In Main 2 : "+System.identityHashCode(arr[i]));
		}
*/
	}
}
