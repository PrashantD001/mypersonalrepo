
class Parent{

	int x = 10;
	static int y = 20 ;

}

class Child extends Parent{

	int x = 30 ;
	static int y = 40 ;
}

class GrandChild extends Parent {

	int x = 50 ;
	static int y = 60 ;

	void fun(){
	
		System.out.println("GrandChildren : x : "+x);
		System.out.println("Child : x : "+super.x);
		System.out.println("Parent : x : "+super.super.x);
	}

	public static void main(String[] pn){
	
		GrandChildren obj = new GrandChildren();

		obj.fun();
	}
}
