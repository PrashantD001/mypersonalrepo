class Demo {

    public static void main(String[] args) {
        
        char arr[] = {'A','B','C','D'};

        String str = "ABCD";

        System.out.println(arr);		// ABCD
        System.out.println(str);		// ABCD
        System.out.println("Arr : "+arr);		//[C@251a69d7
        System.out.println("Str : "+str);		// ABCD

        System.out.println("Arr Identity HashCode: "+System.identityHashCode(arr));
        System.out.println("str  Identity HashCode : "+System.identityHashCode(str));

        System.out.println("arr[0] Identity "+System.identityHashCode(arr[0]));
        System.out.println("str chatAt[0] Identity "+System.identityHashCode(str.charAt(0)));



    }
}

