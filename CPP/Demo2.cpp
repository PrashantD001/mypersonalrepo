
#include<iostream>

using namespace std;

class Demo{


	public :
		Demo(){
			Demo obj5(10);
			Demo obj6(obj5);
			cout<<"No-Args : "<<this<<endl;
		}
		/*{
		
			cout<<"Instance Block "<<endl;
		}*/
		Demo(int x){
		
			cout<<"Para : "<<this<<endl;
		}
		Demo(Demo &xyz){
		
			cout<<"Copy : "<<this<<endl;
		}
};


int main(){

	Demo obj ;

	return 0;
}


