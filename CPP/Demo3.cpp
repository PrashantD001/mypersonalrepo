

#include<iostream>

class Demo{

	public :

		int x = 10 ;

		Demo(){
			std::cout<<"No-Args"<<std::endl;
		}
		Demo(int x ){
			this->x = x ;
			std::cout<<"Para"<<std::endl;
		}
		Demo(Demo &ref){
			std::cout<<"Copy "<<std::endl;
			std::cout<<"X : "<< x <<std::endl;
		}
};

int main(){

	Demo obj1(50);

	Demo obj2 = obj1;

	return 0;
}
