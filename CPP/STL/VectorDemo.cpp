
#include<iostream>
#include<vector>

void printVector(std::vector<int> &v){

	std::vector<int>::iterator itr1 ;
	for(itr1 = v.begin(); itr1!= v.end(); itr1++){
		std::cout<< *itr1 << " , ";
	}
	std::cout<< std::endl;
}

int main(){

	std::vector<int> v = {10,20,30,40,50};


	// begin() and end()	
	std::cout << "begin() and end()" << std::endl;
	std::vector<int>::iterator itr1 ;
	for(itr1 = v.begin(); itr1!= v.end(); itr1++){
		std::cout<< *itr1 << std::endl;
	}
	std::cout<< std::endl;
	
	// rbegin() and rend()	
	std::cout << "rbegin() and rend()" << std::endl;
	std::vector<int>::reverse_iterator itr2 ;
	for(itr2 = v.rbegin(); itr2!= v.rend(); itr2++){
		std::cout<< *itr2 << std::endl;
	}
	std::cout<< std::endl;

	// cbegin() and cend()	
	std::cout << "cbegin() and cend()" << std::endl;
	std::vector<int>::const_iterator itr3 ;
	for( itr3 = v.cbegin(); itr3!= v.cend(); itr3++){
		std::cout<< *itr3 << std::endl;
	}
	std::cout<< std::endl;

	// crbegin() and crend()	
	std::cout << "crbegin() and crend()" << std::endl;
	std::vector<int>::const_reverse_iterator itr4 ;
	for(itr4 = v.crbegin(); itr4!= v.crend(); itr4++){
		std::cout<< *itr4 << std::endl;
	}
	std::cout<< std::endl;

	//----------------------------------Capacity----------------------------------

	// size() - returns size of vector
	std::cout << "size() : " << v.size() << std::endl;
	
	// max_size() - return maximum number of elements in vector 
	std::cout << "max_size() : " << v.max_size() << std::endl;

	// resize(int) - changes the size of vector
	//std::cout << "resize(int) : " << v.resize(1000) << std::endl;
	std::cout << "resize(int) : " ; 
	v.resize(1000);
	std::cout << "size() : " << v.size() << std::endl;

	// capacity() - give the capacity of vector
	std::cout << "capacity() : " << v.capacity() << std::endl;

	// empty() - check the vector is empty or not
	std::cout << "empty() : " << v.empty() << std::endl;

	// reserve(int) - Request a change in capacity
	std::cout << "reserve(int) : " ;
	v.reserve(500) ;
	std::cout << "size() : " << v.size() << std::endl;

	// shrink_to_fit()
	std::cout << "shrink_to_fit() : " ;
	v.shrink_to_fit();
	std::cout << "size() : " << v.size() << std::endl;

	//----------------------------Element Access-----------------------------------------
	
	//operator[int] - give element at given position
	std::cout<< "operator[int] : " << v[0] <<std::endl;
	
	//at(int) - give element at given position
	std::cout<< "at(int) : " << v.at(0) <<std::endl;
	
	//front() -  give First element
	std::cout<< "front() : " << v.front() <<std::endl;
	
	//back() - Returns a reference to the last element in the vector.
	std::cout<< "back() : " << v.back() <<std::endl;
	
	//data() - Returns a direct pointer to the memory array used internally by the vector to store its owned elements.
	std::cout<< "data() : " << v.data() <<std::endl;

	//---------------------------Modifiers-----------------------------------------------

	//assign
	std::vector<int> v1 ;
	v1.assign (7,100);             // 7 ints with a value of 100	
	std::cout << "assign() : " << std::endl;
	printVector(v1);
	
	//push_back
	std::cout << "push_back(int) : "<< std::endl;
	v1.push_back(50);
	printVector(v1);

	//pop_back
	std::cout << "pop_back() : ";
	v1.pop_back();
	std::cout<< std::endl;
	printVector(v1);

	//insert
	std::vector<int>::const_iterator itr6 = v1.cbegin() ;
	std::cout << "insert(pos,val) : "<<std::endl;
       	v1.insert(itr6,200) ;
	printVector(v1);
	
	//erase
	std::cout<<"Erase(start,end) : "<<std::endl;
	v1.erase(itr6,itr6+4);
	printVector(v1);

	//swap : Swap the two VFector
	std::vector<int> v2 ;
	v2.assign (5,10);             // 7 ints with a value of 100	
	std::vector<int> v3 ;
	v3.assign (7,20);             // 7 ints with a value of 100	
	std::cout<<"Swap(vector) : "<<std::endl;
	v2.swap(v3);
	std::cout<<"Vector 2 : "<<std::endl;
	printVector(v2);
	std::cout<<"Vector 3 : "<<std::endl;
	printVector(v3);
	
	//clear = Removes all elements
	std::cout<<"Clear() : "<<std::endl;
	v3.clear();
	printVector(v3);
	
	//emplace(pos,elemet)
	std::vector<int>::const_iterator itr7 = v1.cbegin() ;
	std::cout << "emplace(pos,val) : "<<std::endl;
       	v1.emplace(itr6+1,300) ;
	printVector(v1);

	//emplace_back(element)
	std::cout << "emplace_back(val) : "<<std::endl;
       	v1.emplace_back(800) ;
	printVector(v1);
	return 0;
}
