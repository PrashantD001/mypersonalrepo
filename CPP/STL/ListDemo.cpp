
#include<iostream>
#include<list>

void printList(std::list<int> &l){

	std::list<int>::iterator itr1 ;
	for(itr1 = l.begin(); itr1!= l.end(); itr1++){
		std::cout<< *itr1 << " , ";
	}
	std::cout<< std::endl;
}

int main(){

	std::list<int> l = {10,20,30,40,50};


	// begin() and end()	
	std::cout << "begin() and end()" << std::endl;
	std::list<int>::iterator itr1 ;
	for(itr1 = l.begin(); itr1!= l.end(); itr1++){
		std::cout<< *itr1 << " , ";
	}
	std::cout<< std::endl;
	
	// rbegin() and rend()	
	std::cout << "rbegin() and rend()" << std::endl;
	std::list<int>::reverse_iterator itr2 ;
	for(itr2 = l.rbegin(); itr2!= l.rend(); itr2++){
		std::cout<< *itr2 << " , ";
	}
	std::cout<< std::endl;

	// cbegin() and cend()	
	std::cout << "cbegin() and cend()" << std::endl;
	std::list<int>::const_iterator itr3 ;
	for( itr3 = l.cbegin(); itr3!= l.cend(); itr3++){
		std::cout<< *itr3 << " , ";
	}
	std::cout<< std::endl;

	// crbegin() and crend()	
	std::cout << "crbegin() and crend()" << std::endl;
	std::list<int>::const_reverse_iterator itr4 ;
	for(itr4 = l.crbegin(); itr4!= l.crend(); itr4++){
		std::cout<< *itr4 << " , ";
	}
	std::cout<< std::endl;
	std::cout<< std::endl;

	//----------------------------------Capacity----------------------------------

	// size() - returns size of list
	std::cout << "size() : " << l.size() << std::endl;
	
	// max_size() - return maximum number of elements in list
	std::cout << "max_size() : " << l.max_size() << std::endl;

	// empty() - check the list is empty or not
	std::cout << "empty() : " << l.empty() << std::endl;

	//----------------------------Element Access-----------------------------------------
	
	//front() -  give First element
	std::cout<< "front() : " << l.front() <<std::endl;
	
	//back() - Returns a reference to the last element in the list.
	std::cout<< "back() : " << l.back() <<std::endl;
	printList(l);
	std::cout<< std::endl;
	std::cout<< std::endl;
	
	
	//---------------------------Modifiers-----------------------------------------------

	//assign
	std::list<int> l1 ;
	l1.assign (7,100);             // 7 ints with a value of 100	
	std::cout << "assign() : " << std::endl;
	printList(l1);

	//emplace_front(element)
	std::cout << "emplace_front(val) : "<<std::endl;
       	l1.emplace_front(800) ;
	printList(l1);

	//emplace_back(element)
	std::cout << "emplace_back(val) : "<<std::endl;
       	l1.emplace_back(500) ;
	printList(l1);
	
	//push_front
	std::cout << "push_front(int) : "<< std::endl;
	l1.push_front(700);
	printList(l1);
	
	//push_back
	std::cout << "push_back(int) : "<< std::endl;
	l1.push_back(400);
	printList(l1);

	//pop_front
	std::cout << "pop_front() : ";
	l1.pop_front();
	std::cout<< std::endl;
	printList(l1);
	
	//pop_back
	std::cout << "pop_back() : ";
	l1.pop_back();
	std::cout<< std::endl;
	printList(l1);	

	//insert
	std::list<int>::const_iterator itr6 = l1.cbegin() ;
	std::cout << "insert(pos,val) : "<<std::endl;
       	l1.insert(itr6,200) ;
	printList(l1);
		
	//erase
	std::cout<<"Erase(start,end) : "<<std::endl;
	l1.erase(itr6);
	printList(l1);
	
	//swap : Swap the two List
	std::list<int> l2 ;
	l2.assign (5,10);             // 7 ints with a value of 100	
	std::list<int> l3 ;
	l3.assign (7,20);             // 7 ints with a value of 100	
	std::cout<<"Swap(list) : "<<std::endl;
	l2.swap(l3);
	std::cout<<"List 2 : "<<std::endl;
	printList(l2);
	std::cout<<"List 3 : "<<std::endl;
	printList(l3);
	
	//clear = Removes all elements
	std::cout<<"Clear() : "<<std::endl;
	l3.clear();
	printList(l3);
	
	//emplace(pos,elemet)
	std::list<int>::iterator itr7 = l1.begin() ;
	std::cout << "emplace(pos,val) : "<<std::endl;
       	l1.emplace(itr7,300) ;
	printList(l1);
	std::cout<< std::endl;
	std::cout<< std::endl;


	//------------------------------------------Operations---------------------------

	//splice -Transfer elements from list to list
	std::list<int> l4 ;
	l4.assign (5,10);             // 7 ints with a value of 100	
	std::list<int> l5 ;
	l5.assign (7,20);             // 7 ints with a value of 100	
	std::cout<<"Splice(itr,list) : "<<std::endl;
	std::list<int>::iterator itr8 = l4.begin() ;
	l4.splice(itr8,l5);
	std::cout<<"List 4 : "<<std::endl;
	printList(l4);
	std::cout<<"List 5 : "<<std::endl;
	printList(l5);
	
	

	//remove - Remove elements with specific value
	std::cout<<"remove(val) : "<<std::endl;
	l1.remove(100);
	printList(l1);
	
	/*
	//remove_if
	std::cout<<"remove_if(val) : "<<std::endl;
	l1.remove(100);
	printList(l1);
	*/
	
	//unique - Remove duplicate values
	std::cout<<"unique() : "<<std::endl;
	l1.unique();
	printList(l1);
	
	//sort
	std::list<int> l6 = {50,80,40,30,10};
	std::list<int> l7 = {85,35,45,25,65,95};
	std::cout<<"sort() : "<<std::endl;
	l6.sort();
	l7.sort();
	std::cout<<"List 6 : "<<std::endl;
	printList(l6);
	std::cout<<"List 7 : "<<std::endl;
	printList(l7);


	// merge
	std::cout<<"merge(list) : "<<std::endl;
	l6.merge(l7);
	std::cout<<"List 6 : "<<std::endl;
	printList(l6);
	std::cout<<"List 7 : "<<std::endl;
	printList(l7);

	//reverse
	std::cout<<"reverse() : "<<std::endl;
	l6.reverse();
	std::cout<<"List 6 : "<<std::endl;
	printList(l6);
	return 0;
}
