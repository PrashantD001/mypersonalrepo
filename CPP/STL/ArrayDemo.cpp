
#include<iostream>
#include<array>

void printArray(std::array<int,5> &arr){

	std::array<int,5>::iterator itr1 ;
	for(itr1 = arr.begin(); itr1!= arr.end(); itr1++){
		std::cout<< *itr1 << " , ";
	}
	std::cout<< std::endl;
}

int main(){


	std::array<int,5> arr = { 10,20,30,40,50 };


	// begin() and end()	
	std::cout << "begin() and end()" << std::endl;
	std::array<int,5>::iterator itr1 ;
	for(itr1 = arr.begin(); itr1!= arr.end(); itr1++){
		std::cout<< *itr1 << " , ";		
	}
	std::cout<< std::endl;
	
	// rbegin() and rend()	
	std::cout << "rbegin() and rend()" << std::endl;
	std::array<int,5>::reverse_iterator itr2 ;
	for(itr2 = arr.rbegin(); itr2!= arr.rend(); itr2++){
		std::cout<< *itr2 << " , ";
	}
	std::cout<< std::endl;

	// cbegin() and cend()	
	std::cout << "cbegin() and cend()" << std::endl;
	std::array<int,5>::const_iterator itr3 ;
	for( itr3 = arr.cbegin(); itr3!= arr.cend(); itr3++){
		std::cout<< *itr3 << " , ";
	}
	std::cout<< std::endl;

	// crbegin() and crend()	
	std::cout << "crbegin() and crend()" << std::endl;
	std::array<int,5>::const_reverse_iterator itr4 ;
	for(itr4 = arr.crbegin(); itr4!= arr.crend(); itr4++){
		std::cout<< *itr4 << " , ";
	}
	std::cout<< std::endl;
	std::cout<< std::endl;

	//----------------------------------Capacity----------------------------------

	// size() - returns size of array
	std::cout << "size() : " << arr.size() << std::endl;
	
	// max_size() - return maximum number of elements in arr
	std::cout << "max_size() : " << arr.max_size() << std::endl;

	// empty() - check the vector is empty or not
	std::cout << "empty() : " << arr.empty() << std::endl;

	std::cout<< std::endl;
	std::cout<< std::endl;

	//----------------------------Element Access-----------------------------------------
	
	//operator[int] - give element at given position
	std::cout<< "operator[int] : " << arr[0] <<std::endl;
	
	//at(int) - give element at given position
	std::cout<< "at(int) : " << arr.at(0) <<std::endl;
	
	//front() -  give First element
	std::cout<< "front() : " << arr.front() <<std::endl;
	
	//back() - Returns a reference to the last element in the array
	std::cout<< "back() : " << arr.back() <<std::endl;
	
	//data() - Returns a direct pointer to the memory array used internally by the array to store its owned elements.
	std::cout<< "data() : " << arr.data() <<std::endl;

	std::cout<< std::endl;
	std::cout<< std::endl;

	//---------------------------Modifiers-----------------------------------------------

	//fill(val)
	std::array<int,5> arr1 ;
	arr1.fill(100);             
	std::cout << "fill(val) : " << std::endl;
	printArray(arr1);
	
	
	//swap : Swap the two Array
	std::array<int,5> arr2 ;
	arr2.fill(200);    	
	std::array<int,5> arr3 ;
	arr3.fill(300);   	
	std::cout<<"Swap(array) : "<<std::endl;
	arr2.swap(arr3);
	std::cout<<"Array 2 : "<<std::endl;
	printArray(arr2);
	std::cout<<"Array 3 : "<<std::endl;
	printArray(arr3);
	

	return 0;
}
